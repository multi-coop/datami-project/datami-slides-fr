
# Brief project description


:::::: {.columns}

::: {.column width=65%}
::: {.img-test .text-center .text-micro .img-no-margin}
![](images/datami-views-panel-06.png)
<!-- ![&nbsp;](images/logos/logo-DATAMI-rect-colors.png) -->

::: {.img-nano .mini-inline}
<!-- ![](images/logos/logo-DATAMI-rect-colors-h75.png)&nbsp;&nbsp;[Datami website](https://datami.multi.coop/?locale=en) -->
[Official website](https://datami.multi.coop/?locale=en)
:::

:::
:::

::: {.column .text-micro .text-justify width=35%}
<br>

Small organisations are at the same time the structures 
possessing the most accurate knowledge and informations
about specific topics or a territory,
but are also the kind of organisations 
with the 
<b>
most limited resources to open their data.
</b>

**Datami** open source project aims to 
<b>
help grassroot organisations producing & sharing structured data
</b>
by providing free, generic, and customizable web components 
for visualisation and contribution. 
Those components include features
allowing each 
<b>
organisation's community 
to contribute to such decentralized open data.
</b>

Datami paves the way to<br>
**transition from open data**
<br>
**to inclusive digital commons**
<br>
for a broad range of audiences.

:::

::::::


---

### · Needs · <br>Open data policies for grassroot organisations


::: {.text-center}
**Make grassroot open data become strong digital commons**
:::


::: {.text-micro}

:::::: {.columns}

::: {.column .text-justify width=50%}
Small or medium-sized grassroots organisations often are the very same ones 
acting as first responders to local problems on the territory or on very specific topics. 


Data allows them to act more efficiently at the local level, 
but also to 
<b>
raise public awereness towards a subject :
</b>
environment, health, social aid, urbanism, transportation...

NGOs, non profit associations, small local public institutions... 
all have their own way to gather data and knowledge about 
the topics they are involved in.
More crucially than for national institutions 
grassroot structures often need 
<b>
the help of their own communities
to maintain and enrich their data,
</b>
but most of the time
small orgranisations produce open data
<b>
with very limited means.
</b>
:::

::: {.column .text-justify width=50%}

Publishing grassroot data as it's produced - i.e. in a decentralized manner -
is a matter of public information 
but also a matter of empowering citizen while building it.

Hence, to reach public awareness
those organisations must beforehand overcome 
complex technical, design, and uses challenges at their level.

In a word, sharing the information such organisations produce 
is a matter of public interest, 
but also an activity more and more
complicated to follow through due to
<b>
an economic context
resulting by a growing lack of resources
for grassroot organisations.
</b>

:::

::::::

:::


---

### · Problem ·<br>Limited resources of grassroot organisations

::: {.text-center}
**How to open data despite limited resources ?**
:::

::: {.text-micro}
Following through an open data policy could get hard for small organisation because
<b>
publishing a dataset is rarely enough
</b>
... it's merely just a starting point.
Open data policies must aspire to improve the public's ability 
to 
<b>share</b>
the data, to 
<b>understand</b>
it, to 
<b>edit and contribute</b>
to it,
while assuring its 
<b>integrity</b>,
let producers keep their 
<b>
sovereignty
</b>
over their data,
and secure the 
<b>publishers' responsability.</b>
:::

:::::: {.columns .text-micro}

::: {.column .text-justify width=50%}
Open data policies led by small organisations
can quickly be put on hold due to the complexity of making 
open data attractive and accessible enough for broad audiences.

An open dataset must be adapted to very common uses, and anticipate the risk of
excluding "non-tech" publics so they could equally benefit from open & transparent information, 
but also so such publics could help improving information from grassroot.

To overcome this complexity open datasets must fulfil a set of technical needs, 
be shared via intermediary tools, platforms or developments often involving either 
overcosts, less control over the data, discouragement, or loss of time learning how to use such tools...
:::

::: {.column .text-justify width=50%}
We observed the most recurrent reasons causing open data policies to be put "on hold" were often to be found among the following :

- **Thight budget**
- **Lack of competences or technical difficulties**
- **Lack of time to curate and moderate contributions**
- **Lack of interest or impression of complexity**
- **Lack of open source tools for grassroot data producers**
- **Fear of losing control or sovereignty over data**
:::

::::::


---

### · Solution · <br>Make open data and grassroot contributions<br>inclusive and affordable

:::::: {.columns .text-micro}

::: {.column width=25%}

<br><br>

::: {.img-no-margin .img-mini .text-center}
![](images/logos/logo-DATAMI-title.png)
:::

:::

::: {.column width=75%}

::: {.img-no-margin .img-small .text-center}
![](images/datami-views-panel-05.png)
:::

:::

::::::

:::::: {.columns .text-micro}

::: {.column width=25%}
<b>
  **More intelligible**
  <br>
  **an informative datasets**
</b>

::: {.text-justify}
Datami provides a variety of data-visualisations and views commonly used on internet
such as lists of cards, maps, graphs, diagrams... 
and also a variety of commons tools like interactive filters
you can customize for any specific dataset
:::
:::

::: {.column width=25%}
<b>
  **More easily open**
  <br>
  **to external contributions**
</b>

::: {.text-justify}
Datami relies on external platforms such as Gitlab or Gihub, 
thus also being able to use their APIs 
to send contributions onto datasets 
via merge requests on new branches.
:::
:::

::: {.column width=25%}
<b>
  **More structured**
  <br>
  **and interoperable datasets**
</b>

::: {.text-justify}
Datami includes the possibilty to associate data models to datasets.
For instance table schema standard (by Frictionless) is used as Datami's standard for csv tables.
:::
:::

::: {.column width=25%}
<b>
  **More affordable, sovereign**
  <br>
  **and transparent**
</b>

::: {.text-justify}
Datami delagates to external platforms (like Gitlab)
the data storage and the versionning control features.
Doing so Datami stands for a larger complementarity between FLOSS solutions,
and for a low-code approach to open data.
:::
:::

::::::

---

### · Ambitions ·

::: {.text-micro}
Datami's project aims to adress the latter problem by considering 
**the future of open data must be even more distributed,**
as much in its production as in its maintenance or considering broader target audiences. 

We have the ambition to provide a FLOSS (Free/Libre and Open Source Software) toolbox for grassroot organisations,
allowing them to accelerate their open data policies 
based on a combination of open source technologies.
:::

:::::: {.columns .text-micro}

::: {.column .text-justify width=50%}
### VALORIZE

Enable organisations of all sizes 
<b>to produce open data & valorise their datasets</b> with their communities.

:::

::: {.column .text-justify width=50%}
### CONTRIBUTE

Empower organisations' audiences by  helping them to 
<b>contribute to open data freely</b>, easily, while respecting their privacy.

:::

::: {.column .text-justify width=50%}
### DISTRIBUTE

Help any organisation to create 
<b>interoperable & structured</b> datasets,
with the strict minimum of technical background. 

:::

::: {.column .text-justify width=50%}
### PROMOTE FLOSS

Promote <b>complementarity within FLOSS (Free/Libre and Open Source Software) robust initiatives</b>, 
notably by the use of Git / Gitlab as a backend for data storage and version control.
:::

::::::

::: {.text-micro .text-center}
As you will discover later in this document we segmented those ambitions into three challenges : 
<br>
**intelligibility** &nbsp;
**contributibility** &nbsp;
**affordability**
:::

---

### · Innovation / differentiation · 1/2

::: {.text-micro}

:::::: {.columns}

::: {.column .text-justify width=50%}
There is nowadays a large variety of solutions - either open source or proprietary - for data visualisation or edition.
Those solutions can either be provided as Saas or stand alone. 

Most common ones are Metabase or Grist in the open source category, or Airtable and GSheet in the proprietary section (*).

Those existing solutions still have two common factors :

- They suppose either to <b>use a backend server you don't have sovereignty over ;</b> or to <b>install and maintain a backend server yourself</b> ;

- They often <b>lack or provide limited features for contribution and moderation.</b>

:::

::: {.column .text-justify width=50%}
**Datami's approach intends to do "more with less"** : 
we intended to provide more features for contribution and moderation on a dataset, 
whereas liberating us from developping ourselves a complex backend for data storage and/or version control.

The solution we found was quite "on the nose" : 
the best tools we can think of today for making or managing contributions - and then to moderate them - 
are the git language and platforms relying on git like Gitlab.

That said and to our knowledge,
<b>
we didn't find existing open source solutions 
adressing at the same time the needs 
we encountered
</b>
talking to small organisations
hoping to produce open data for their audiences :
a tool
<b>
simple to apprehend,
affordable, 
helping audiences to propose contributions,
with variety and quality in terms visualisations and design.
</b>

:::

::::::

(*) _An [extensive benchmark](https://datami.multi.coop/benchmark?locale=en) is provided later in this document._

:::

---

### · Innovation / differentiation · 2/2

::: {.text-micro}

<b>
To adress the needs of open data's grassroot producers
we designed a software architecture 
<br>
based on a serie of simple principles
we follow altogether
</b>

:::::: {.columns .text-justify}

::: {.column width=25%}
**Use existing platforms**<br>**as backends**

We use platforms such as 
Gitlab / Github / Bitbucket / SourceForge / external APIs... 
<b>
as backends for data storage and version control.
</b>

Such platforms are proved to be robust,
we only account for a few,
some are FLOSS (Free/Libre and Open Source Software),
and they already provide all the tools
needed for version control and moderation.

Thus we can store either datasets
or any other file (such as configuration files, schemas, text files...)
on those platforms independantly from Datami's application.
:::

::: {.column width=25%}
**Embed your own**<br>**interfaces**

The spaces where small orgaizations 
need to valorize their data is
where their target audience goes,
usually the organization's own website. 

It is very common small for organizations to use
Wordpress or on-the-shelf solutions for their website,
but some are custom-made.

The use of 
<b>
web components 
</b>
technology and web standards (or "widgets") is the choice 
we made to 
<b>
ensure the ability to embed data onto any website.
</b>
:::

::: {.column width=25%}
**Keep the data structure**<br>**at all steps**

Data interoperability is a principle we implemented
in Datami with the use of standards, 
such as Frictionless for tables and json files.

Any table displayed with Datami can be 
<b>
associated with a local or distant schema, 
</b>
in addition to configuration files. 

Doing so, Datami can display datasets
accordingly to its schema,
but also provide pre-rendered forms
to help for open contributions.
:::

::: {.column width=25%}
**Display the data**<br>**as you need to**

Genericity and customisation are core intentions of Datami.
We search for a 
<b>
balance between the need to customize 
</b>
the UI/UX of the widgets,
<b>
and the need for a versatile tool
</b>
adaptable to a large range of data structures or properties.

We distinguish several types of files : 
end-user data (csv, text, json, API requests...),
schemas files,
and configuration files. 

What makes a dataset unique
can be set up either in its schema or configuration files,
or even in the web component's html itself. 
:::

::::::

:::


