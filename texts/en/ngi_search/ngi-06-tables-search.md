

## Priorities

::: {.text-micro .text-justify}

:::::: {.columns}

::: {.column width=50%}

In 2022 the Datami project benefited from a support of 62 k€ from the ANCT, and was also co-funded by several private entities during missions up to 35 k€. 

These fundings allowed us to build most of its core features, such as providing customazible frontend web components, the ability to dialog with Gitlab’s / Github’s / mediawiki’s APIs to retrieve data and send contributions, and its ability to associate data models to open datasets.

However Datami is still a project in its earliest phases of developments. 
We apply for the 
NGI Zero entrust program 
because 
**we identified some core features still in need of being developped**
so Datami could fulfill its scope of promises.

From the [global roadmap](https://datami.multi.coop/roadmap/?locale=en) of the Datami project we established a sub-roadmap specific to 
NGI Zero entrust program
we consider coherent with its main goals.
:::

::: {.column width=50%}
The NGI zero entrust grant 
will be used exclusively in tasks of 
**research and development**, including :

- Developping a WYSIWYG interface on top of Datami <b>to easily configure a Datami widget from scratch</b> for end users with low technical knowledge ;

- Developping a WYSIWYG interface on top of Gitlab / Github <b>to moderate and manage pull requests</b> for end users with low technical knowledge ;

- <b>Improving the ability to interact with third-party API</b> of services such as Github, Gitlab or other APIs for retrieving data, data structure, pushing commits or PR...

- <b>Better integration of Frictionless standards</b> ;

- Experiments with <b>new use cases and roadmap management</b> with the stakeholders.
:::

::::::

:::


---

## Budget

::: {.text-center} 
**Regrouped by category and milestones**
:::

|**Family**|**Category**|**Milestones**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|------------|------|------|---:|---:|
|TECH|Implementing|<b>More data sources</b>|🔴 high|▪️▪️ medium|<b> 16 days</b>|<b>14 400 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|🔴 high|▪️▪️▪️ hard|<b> 13 days</b>|<b>11 700 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|🔴 high|▪️▪️▪️ hard|<b> 12 days</b>|<b>10 800 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|🟡 low|▪️▪️ medium|<b> 28 days</b>|<b>25 200 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|🟡 low|▪️▪️ medium|<b> 26 days</b>|<b>23 400 €</b>|
|TECH|Implementing|<b>Better UX - maps</b>|🟡 low|▪️▪️ medium|<b> 12 days</b>|<b>10 800 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|🟠 medium|▪️ easy|<b> 4 days</b>|<b>3 600 €</b>|
|TECH|Implementing|<b>Better UI - new views</b>|🟡 low|▪️▪️ medium|<b> 16 days</b>|<b>14 400 €</b>|
|TECH|Implementing|<b>Refactoring</b>|🟠 medium|▪️▪️▪️ hard|<b> 18 days</b>|<b>16 200 €</b>|
|TECH|Implementing|<b>Tests</b>|🟡 low|▪️▪️▪️ hard|<b> 7 days</b>|<b>6 300 €</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|🔴 high|▪️ easy|<b> 3 days</b>|<b>2 700 €</b>|
|BIZDEV|OnBoarding|<b>Translations</b>|🟡 low|▪️ easy|<b> 2 days</b>|<b>1 800 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|🔴 high|▪️▪️▪️ hard|<b> 7 days</b>|<b>6 300 €</b>|
|||<b>TOTAL</b>|||**164 d.**|**147 600 €**|

---

## Detailed budget

---

### Detailed budget & roadmap · 1/2

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>6 d.</b>|<b>5 400 €</b>|
|TECH|Backlog|<b>More data sources</b>|Connector to SourceForge|🟡|▪️▪️▪️|<b>5 d.</b>|<b>4 500 €</b>|
|TECH|Backlog|<b>More data sources</b>|Connector to BitBucket|🟡|▪️▪️▪️|<b>5 d.</b>|<b>4 500 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>8 d.</b>|<b>7 200 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>5 d.</b>|<b>4 500 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>6 d.</b>|<b>5 400 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|<b>5 400 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>|<b>4 500 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>5 d.</b>|<b>4 500 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>4 d.</b>|<b>3 600 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Add / drag-drop a picture in a cell|🟡|▪️▪️|<b>6 d.</b>|<b>5 400 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>8 d.</b>|<b>7 200 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Full screen - debug|🟠|▪️▪️|<b>6 d.</b>|<b>5 400 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Range filter|🟡|▪️|<b>4 d.</b>|<b>3 600 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Change width column|🟠|▪️▪️|<b>8 d.</b>|<b>7 200 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Helper at loader|🟡|▪️|<b>2 d.</b>|<b>1 800 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>|<b>5 400 €</b>|




---

### Detailed budget & roadmap · 2/2

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>Better UX - maps</b>|Add or edit geojson objects|🟡|▪️▪️▪️|<b>12 d.</b>|<b>10 800 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>4 d.</b>|<b>3 600 €</b>|
|TECH|Implementing|<b>Better UI - new views</b>|Agenda view|🟡|▪️|<b>5 d.</b>|<b>4 500 €</b>|
|TECH|Adopting|<b>Better UI - new views</b>|Graphs view with D3js|🟡|▪️▪️|<b>7 d.</b>|<b>6 300 €</b>|
|TECH|Adopting|<b>Better UI - new views</b>|Simultaneous dataviz + map on same view|🟡|▪️▪️|<b>4 d.</b>|<b>3 600 €</b>|
|TECH|Implementing|<b>Refactoring</b>|Put all git* requests into a package|🟡|▪️▪️▪️|<b>8 d.</b>|<b>7 200 €</b>|
|TECH|Implementing|<b>Refactoring</b>|Migration to Typescript|🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 500 €</b>|
|TECH|Backlog|<b>Refactoring</b>|Migration to Vue3|🟡|▪️▪️▪️|<b>5 d.</b>|<b>4 500 €</b>|
|TECH|Implementing|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>|<b>6 300 €</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>3 d.</b>|<b>2 700 €</b>|
|BIZDEV|OnBoarding|<b>Translations</b>|Translate to Spanish|🔴|▪️|<b>2 d.</b>|<b>1 800 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>4 d.</b>|<b>3 600 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Set up showcases|🟡|▪️▪️|<b>3 d.</b>|<b>2 700 €</b>|

<br>

|&nbsp;|&nbsp;|&nbsp;|&nbsp;|&nbsp;|&nbsp;|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|------|------------|:---:|---|---:|---:|
||||||<b>TOTAL</b>|**164 d.**|**147 600 €**|


--- 

## Budget analysis

::: {.text-center} 
**Priority · _vs_ · Difficulty**
:::

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
    In days
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high| 14 days| 14 days| 8 days|<b> 36 days</b>|
|🟠 medium| 13 days| 27 days|  days|<b> 40 days</b>|
|🟡 low| 48 days| 29 days| 11 days|<b> 88 days</b>|
|**TOTAL**|<b> 75 days</b>|<b> 70 days</b>|<b> 19 days</b>|**164 days**|

:::


::: {.column width=50%}

::: {.text-center}
<b>
    In euros    
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|12 600 €|12 600 €|7 200 €|<b>32 400 €</b>|
|🟠 medium|11 700 €|24 300 €|  €|<b>36 000 €</b>|
|🟡 low|43 200 €|26 100 €|9 900 €|<b>79 200 €</b>|
|**TOTAL**|<b>67 500 €</b>|<b>63 000 €</b>|<b>17 100 €</b>|**147 600 €**|

:::

::::::

<br>

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
    In proportions
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|8.5%|8.5%|4.9%|<b>22.0%</b>|
|🟠 medium|7.9%|16.5%|0.0%|<b>24.4%</b>|
|🟡 low|29.3%|17.7%|6.7%|<b>53.7%</b>|
|**TOTAL**|<b>45.7%</b>|<b>42.7%</b>|<b>11.6%</b>|**100.0%**|

:::

::::::








# Provisional timelines

**A lean approach for iterative and incremental developments**

---

## Timelines

<br>

::: {.text-center}

<b>
The following timelines are
to be considered as only 
provisional.
</b>

These timelines may be adapted depending on
<br> 
the context, our resources, 
and the priority of demands 
<br>
emerging from  new use cases. 
:::


---

### Timeline · Technical tasks

::: {.ngi-timeline .ngi-timeline-mini}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|
|TECH|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>6 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>More data sources</b>|Connector to SourceForge|🟡|▪️▪️▪️|<b>5 d.</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>More data sources</b>|Connector to BitBucket|🟡|▪️▪️▪️|<b>5 d.</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>8 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|||||
|TECH|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>6 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|||<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>5 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UX - data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>4 d.</b>|||||<span class="cell-bg">x</span>||
|TECH|<b>Better UX - data management</b>|Add / drag-drop a picture in a cell|🟡|▪️▪️|<b>6 d.</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>Better UX - data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>8 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|TECH|<b>Better UX - data interaction</b>|Full screen - debug|🟠|▪️▪️|<b>6 d.</b>|||||<span class="cell-bg">x</span>||
|TECH|<b>Better UX - data interaction</b>|Range filter|🟡|▪️|<b>4 d.</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>Better UX - data interaction</b>|Change width column|🟠|▪️▪️|<b>8 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data interaction</b>|Helper at loader|🟡|▪️|<b>2 d.</b>||<span class="cell-bg">x</span>|||||
|TECH|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UX - maps</b>|Add or edit geojson objects|🟡|▪️▪️▪️|<b>12 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|TECH|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UI - new views</b>|Agenda view|🟡|▪️|<b>5 d.</b>||||<span class="cell-bg">x</span>|||
|TECH|<b>Better UI - new views</b>|Graphs view with D3js|🟡|▪️▪️|<b>7 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UI - new views</b>|Simultaneous dataviz + map on same view|🟡|▪️▪️|<b>4 d.</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>Refactoring</b>|Put all git* requests into a package|🟡|▪️▪️▪️|<b>8 d.</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|TECH|<b>Refactoring</b>|Migration to Typescript|🟠|▪️▪️▪️|<b>5 d.</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>Refactoring</b>|Migration to Vue3|🟡|▪️▪️▪️|<b>5 d.</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|TECH|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|








:::

---

### Timeline · Bizdev tasks

::: {.ngi-timeline .ngi-timeline-mini}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|
|BIZDEV|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|BIZDEV|<b>Translations</b>|Translate to Spanish|🔴|▪️|<b>2 d.</b>||<span class="cell-bg">x</span>|||||
|BIZDEV|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>4 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|BIZDEV|<b>Community management</b>|Set up showcases|🟡|▪️▪️|<b>3 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|


:::

