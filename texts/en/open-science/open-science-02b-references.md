
# Some references & examples

<br>

:::{.img-mini}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)
:::

:::{.text-center .text-micro}
All our projects are **open-sourced**
:::

---

## DBnomics

:::::: {.columns}

::: {.column width=30%}

::: {.text-nano}
[DBnomics](https://db.nomics.world/) is one of the largest economic databases in the world, aggregating hundreds of millions of time series from dozens of sources (INSEE, Eurostat, IMF, WTO, WB... i.e. 50+ providers) and making them available via a single API.

DBnomics is a project led by the Macro Observatory of the Center for Economic Research and its Applications (CEPREMAP), the Banque de France and France Stratégie are partners in the project and the software development is ensured by Jailbreak. DBnomics is also a winner of the Future Investment Program (PIA).
:::

:::

::: {.column width=70%}

:::r-stack

::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .current-visible}
![&nbsp;](images/references/dbnomics/01.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/dbnomics/02.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/dbnomics/03.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/dbnomics/04.png)
:::

:::

:::

::::::

<hr>

:::::: {.columns}

::: {.column width=33%}

**Problem**

::: {.text-nano}
- Agregate multiple data sources
- Regular data updates
:::

:::

::: {.column width=33%}

**Solution**

::: {.text-nano}
- Dedicated platform
- Public and protected API
- Connectors & transformers
:::

:::

::: {.column width=33%}

**Constraints**

::: {.text-nano}
- Connectors maintenance
- Data complexity
:::

:::

::::::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column width=20%}
<b>Data management</b>

⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data preprocessing</b>

⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data enrichment</b>

⭐️⭐️
:::
::: {.column width=20%}
<b>Data publishing</b>

⭐️⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data valorisation</b>

⭐️⭐️⭐️
:::
::::::
:::

---

## Validata

:::::: {.columns}

::: {.column width=30%}

::: {.text-nano}
[Validata](https://validata.fr/) aims to offer a platform for validating open data sources.

It is aimed at French communities wishing to validate the quality and interoperability of the data they publish using an external tool. It also allows data warehouse managers to qualify the integrity of the data they wish to use before importing it into a multi-source database.

This platform, developed in open source, is accessible to any public or private actor wishing to publish or operate open public data.
:::

:::

::: {.column width=70%}

:::r-stack

::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .current-visible}
![&nbsp;](images/references/validata/validata-01.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/validata/validata-02.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/validata/01.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/validata/02.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/validata/03.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/validata/04.png)
:::

:::

:::

::::::

<hr>

:::::: {.columns}

::: {.column width=33%}

**Problem**

::: {.text-nano}
- Compare source datasets to schemas
- Schema sources could be external
- Sovereign deployment
:::

:::

::: {.column width=33%}

**Solution**

::: {.text-nano}
- Dedicated platform
- Public and protected API
- Connectors & transformers
:::

:::

::: {.column width=33%}

**Constraints**

::: {.text-nano}
- Connectors maintenance
- Data complexity
:::

:::

::::::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column width=20%}
<b>Data management</b>

⭐️
:::
::: {.column width=20%}
<b>Data preprocessing</b>

⭐️⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data enrichment</b>

⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data publishing</b>

⭐️⭐️
:::
::: {.column width=20%}
<b>Data valorisation</b>

⭐️
:::
::::::
:::

---

## Ma cantine

:::::: {.columns}

::: {.column width=30%}

::: {.text-nano}
[Ma Cantine](https://ma-cantine.agriculture.gouv.fr/) aims to increase the share of sustainable products in collective catering. A platform is offered to canteen managers and citizens. It serves as display windows for canteens as well as a management tool. For the administration, it is a tool for managing certain measures of the EGALim law. The project is a state start-up in which around ten people work.
:::

:::

::: {.column width=70%}

:::r-stack

::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .current-visible}
![&nbsp;](images/references/ma-cantine/landing.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/ma-cantine/app.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/ma-cantine/home.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/ma-cantine/tools.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/ma-cantine/registre.png)
:::

:::

:::

::::::

<hr>

:::::: {.columns}

::: {.column width=33%}

**Problem**

::: {.text-nano}
- National service for ultra-local entities
- Datasets quality
- Opening data as a project within the project
:::

:::

::: {.column width=33%}

**Solution**

::: {.text-nano}
- Dedicated platform
- Public and protected API
- Connectors & transformers
:::

:::

::: {.column width=33%}

**Constraints**

::: {.text-nano}
- Project lead by public administration
- Number of contributors
:::

:::

::::::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column width=20%}
<b>Data management</b>

⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data preprocessing</b>

⭐️⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data enrichment</b>

⭐️
:::
::: {.column width=20%}
<b>Data publishing</b>

⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data valorisation</b>

⭐️⭐️⭐️
:::
::::::
:::

---

## Datami

:::::: {.columns}

::: {.column width=30%}

::: {.text-nano}
[Datami](https://datami.multi.coop) is a generic widget-type tool, allowing you to view and edit data hosted on Github or Gitlab, or even on a mediawiki.

Datami has different features aimed at simplifying data editing and enrichment:

Visualization in the form of a table, files, cartography or data visualizations / 
Visualization of the differences between the original version and the version edited by the user / 
Search by filters or in full text / 
Import/export data / 
Apply a data schema to spreadsheet data / 
Easy copy/paste the html block of the widget / 
Simplified process for creating a merge request...
:::

:::

::: {.column width=70%}

:::r-stack

::: {.fragment .no-pt .no-mt .h-35 .no-caption .current-visible}
![&nbsp;](images/datami-views-panel-05.png)
:::
::: {.fragment .no-pt .no-mt .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/datami-widgets-clients.png)
:::
::: {.fragment .no-pt .no-mt .h-35 .img-border .no-caption .fade-in-then-out}
![&nbsp;](images/screenshots/datami-website-en.png)
:::
::: {.fragment .no-pt .no-mt .h-35 .img-border .no-caption .fade-in}
![&nbsp;](images/screenshots/datami-doc-website.png)
:::

:::

:::

::::::

<hr>

:::::: {.columns}

::: {.column width=33%}

**Problem**

::: {.text-nano}
- General public as target
- For structures with low level of resources
- Variety of datasets' models
:::

:::

::: {.column width=33%}

**Solution**

::: {.text-nano}
- Customizable widget
- Git-based versionning
- "Database-less"
:::

:::

::: {.column width=33%}

**Constraints**

::: {.text-nano}
- Manual data management & pre-processing
- Complex to configure
:::

:::

::::::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column width=20%}
<b>Data management</b>

⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data preprocessing</b>

⭐️⭐️
:::
::: {.column width=20%}
<b>Data enrichment</b>

⭐️⭐️
:::
::: {.column width=20%}
<b>Data publishing</b>

⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data valorisation</b>

⭐️⭐️⭐️⭐️⭐️
:::
::::::
:::
