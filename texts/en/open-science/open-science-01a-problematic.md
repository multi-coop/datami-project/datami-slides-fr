
# Open the science

::: {.text-center style="font-size:2em"}
**A long, and bumpy road ...**
:::

## Introduction

::: {.text-micro}
**The Practices and Attitudes of Researchers at Concordia University with Regard to Research Data Management:**<br>
**Results of an Investigation**
:::

:::::: {.columns}

::: {.column width=30%}

::: {.text-nano}

"A survey and a series of interviews of Concordia University (Montréal) professors were undertaken in 2015-2016 to better understand the needs and attitudes of Canadian researchers in the area of data management.

The <b>majority of researchers want to better manage, preserve and share their research data</b>. They also recognise the merits of doing so.

However, certain barriers prevent them from better managing their data, namely the <b>lack of incentives, the shortage of human and technological resources, confidentiality issues and the ability to exercise a certain level of control</b> on the use of the data by others."
:::

:::

::: {.column width=70%}

:::r-stack

::: {.fragment .no-pt .no-mt .img-border .h-40 .no-caption .current-visible}
![&nbsp;](images/open-science/Guindon-01.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-40 .no-caption .fade-in-then-out}
![&nbsp;](images/open-science/Guindon-02.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-40 .no-caption .fade-in-then-out}
![&nbsp;](images/open-science/Guindon-03.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-40 .no-caption .fade-in-then-out}
![&nbsp;](images/open-science/Guindon-04.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-40 .no-caption .fade-in-then-out}
![&nbsp;](images/open-science/Guindon-05.png)
:::

:::

:::

::::::


::: {.text-nano}
*<b>Dennie, D. & Guindon, A. (2017)</b>. Résultats d’une enquête sur les pratiques et attitudes des chercheurs de l’Université Concordia en matière de gestion des données de recherche. Documentation et bibliothèques, 63(4), 59–72. doi:10.7202/1042311ar*
:::

---

## Challenge #1

### **Structure** and **enrich** your datasets

---

:::::::::::::: {.columns}

::: {.column width="45%"}
A major step to **make datasets more discoverable and reusable** is to specify and document its schema, metadata, sources, methodology...
This step could be considered quite time-consuming and technical in nature.

Another step before publishing datasets shall be to enrich them by **cross-refencing them with third party open datasets** to insure their interoperability.
:::

::: {.column width="55%"}
<br>
<br>
![&nbsp;](images/misc/data-package/frictionless-tiles.png)
:::

::::::::::::::

---

## Challenge #2

### Make datasets **intelligible**

---

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/messy-excel-02.png)
:::
:::

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ODF-map-all.png)
:::
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
A **raw** data
:::
:::

::: {.column width="50%"}
::: {.text-center}
The **same** dataset
<br>
for open publication
:::
:::
::::::::::::::

---

## Challenge #3

### Make **updating** and **contributing** easier

---

:::::::::::::: {.columns}

::: {.column width="40%"}
Handling data is a matter of habits, facilitating their manipulation for the greatest number means that you have to **adapt to the habits** of the greatest number

::: {.text-micro}
To allow everyone to easily contribute to the data, the view in the form of a table is still the most commonly adopted today.
:::

:::

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/table-view-01.png)
:::
:::

::::::::::::::

---

## Challenge #4

### Make all of the above **with limited resources**

---

Publishing data of general interest (research data in particular) while publishing research papers, in order to highlight them on a website, or calling on your community to put them up to date, could turn out to be complicated :

::: {.text-center}
**Lack of resources, lack of skills, lack of time...**
:::

:::::::::::::: {.columns}
::: {.column width="30%"}
:::fragment
::: {.text-center}
<i class="ri-tools-fill"></i>
:::
::: {.text-justify}

The cost of the usual technical solutions for sharing / viewing / contributing to data is often explained by the technical **complexity** of these functionalities
:::
:::
:::
::: {.column width="30%"}
:::fragment
::: {.text-center}
<i class="ri-server-fill"></i>
:::
::: {.text-justify style="padding: 0 1.5em 0 1.5em"}

Another cost is related to the need to set up dedicated **servers** in _backend_ or very specific configurations
:::
:::
:::
::: {.column width="30%"}
:::fragment
::: {.text-center}
<i class="ri-hourglass-2-fill"></i>
:::
::: {.text-justify}

The **maintenance** of applications or servers often generates significant costs
:::
:::
:::
::::::::::::::

