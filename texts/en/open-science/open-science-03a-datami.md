# Open science with **Datami** toolbox

<br>

::: {.img-mini}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::

::: {.text-center}
**https://datami.multi.coop**
:::

---

## Simplifying datasets preparation before open science publishing

::: {.h-45 .no-mt }
![&nbsp;](images/diagrams/diagram-open-science-01.png)
:::

<hr>

:::::: {.columns .text-center .text-micro}

::: {.column width=25%}
<i class="ri-database-2-line"></i>

<b>Control over <br>data storage & open level</b>
:::
::: {.column width=25%}
<i class="ri-file-search-line"></i>

<b>Interoperability <br>with open standards</b>
:::
::: {.column width=25%}
<i class="ri-window-fill"></i>

<b>Interfaces <br>for all audiences</b>
:::
::: {.column width=25%}
<i class="ri-message-2-line"></i>

<b>Versionning <br> and open contributions</b>
:::

::::::


---

## You control your data <br>Datami makes it understandable

Datami's architecture is designed as an **interface between the citizen and your database** in order to facilitate the link **between citizens and open data producers**

::: {.no-mt .h-30}
![](images/roadmaps/archi-devs-en.png)
:::

---

## Structure your data <br>to make it interoperable

:::::::::::::: {.columns}

::: {.column width="40%"}
Your data may be associated with files to structure it, such as **data schema files**

By associating your data set with a data schema respecting international **standards** you ensure that they can be correctly reused and improved

:::

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-schemas-01.png)
:::

::::::::::::::

---

**Datami doesn't store your data** : your data stays within the tool of your choice (Github, Gitlab, database or API).

Using Datami is more **affordable** because you don't have to install nor maintain a dedicated _backend_. 

---

## View your data from all angles

:::::::::::::: {.columns}

::: {.column width="40%"}
Your data can be viewed as **spreadsheets**, **maps**, miniature or detailed **lists of records**, or **graphs**

All views are interactive and customizable to highlight all the specifics of your data

:::

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-views-01.png)
:::

::::::::::::::

---

## An affordable and ethical solution<br> for structures with limited resources

Datami provides a way 
for data producers from small-sized or local organisations
to **keep control** over their data, 
**engage** with their communities,
<br>
**without subscription nor infrastucture or maintaining costs.**

:::::: {.columns}

::: {.column width=30%}

::: {.text-micro .text-justify}
For a small structure
producing data of general interest 
it can be complicated to allocate resources 
to highlight datasets on your website 
or to call on your community to update them.

The cost of sharing, viewing, or contributing to data 
is often explained by the 
<b>
technical complexity
</b>
of such features,
the necessity to set up 
<b>
dedicated servers
</b>
as _backend_,
or costs of their 
<b>
maintenance
</b>.

<!-- Valorizing your open data and let your communities
contribute should not be limited by the amount of 
technical or financiary resources you are disposing of. -->

:::

:::

::: {.column width=70%}

::: {.img-medium .img-no-margin .text-center}
![](images/diagrams/diagram-benchmark-01.png)
:::
:::

::::::

::: {.text-micro .text-no-mt}
Datami's original architecture makes possible to 
<b>
get rid of a large part of the _backend_ server costs
</b>
<br>
while allowing customization dataset by dataset, 
as open contributions and fine control over the data.
:::

---

## A free and multi-purpose software

![&nbsp;](images/screenshots/datami-feats-matrix-en.png)

---

## The Datami offer by the multi cooperative

**We aim to propose different services aroud Datami**

<br>

::: {.text-micro}

:::::::::::::: {.columns}

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-setup.png)

::: {.text-center}
**Datami setup**
:::

Multi.coop can provide an economic package
of a few days only for support in setting up Datami
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-pack.png)

::: {.text-center}
**Datami as free software** 
:::

Use Datami as it is based on the documentation, 
you are free!
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-custom_dev.png)

::: {.text-center}
**Custom developments** 
:::

Multi.coop can help you responding to you needs
using Datami while adding new features 
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-setup-interface.png)

::: {.text-center}
**Online configuration** \*
:::

Use a simple interface to setup yourself the 
widget you need for your datasets.

_\* This offer is currently under construction_

:::

::::::::::::::

::: 

::: {.text-micro}
In order to make Datami as accessible and affordable as possible, 
our principle is to 
<b>
mutualize design and development costs.
</b>
<br>
All developments - even minimal ones - contributing to improve Datami 
eventually benefit all users.
:::


---

## Integrate Datami into your <br>site and your partners

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami uses **_widgets_** technology: turnkey and customizable modules that you can add to an existing site

Datami _widgets_ are **_open source_**, simple to copy and paste, without subscription, **without additional cost**

:::

::: {.column width="60%"}
![&nbsp;](images/screenshots/widget-copy-01.png)
:::

::::::::::::::

---

## Empower your teams and audiences <br>to improve data

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami includes a **contribution and moderation system**

Based on the Git language, Datami _widgets_ allow you to keep control of your data and manage contributions, **without creating an account** to suggest improvements

:::

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-contribution-01.png)
:::
:::

::::::::::::::

---

## Drive your field actions through data

:::::::::::::: {.columns}

::: {.column width="40%"}
**Exploring your data** allows you to better understand and manage your actions in the field

Datami allows you to set up personalized interactive data-visualizations, in order to make your data easily explorable

:::

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-pilotage-01.png)
:::

::::::::::::::

---

Datami is fully auditable and reusable **free licensed software**

To discover and learn how to use Datami you can:

- Go to the [official Datami website](https://datami.multi.coop);
- Directly access the [Datami source code](https://gitlab.com/multi-coop/datami-project/datami);
- Consult the [technical documentation](https://datami-docs.multi.coop) and online tutorials;
- Call on the [Multi cooperative](https://multi.coop) for advice in data science and for training.

---

## Our first users

**Since late 2022 we implemented Datami for a dozen organisations,**<br>**some public and other private**

::: {.text-micro}

Each organisation may at the time have had one or several datasets to expose and share.
<br>
The needs we covered were related to data-visualisation, cartography, embeddibility, reproductibility... 
<br>
<b>
The expression of needs along the way participated to build our roadmap,
</b>
feature demand by feature demand.

:::::: {.columns}

::: {.column width=35%}

Current use cases include datasets focusing on a variety of topics of public interest :

- Indicators about places providing public digital services (France services) ;
- Indicator on digital parental fragility ;
- Open data producers ;
- Third places characteristics ;
- Alternative mobility projects ...

More examples on the [Datami website](https://datami.multi.coop/examples?locale=en)

:::

::: {.column .img-no-margin  width=65%}
![](images/clients/clients.png)
:::

::::::

:::


---

## Analytics

**We use the open souce solution Matomo as our main analytics platform.**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
The metrics are visible for everyone 
on this [link](https://multi.matomo.cloud/index.php?module=CoreHome&action=index&idSite=1&period=range&date=previous30#?period=range&date=2022-08-28,2023-03-25&category=Dashboard_Dashboard&subcategory=1&idSite=1)
and are set to preserve the privacy of Datami's users.

Our metrics shows 
<b>
a slow but continuous trend of growth in the using of Datami
</b>
amongst our first users.
:::

::: {.column width=50%}

We also observed the peaks in our metrics may be related
with events - either organized by multi or by stakeholders -
boosting up the visibility of datasets exposed with Datami
towards their target audiences.
:::

::::::

:::

::: {.text-center .img-pdf}
![](images/matomo/matomo-datami_widget-04.png)
:::

---


## The official website

:::::::::::::: {.columns}

::: {.column width="40%"}
On our **official website** you will find **videos** of presentation, **examples**, as well as our **blog** space

The site is translated into French and English

:::

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-website-en.png)
:::
::: {.text-center}
🚀 [datami.multi.coop](https://datami.multi.coop)
:::
:::

::::::::::::::

---

## Datami's sponsors in 2022-2023

<br>

:::{.text-micro}
Datami project was **laureate of the "Plan France Relance 2022"**
and has benefited from the support of the following organizations
![&nbsp;](images/sponsors/sponsors.png)

Julien Paris (Datami's product owner and lead developer) is **laureate of the 1st NGI Enrichers** european fellowship program.
He will stay for 6 months in Montreal (Canada) in 2024, in collaboration with the Concordia University and Mitacs.

:::{.text-center}
:::{.columns}
:::{.column width="33%"}
![&nbsp;](images/sponsors/ngi_enrichers-logo.png)
:::
:::{.column width="33%"}
![&nbsp;](images/sponsors/logo-concordia-university-montreal.png)
:::
:::{.column width="33%"}
![&nbsp;](images/sponsors/mitacs-logo.png)
:::
:::
:::
:::

---

## The source code

:::::::::::::: {.columns}

::: {.column width="40%"}
Our **source code is on Gitlab** under _open source_ license

Do not hesitate to report _bugs_ to us by suggesting [_issues_](https://gitlab.com/multi-coop/datami-project/datami/-/issues), or to give ideas for new features on our [ _roadmap_](https://gitlab.com/multi-coop/datami-project/datami/-/boards/4736577)

A [mirror repo](https://github.com/multi-coop/datami) is also automatically synced to Github

Don't forget to leave a little ⭐️ if you like the project!
:::

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-gitlab.png)
:::
::: {.text-center}
💻 [Repo Gitlab](https://gitlab.com/multi-coop/datami-project/datami)

![](https://img.shields.io/badge/dynamic/json?color=turquoise&label=gitlab%20stars%20%E2%98%85&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39268471)

:::
:::

::::::::::::::

---

## Existing open source audits

**We were audited in late 2022
by the cabinet Smile**
<br>
on the open source dimensions of the Datami project. 


:::::: {.columns}

::: {.column width=30%}

::: {.text-micro}
Smile helped us formalising different recommandations 
about 
<b>
Datami's open source strategies, its functional roadmap, or even about the project's governance.
</b>

::: {.text-center}
![](images/audits/smile-logo.png)
:::

All the documents related to this audit can be requested 
by contacting us at : 
[contact@multi.coop](mailto:contact@multi.coop)
:::

:::

::: {.column width=70%}
![&nbsp;](images/audits/smile-01.png)
:::

::::::


---

## The technical documentation site

:::::::::::::: {.columns}

::: {.column width="40%"}
Also visit our dedicated **documentation site**

The site is translated into French and English

You will find different sections there: technical principles, tutorials, examples, description of the different widgets and their configuration elements...

:::

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-doc-website.png)
:::
::: {.text-center}
🔧 [datami-docs.multi.coop](https://datami-docs.multi.coop)
:::
:::

::::::::::::::

---

## The _stack_

:::::::::::::: {.columns}

::: {.column width="40%"}
The technical _stack_ is **entirely composed of _open source_ libraries**
:::

::: {.column width="60%"}
- [`Vue.js (v2.x)`](https://v2.vuejs.org/v2/guide)
- [`VueX`](https://vuex.vuejs.org/)
- [`vue-custom-element`](https://github.com/karol-f/vue-custom-element)
- [`gray-matter`](https://www.npmjs.com/package/gray-matter)
- [`Showdown`](https://www.npmjs.com/package/showdown) + [`showdown-table extension`](https://github.com/showdownjs/table-extension#readme)
- [`Bulma`](https://bulma.io/) + [`Buefy`](https://buefy.org/)
- [`Material Design`](https://materialdesignicons.com/)
- [`Fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
- [`JSDiff`](https://github.com/kpdecker/jsdiff)
- [`Diff2html`](https://www.npmjs.com/package/diff2html)
- [`MapLibre`](https://maplibre.org)
- [`ApexCharts`](https://apexcharts.com)
:::

::::::::::::::
