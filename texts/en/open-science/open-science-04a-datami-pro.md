
# Datami PRO (teasing)

![&nbsp;](images/datami-pro/intro.png)

---

## Why Datami PRO ?

Datami PRO will be the platform to simplify the opening datasets for individual or organisations, without the need for any tech skills.

- **Autonomous data management**

- **Step-by-step opening process**

- **Upload structured & enriched datasets to forges** (Github, Gitlab...)

- **Autonomous configuration of Datami widgets**

- **Plans for every need and type of organisation**

---

## First interfaces

:::r-stack

::: {.fragment .no-mt .h-75 .img-border .current-visible}
![Landing page](images/datami-pro/datamipro-home.png)

:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Dataset edition](images/datami-pro/datamipro-dataset.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Dataset's columns settings (basic schema)](images/datami-pro/datamipro-dataset-settings.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Dataset's columns settings (schema details)](images/datami-pro/datamipro-dataset-settings-dialog.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Dataset's metadata edition](images/datami-pro/datamipro-dataset-settings-metadata.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Dataset converted into a Datami widget](images/datami-pro/datamipro-widget.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Passwordless authentication](images/datami-pro/datamipro-login.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in}
![Datami PRO plans](images/datami-pro/datamipro-plans.png)
:::

:::

---

## Diagram of developments

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
Elements in <span class="text-secondary">orange</span> symbolize the main milestones for Datami PRO roadmap.

Elements colored in **turquoise** symbolize features already existing in Datami.
:::

::: {.column width=50% .mt-default}
- <soan class="text-secondary">A</soan> : <b>Online widget configuration</b>
- <soan class="text-secondary">B</soan> : <b>Manage contribution widget</b>
- <soan class="text-secondary">C</soan> : <b>More data sources</b>
:::

::::::

:::

::: {.no-mt .h-50}
![&nbsp;](images/roadmaps/archi-devs-roadmap-en.png)
:::

