
#

::: {.text-center}
**Thanks for your attention !**
:::

<br>

::::::::::::: {.columns}

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::
:::

::: {.column width="20%"}
::: {.text-micro .text-center}
[Datami](https://datami.multi.coop)

is a project led by the cooperative

[multi](https://multi.coop)
:::
:::

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)
:::
:::

:::::::::::::


::::::::::::: {.columns}

::: {.column width="30%"}
::: {.img-mini  .no-mt .no-caption}
![&nbsp;](images/sponsors/sponsors.png)
:::
::: {.img-mini .no-mt }
![&nbsp;](images/sponsors/ngi_enrichers-logo.png)
:::
:::

:::::::::::::

::: {.text-center}
**contact@multi.coop**
:::

---

::: {.text-center}
[Slides url](https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-en-open-science.html#/title-slide)
:::

::: {.text-center}
[Slides source](https://gitlab.com/multi-coop/datami-project/datami-slides-fr)
:::