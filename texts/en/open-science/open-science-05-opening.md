
# Openings & propositions

<i class="ri-message-2-line"></i>
**Labs / test**

::: {.text-micro}
We can <b>accompany researchers/labs</b> in publishing their datasets, according to their specific needs.
:::

<i class="ri-survey-line"></i>
**Grants**

::: {.text-micro}
With the university's support can build a <b>proposal for international grants</b> (
[NGI Search](https://www.ngisearch.eu/view/Main/OpenCalls), 
[NGI Sargasso](https://ngisargasso.eu/)
, ...) to develop a platform helping researchers publishing their data for open science. 
:::

<i class="ri-login-circle-line"></i>
**Datami PRO early adopters plan**

::: {.text-micro}
We propose to the university and to researchers <b>several early plans</b> for Datami PRO.
:::

<i class="ri-question-answer-line"></i>
**Open discussion**

::: {.text-micro}
<b>What is your practice and/or your constraints</b> as researcher or bibliotarian when it comes to open science ?
:::
