---
title: Researcher's datasets and Open Science
# ![](images/logos/logo-DATAMI-rect-colors.png)
# ref: https://www.ow2con.org/view/2023/Abstract_Community_Day#14061510
subtitle: |
  <br>
  <strong>
    A LONG, BUMPY BUT OPEN ROAD
  </strong>
  <br>
  <strong>
    TOWARD DIGITAL COMMONS
  </strong>
  <br>
  <br>
  <br>
# <span class="text-micro">
#   A customizable open source widget 
#   to visualize and edit open datasets
# </span><br>
# <span class="text-micro">
#   without any other backend than Github or Gitlab
# </span><br>
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _fullstack developer / lead dev of Datami_
  </span><br>
  <span class="text-micro text-author-details">
    _co-founder of [tech cooperative multi](https://multi.coop)_
  </span><br>
  <span class="text-micro text-author-details">
    _julien.paris@multi.coop_
  </span><br><br>
  <span class="img-cover">
    ![](images/logos/logo-MULTI-colored-063442-02-w120.png)
  </span>
  <span class="img-cover">
    ![](images/sponsors/ngi_enrichers-logo.png)
  </span>
'
date: '
  <a href="https://multi.coop?locale=en" target="_blank">
    https://multi.coop
  </a><br>
  <span class="emph-line">2024 February 19th </span> / 
  Plan by pressing <span class="text-nano">`esc`</span><br>'
title-slide-attributes:
  data-background-image: "images/open-science/concordia.png"
  # data-background-image: "images/logos/logo-DATAMI-rect-colors.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 8%, auto 5%"
  data-background-position: "right 10% top 40%, right 4% top 40%"
---
