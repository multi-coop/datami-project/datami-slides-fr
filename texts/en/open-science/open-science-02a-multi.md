
# The multi cooperative

:::::::::::::: {.columns}

::: {.column width="60%"}
Our cooperative contributes to the development of **digital commons** and associated services, by bringing together a community of professionals working for a **digital of general interest**
:::

::: {.column width="40%"}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)

::: {.text-center}
🚀 [https://multi.coop](https://multi.coop)
:::

:::

::::::::::::::

## Our clients

![&nbsp;](images/clients/multi-clients.png)

---

## Our methods

#### **Project management**

::: {.text-micro}
At multi we chose to engage in <b>agile projects management</b>, while mobilizing our core technical skills transversaly.
:::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column .text-center width=20%}
<b>⭐️ Product owner</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data science</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data engineering</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Web development</b>

:::

::::::
:::

<hr>

<br>

#### **Data auditing & developments**

::: {.text-micro}
<b>Every project we engage in has a data dimension</b>. As our clients are mainly public organisations the datasets we help to build or valorize need to be prepared for open data publishing.
:::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column .text-center width=20%}
<b>⭐️ Data management</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data preprocessing</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data enrichment</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data publishing</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data valorisation</b>

:::
::::::
:::

<hr>

---

## Data lifecycle guidelines

::::::{.columns}

:::{.column width="35%"}
Since their creation [Datactivist](https://datactivist.coop/en/) shared a lot of guidelines we are glad to adopt in the projects and developments we engage in.

<br>

::: {.text-micro}
Datactivist is a fellow French cooperative specialized in data consulting.
:::

:::{.img-nano}
![&nbsp;](images/misc/datactivist/Logo_Datactivist.png)
:::

:::

:::{.column width=75%}

:::{.no-mt .no-caption .h-60}
![&nbsp;](images/misc/datactivist/data-lifecycle.png)
:::


:::

:::::::

---

## Our team (aka the Multeam)

::: {.no-mt .no-pt .h-50}
![&nbsp;](images/team/multeam.png)
:::
