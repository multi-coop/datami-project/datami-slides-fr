
# **Goal #3**<br> Exploring use cases<br>for new open data communities

**Empower communities by providing Datami for new use cases**


::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}

Since its beginning mid-2022 Datami users list
accounts for a more than dozen organisations,
some public other private, 
each structure having both similar and specific expectations.

<b>
Specific needs 
</b>
are those about representing correctly a dataset
with a editorial point of view,
a customized process of curation depending on the data properties
and data producers needs. 

<b>
Similar needs
</b>
are those about having simple visualisations, 
an affordable solution, lower the level technical knowledge 
both for contributors as much for data producers themselves.

:::

::: {.column width=50%}

Datami was developed step by step, each step corresponding 
to a 
<b>
"fit" between a roadmap and new actors needs 
in term of open data valorisation towards their communities. 
</b>
We believe the best way to develop a digital project lays in 
iterating as we improve the solution to new users.

To perfect Datami we found no better way 
than building each of its features according to new use cases, 
new actors eager to open their data to their audiences.

New use cases bring new challenges, each in their own way.

<b>
For the Datami project our goal is to answer to new needs
but within a frame of a global roadmap while doing so.
</b>
:::

::::::

:::

---

## Users & contributors profiles

We identified three main user profiles for Datami, <br>
depending on their role in 
<b>
producing open data as inclusive digital commons.
</b>

:::::: {.columns .text-justify}

::: {.column width="33%"}

**Data producers**

::: {.text-micro}
As we mentionned throughout this document
we target small and local organisations
with limited resources, 
but in dire need to produce and share information
about the topic they specialized in.  

Due to their lack of resources 
they also need their communities 
to participate in the consolidation of the 
datasets published in the first place.
Data producers are ultimatlely responsible
of the data content and publication.

<b>
Thus data producers first expectation is
to encourage their communities to contribute,
but also to be able to moderate these contributions.
</b>

:::

:::

::: {.column width="33%"}

**Citizen**

::: {.text-micro}
By citizen we mean here any invidual 
potentially concerned by informations published
by the data producers.

Using such a generic term ("citizen") we want to highlight the fact we consider 
that any individual should have the right 
to contribute to informations of general interest, 
whomever this information is produced by.

<b>
This consideration implies to build interfaces 
accessible to any individual, regardless of their
sociological or technological background. 
</b>
:::

:::

::: {.column width="33%"}

**Low level**<br>**contributors**

::: {.text-micro}
This family of users have more technical background than average, 
but their field of technical expertise can vary.

Some could posses a level of know-how in vue.js for instance, 
and contribute directly to Datami's source code.

Others can have knowledge on data models or standards,
such as Frictionless' communities,
and may contribute to help data producers
in structuring their data 
or make it more interoperable.

<b>
To adress and include this profile 
we must pay a special attention 
to its technical documentation.
</b>
:::

:::

::::::

---

## Our first users

**Since late 2022 we implemented Datami for a dozen organisations,**<br>**some public and other private**

::: {.text-micro}

Each organisation may at the time have had one or several datasets to expose and share.
<br>
The needs we covered were related to data-visualisation, cartography, embeddibility, reproductibility... 
<br>
<b>
The expression of needs along the way participated to build our roadmap,
</b>
feature demand by feature demand.

:::::: {.columns}

::: {.column width=35%}

Current use cases include datasets focusing on a variety of topics of public interest :

- Indicators about places providing public digital services (France services) ;
- Indicator on digital parental fragility ;
- Open data producers ;
- Third places characteristics ;
- Alternative mobility projects ...

More examples on the [Datami website](https://datami.multi.coop/examples?locale=en)

:::

::: {.column .img-no-margin  width=65%}
![](images/clients/clients.png)
:::

::::::

:::


---

## Analytics

**We use the open souce solution Matomo as our main analytics platform.**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
The metrics are visible for everyone 
on this [link](https://multi.matomo.cloud/index.php?module=CoreHome&action=index&idSite=1&period=range&date=previous30#?period=range&date=2022-08-28,2023-03-25&category=Dashboard_Dashboard&subcategory=1&idSite=1)
and are set to preserve the privacy of Datami's users.

Our metrics shows 
<b>
a slow but continuous trend of growth in the using of Datami
</b>
amongst our first users.
:::

::: {.column width=50%}

We also observed the peaks in our metrics may be related
with events - either organized by multi or by stakeholders -
boosting up the visibility of datasets exposed with Datami
towards their target audiences.
:::

::::::

:::

::: {.text-center .img-pdf}
![](images/matomo/matomo-datami_widget-04.png)
:::

---

## Prospection strategy

**Finding new use cases and promoting Datami**

::: {.text-micro}

We are actively participating to several events to promote Datami and looking out for new use cases.

:::::: {.columns .text-justify}

::: {.column width=50%}
For instance we presented Datami to the 
"[NEC](https://numerique-en-communs.fr/la-programmation-2022/boite-a-outils-et-bonnes-pratiques-de-la-recolte-au-partage-pour-saisir-manipuler-analyser-et-partager-la-donnee/)" 
event (_Digital In Commons_) in Lens city in late 2022, 
we will organize a workshop during the 
"[Journées du Logiciel Libre](https://jdll.org/user/pages/03.programme/JdLL_2023-programme.pdf)" 
(_FLOSS Days_)
in Lyon in April 2023, 
or to the 
[OW2 annual conferences](https://www.ow2con.org/view/2023/) in Paris next June.
:::

::: {.column width=50%}
We are also directly in contact with several national organisations in France, 
and currently working with them to find ways implement Datami 
given their needs.
:::

::::::

:::

<b>
With the act of applying to this grant we hope we could make a difference 
by devoting more resources to this market exploration.
</b>



