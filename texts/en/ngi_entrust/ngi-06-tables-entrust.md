
## Priorities

::: {.text-micro .text-justify}

:::::: {.columns}

::: {.column width=50%}

In 2022 the Datami project benefited from a support of 62 k€ from the ANCT, and was also co-funded by several private entities during missions up to 35 k€. 

These fundings allowed us to build most of its core features, such as providing customazible frontend web components, the ability to dialog with Gitlab’s / Github’s / mediawiki’s APIs to retrieve data and send contributions, and its ability to associate data models to open datasets.

However Datami is still a project in its earliest phases of developments. 
We apply for the 
NGI Zero entrust program 
because 
**we identified some core features still in need of being developped**
so Datami could fulfill its scope of promises.

From the [global roadmap](https://datami.multi.coop/roadmap/?locale=en) of the Datami project we established a sub-roadmap specific to 
NGI Zero entrust program
we consider coherent with its main goals.
:::

::: {.column width=50%}
The NGI zero entrust grant 
will be used exclusively in tasks of 
**research and development**, including :

- Developping a WYSIWYG interface on top of Datami <b>to easily configure a Datami widget from scratch</b> for end users with low technical knowledge ;

- Developping a WYSIWYG interface on top of Gitlab / Github <b>to moderate and manage pull requests</b> for end users with low technical knowledge ;

- <b>Better integration of Frictionless standards</b> ;

- Experiments with <b>new use cases and roadmap management</b> with the stakeholders.

<!-- - <b>Improving the ability to interact with third-party API</b> of services such as Github, Gitlab or other APIs for retrieving data, data structure, pushing commits or PR... -->
:::

::::::

:::


---


## Detailed budget

---

### Detailed budget & roadmap

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>12 d.</b>|<b>10 800 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>5 d.</b>|<b>4 500 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>10 d.</b>|<b>9 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|<b>5 400 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>|<b>5 400 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>4 d.</b>|<b>3 600 €</b>|
|TECH|Implementing|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>3 d.</b>|<b>2 700 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>3 d.</b>|<b>2 700 €</b>|
|BIZDEV|Adopting|<b>Design</b>|UX workshops|🟠|▪️▪️|<b>5 d.</b>|<b>4 500 €</b>|
||||||<b>TOTAL</b>|**54 d.**|**48 600 €**|

--- 

### Budget analysis

::: {.text-center} 
**Priority · _vs_ · Difficulty**
:::

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
    In days
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high| 22 days| 5 days| 6 days|<b> 33 days</b>|
|🟠 medium|  days| 12 days|  days|<b> 12 days</b>|
|🟡 low| 9 days|  days|  days|<b> 9 days</b>|
|**TOTAL**|<b> 31 days</b>|<b> 17 days</b>|<b> 6 days</b>|**54 days**|

:::


::: {.column width=50%}

::: {.text-center}
<b>
    In euros    
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|19 800 €|4 500 €|5 400 €|<b>29 700 €</b>|
|🟠 medium|  €|10 800 €|  €|<b>10 800 €</b>|
|🟡 low|8 100 €|  €|  €|<b>8 100 €</b>|
|**TOTAL**|<b>27 900 €</b>|<b>15 300 €</b>|<b>5 400 €</b>|**48 600 €**|

:::

::::::

<br>

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
    In proportions
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|40.7%|9.3%|11.1%|<b>61.1%</b>|
|🟠 medium|0.0%|22.2%|0.0%|<b>22.2%</b>|
|🟡 low|16.7%|0.0%|0.0%|<b>16.7%</b>|
|**TOTAL**|<b>57.4%</b>|<b>31.5%</b>|<b>11.1%</b>|**100.0%**|

:::

::::::






# Provisional timelines

**A lean approach for iterative and incremental developments**

---

## Timelines

<br>

::: {.text-center}

<b>
The following timelines are
to be considered as only 
provisional.
</b>

These timelines may be adapted depending on
<br> 
the context, our resources, 
and the priority of demands 
<br>
emerging from  new use cases. 
:::


---

### Timeline

::: {.ngi-timeline}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|
|TECH|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>12 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|||||
|TECH|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>10 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|||<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|BIZDEV|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>3 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|BIZDEV|<b>Design</b>|UX workshops|🟠|▪️▪️|<b>5 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||

:::
