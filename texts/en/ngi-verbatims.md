
### Marion Letorey

The Incubateur des territoires is dedicated to providing solutions and services to communities, no matter their size or means. The Incubateur des territoires chose to support Datami mostly because of its unique approach on relationship with data. On the one hand as an open source by design solution, and on the other hand as low-code spirited application. When funding Datami, we noticed an opportunity to contribute to open source tools for more human relations to data through a seamless and simple interface.