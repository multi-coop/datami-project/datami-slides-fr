---
title: Datami![](images/logos/logo-DATAMI-rect-colors.png)
# ref: https://www.ow2con.org/view/2023/Abstract_Community_Day#14061510
subtitle: |
  <br>
  <strong>
    SHARE, SHOW AND CONTRIBUTE
  </strong>
  <br>
  <strong>
    TO DIGITAL COMMONS
  </strong>
  <br>
  <br>
  <span class="text-micro">
    A customizable open source widget 
    to visualize and edit open datasets
  </span><br>
  <span class="text-micro">
    without any other backend than Github or Gitlab
  </span><br>
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _fullstack developer / lead dev of Datami_
  </span><br>
  <span class="text-micro text-author-details">
    _co-founder of [tech cooperative multi](https://multi.coop)_
  </span><br>
  <span class="text-micro text-author-details">
    _julien.paris@multi.coop_
  </span><br>
  <span class="img-cover">
    ![](images/logos/logo-MULTI-colored-063442-02-w120.png)
  </span>
'
date: '
  <a href="https://datami.multi.coop?locale=en" target="_blank">
    https://datami.multi.coop
  </a><br>
  <span class="emph-line">June 14th 2023</span> / 
  Plan by pressing <span class="text-nano">`esc`</span><br>'
title-slide-attributes:
  data-background-image: "images/sponsors/ow2con23_borderL.png"
  # data-background-image: "images/logos/logo-DATAMI-rect-colors.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 5%"
  data-background-position: "right 10% top 40%, right 4% top 40%"
---

#

::: {.text-center}

::: {.img-medium}
![&nbsp;](images/QR-code-ow2.png)
:::

**https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-en-ow2.html**
:::

# Help your publics with your data

::: {.text-center style="font-size:2em"}
**What are the challenges ?**
:::

## Challenge #1 

### Make your data **intelligible**

---

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/messy-excel-02.png)
:::
:::

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ODF-map-all.png)
:::
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
Your raw data <br>**without** Datami
:::
:::

::: {.column width="50%"}
::: {.text-center}
Your data enhanced <br>**with** Datami
:::
:::
::::::::::::::

---

## Challenge #2 

### Make **updating** and **contributing** easier

---

:::::::::::::: {.columns}

::: {.column width="40%"}
Handling data is a matter of habits, facilitating their manipulation for the greatest number means that you have to **adapt to the habits** of the greatest number

:::fragment
To allow everyone to easily contribute to the data, the view in the form of a table is still the most commonly adopted today.
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/table-view-01.png)
:::
:::

::::::::::::::

---

## Challenge #3

### Make all of the above **with a tight budget**

---

When you are a small or medium-sized structure (association, community, etc.) and you produce data of general interest, it can be complicated to highlight them on your site or to call on your community to put them up to date

::: {.text-center}
**Lack of resources, lack of skills, lack of time...**
:::

:::::::::::::: {.columns}
::: {.column width="30%"}
:::fragment
::: {.text-justify}
The cost of the usual technical solutions for sharing / viewing / contributing to data is often explained by the technical **complexity** of these functionalities
:::
:::
:::
::: {.column width="30%"}
:::fragment
::: {.text-justify style="padding: 0 1.5em 0 1.5em"}
Another cost is related to the need to set up dedicated **servers** in _backend_ or very specific configurations
:::
:::
:::
::: {.column width="30%"}
:::fragment
::: {.text-justify}
The **maintenance** of applications or servers often generates significant costs
:::
:::
:::
::::::::::::::

:::fragment
Datami's original architecture makes it possible to **get rid of a large part of these _backend_ server costs** while allowing customization dataset by dataset
:::
