
# Credits

## A project by the multi cooperative

:::::::::::::: {.columns}

::: {.column width="60%"}
Our cooperative contributes to the development of **digital commons** and associated services, by bringing together a community of professionals working for a **digital of general interest**
::: 

::: {.column width="40%"}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)

::: {.text-center}
🚀 [https://multi.coop](https://multi.coop)
:::

:::

::::::::::::::

---

## Our first users

![&nbsp;](images/clients/clients.png)

---

## Our sponsors

Datami was **laureate of the Plan France Relance 2022**<br>
and has benefited from the support of the following organizations

![&nbsp;](images/sponsors/sponsors.png)

#

::: {.text-center}
**Thanks for your attention !**
:::

<br>

::::::::::::: {.columns}

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::
:::

::: {.column width="20%"}
::: {.text-micro .text-center}
[Datami](https://datami.multi.coop)

is a project led by the cooperative

[multi](https://multi.coop)
:::
:::

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)
:::
:::

:::::::::::::


::::::::::::: {.columns}

::: {.column width="30%"}
::: {.img-mini}
![&nbsp;](images/sponsors/sponsors.png)
:::
:::

:::::::::::::

::: {.text-center}
**contact@multi.coop**
:::

---

::: {.img-mini}
![&nbsp;](images/sponsors/ow2con23_borderL.png)
:::

::: {.text-center}
[Slides source](https://gitlab.com/multi-coop/datami-project/datami-slides-fr)
:::