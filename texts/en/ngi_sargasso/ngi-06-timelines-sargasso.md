
# Provisional timelines

**A lean approach for iterative and incremental developments**

---

## Timelines

<br>

::: {.text-center}

<b>
The following timelines are
to be considered as only 
provisional.
</b>

Some tasks listed in the Gantt chart,
and estimated to a certain amount of work days,
are considered to ventilated on one or several quarters.

:::{.text-micro}
These timelines may be adapted depending on
<br> 
the context, our resources,
and the priority of demands
<br>
emerging from  new use cases.
:::
:::

---

### Gantt chart

::: {.ngi-timeline .ngi-timeline-mini}

|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2024|T2 2024|T3 2024|T4 2024|
|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|
|<b>Privacy frameworks</b>|Implementation of international legal frameworks and schemas|🔴|▪️▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Transparency performance scheme and consent protocol|🔴|▪️▪️|<b>3 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Signalling interface (Privacy Broadcsasting) for digital and physical assessments|🔴|▪️|<b>5 d.</b>||||<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Receipt and record generation for framework to the schema|🔴|▪️|<b>4 d.</b>||||<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Open source technology and community identification and roadmap|🔴|▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Physical Access and Surveillance Risk assessment |🟠|▪️|<b>1 d.</b>||||<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Physical Access and Surveillance Code of Practice |🟠|▪️|<b>1 d.</b>||||<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Physical access and surveillance open source controller and peripheral device  contribution|🟠|▪️|<b>1 d.</b>||||<span class="cell-bg">x</span>|
|<b>Backend</b>|Dedicated backend to process consent protocol and git<>user requests|🔴|▪️▪️|<b>1 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Backend</b>|Open data API|🟡|▪️▪️|<b>5 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Backend</b>|Third party services connectors (GAIA, IDS...)|🟡|▪️▪️|<b>3 d.</b>||||<span class="cell-bg">x</span>|
|<b>Backoffice - widget configuration</b>|Interactive interface + preview|🟠|▪️▪️▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Backoffice - widget configuration</b>|Save new config to git repo|🟠|▪️▪️|<b>5 d.</b>|||<span class="cell-bg">x</span>||
|<b>Backoffice - manage contributions</b>|Authentication processes|🔴|▪️▪️▪️|<b>7 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Backoffice - manage contributions</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>||
|<b>Backoffice - manage contributions</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>2 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>3 d.</b>|||<span class="cell-bg">x</span>||
|<b>Data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>UI customization</b>|Accessibility|🟠|▪️▪️|<b>3 d.</b>||||<span class="cell-bg">x</span>|
|<b>Deployment / CI</b>|Deployment and CI/CD setup|🔴|▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Refactoring</b>|JS package for Git interactions|🟡|▪️▪️▪️|<b>3 d.</b>||||<span class="cell-bg">x</span>|
|<b>Refactoring</b>|Typescript implementation|🟠|▪️▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Refactoring</b>|Migration to Vue3|🟠|▪️▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|||
|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>2 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Project management</b>|Roadmap management|🟠|▪️▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Business model</b>|Implementation of payment portal|🔴|▪️|<b>2 d.</b>||<span class="cell-bg">x</span>|||
|<b>Business model</b>|Outreach to regulators / EU|🔴|▪️▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Business model</b>|Regulator review of code of conduct |🔴|▪️▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|

:::
