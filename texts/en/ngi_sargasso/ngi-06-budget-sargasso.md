
## Budget

::: {.text-center}
**Regrouped by category and milestones**
:::

|**Category**|**Milestones**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|------------|------|------|---:|---:|
|Implementing|<b>Privacy frameworks</b>|🔴 high|▪️▪️▪️ hard|<b> 21 days</b>|<b>17 850 €</b>|
|Implementing|<b>Backend</b>|🔴 high|▪️▪️▪️ hard|<b> 9 days</b>|<b>7 650 €</b>|
|Implementing|<b>Backoffice - widget configuration</b>|🟠 medium|▪️ easy|<b> 8 days</b>|<b>6 800 €</b>|
|Implementing|<b>Backoffice - manage contributions</b>|🟡 low|▪️▪️ medium|<b> 17 days</b>|<b>14 450 €</b>|
|Implementing|<b>Data management</b>|🟡 low|▪️▪️ medium|<b> 15 days</b>|<b>12 750 €</b>|
|Implementing|<b>Data interaction</b>|🟠 medium|▪️ easy|<b> 4 days</b>|<b>3 400 €</b>|
|Implementing|<b>UI customization</b>|🟠 medium|▪️▪️▪️ hard|<b> 3 days</b>|<b>2 550 €</b>|
|Implementing|<b>Deployment / CI</b>|🔴 high|▪️ easy|<b> 5 days</b>|<b>4 250 €</b>|
|Implementing|<b>Refactoring</b>|🟡 low|▪️ easy|<b> 13 days</b>|<b>11 050 €</b>|
|Implementing|<b>Tests</b>|🔴 high|▪️▪️▪️ hard|<b> 7 days</b>|<b>5 950 €</b>|
|OnBoarding|<b>Project management</b>|🔴 high|▪️▪️▪️ hard|<b> 5 days</b>|<b>4 250 €</b>|
|Adopting|<b>Business model</b>|🟠 medium|▪️▪️ medium|<b> 10 days</b>|<b>8 500 €</b>|
||<b>TOTAL</b>|||**117 d.**|**99 450 €**|

---

## Detailed budget

---

### Detailed budget & roadmap - 1/2

|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost**|
|:---:|---------|------------|:---:|---|---:|---:|
|Implementing|<b>Privacy frameworks</b>|Implementation of international legal frameworks and schemas|🔴|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Privacy frameworks</b>|Transparency performance scheme and consent protocol|🔴|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Privacy frameworks</b>|Signalling interface (Privacy Broadcsasting) for digital and physical assessments|🔴|▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Privacy frameworks</b>|Receipt and record generation for framework to the schema|🔴|▪️|<b>4 d.</b>|<b>3 400 €</b>|
|Implementing|<b>Privacy frameworks</b>|Open source technology and community identification and roadmap|🔴|▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Privacy frameworks</b>|Physical Access and Surveillance Risk assessment |🟠|▪️|<b>1 d.</b>|<b>850 €</b>|
|Implementing|<b>Privacy frameworks</b>|Physical Access and Surveillance Code of Practice |🟠|▪️|<b>1 d.</b>|<b>850 €</b>|
|Implementing|<b>Privacy frameworks</b>|Physical access and surveillance open source controller and peripheral device  contribution|🟠|▪️|<b>1 d.</b>|<b>850 €</b>|
|Implementing|<b>Backend</b>|Dedicated backend to process consent protocol and git<>user requests|🔴|▪️▪️|<b>1 d.</b>|<b>850 €</b>|
|Implementing|<b>Backend</b>|Open data API|🟡|▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Backend</b>|Third party services connectors (GAIA, IDS...)|🟡|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Backoffice - widget configuration</b>|Interactive interface + preview|🟠|▪️▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Backoffice - widget configuration</b>|Save new config to git repo|🟠|▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Backoffice - manage contributions</b>|Authentication processes|🔴|▪️▪️▪️|<b>7 d.</b>|<b>5 950 €</b>|
|Implementing|<b>Backoffice - manage contributions</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>4 d.</b>|<b>3 400 €</b>|
|Implementing|<b>Backoffice - manage contributions</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|<b>5 100 €</b>|

---

### Detailed budget & roadmap - 2/2

|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost**|
|:---:|---------|------------|:---:|---|---:|---:|
|Implementing|<b>Data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>2 d.</b>|<b>1 700 €</b>|
|Implementing|<b>Data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>4 d.</b>|<b>3 400 €</b>|
|Implementing|<b>UI customization</b>|Accessibility|🟠|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Deployment / CI</b>|Deployment and CI/CD setup|🔴|▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Refactoring</b>|JS package for Git interactions|🟡|▪️▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Refactoring</b>|Typescript implementation|🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Refactoring</b>|Migration to Vue3|🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>|<b>5 950 €</b>|
|OnBoarding|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>2 d.</b>|<b>1 700 €</b>|
|Adopting|<b>Project management</b>|Roadmap management|🟠|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Adopting|<b>Business model</b>|Implementation of payment portal|🔴|▪️|<b>2 d.</b>|<b>1 700 €</b>|
|Adopting|<b>Business model</b>|Outreach to regulators / EU|🔴|▪️▪️▪️|<b>4 d.</b>|<b>3 400 €</b>|
|Adopting|<b>Business model</b>|Regulator review of code of conduct |🔴|▪️▪️▪️|<b>4 d.</b>|<b>3 400 €</b>|
|||||<b>TOTAL</b>|**117 d.**|**99 450 €**|


---

## Budget analysis

::: {.text-center} 
**Priority · _vs_ · Difficulty**
:::

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
    In days
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high| 19 days| 9 days| 25 days|<b> 53 days</b>|
|🟠 medium| 18 days| 16 days| 3 days|<b> 37 days</b>|
|🟡 low| 14 days| 13 days|  days|<b> 27 days</b>|
|**TOTAL**|<b> 51 days</b>|<b> 38 days</b>|<b> 28 days</b>|**117 days**|

:::


::: {.column width=50%}

::: {.text-center}
<b>
In euros
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|16 150 €|7 650 €|21 250 €|<b>45 050 €</b>|
|🟠 medium|15 300 €|13 600 €|2 550 €|<b>31 450 €</b>|
|🟡 low|11 900 €|11 050 €|  €|<b>22 950 €</b>|
|**TOTAL**|<b>43 350 €</b>|<b>32 300 €</b>|<b>23 800 €</b>|**99 450 €**|

:::

::::::

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
In proportions
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|16.2%|7.7%|21.4%|<b>45.3%</b>|
|🟠 medium|15.4%|13.7%|2.6%|<b>31.6%</b>|
|🟡 low|12.0%|11.1%|0.0%|<b>23.1%</b>|
|**TOTAL**|<b>43.6%</b>|<b>32.5%</b>|<b>23.9%</b>|**100.0%**|

:::

::::::
