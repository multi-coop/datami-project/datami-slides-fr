---
title: '
  <strong>NGI SARGASSO OPEN CALL \#2</strong>
  <!-- ![](images/logos/logo-DATAMI-rect-colors.png) -->
  '
subtitle: '
  <strong>0PN DPT 4 ALL</strong><br>
  <b>Digital Privacy Transparency Assessments, Listings<br>
  and Ledgers for Legal and Regulatory Adequacy <br>
  of International Data Transfers <br>
  (Quebec, CAN – France) 
  </b>
  <br>
  <br>
  Gantt chart & provisional budget'
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _fullstack developer_
  </span><br>
  <span class="text-micro text-author-details">
    _co-founder of the digital cooperative [multi.coop](https://multi.coop)_
  </span><br>
  <span class="img-cover">
    ![](images/logos/logo-MULTI-colored-063442-02-w120.png)
    ![](images/sponsors/0pn+green.png)
    ![](images/sponsors/OC-brand-logo-TM.png)
    ![](images/sponsors/SurveillanceTrustLogo.jpg)
    ![](images/sponsors/cropped-IDmachineLogo.png)
    ![](images/sponsors/Logo_Human_Colossus.png)
  </span>
'
date: '
  <span class="emph-line">December 18th 2023</span>
  <br>Navigation with <i class="ri-drag-move-2-fill"></i> / For plan press <span class="text-nano">`echap`</span>
  '
title-slide-attributes:
  data-background-image: "images/sponsors/ngi_sargasso-logo.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 5%"
  data-background-position: "right 10% top 45%, right 4% top 40%"

---
