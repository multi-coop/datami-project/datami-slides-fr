

<!-- ## Priorities

::: {.text-micro .text-justify}

:::::: {.columns}

::: {.column width=50%}

In 2022 the Datami project benefited from a support of 62 k€ from the ANCT, and was also co-funded by several private entities during missions up to 35 k€.

These fundings allowed us to build most of its core features, such as providing customazible frontend web components, the ability to dialog with Gitlab’s / Github’s / mediawiki’s APIs to retrieve data and send contributions, and its ability to associate data models to open datasets.

However Datami is still a project in its earliest phases of developments. 
We apply for the 
NGI Sargasso program 
because 
**we identified some core features still in need of being developped**
so Datami could fulfill its scope of promises.

From the [global roadmap](https://datami.multi.coop/roadmap/?locale=en) of the Datami project we established a sub-roadmap specific to 
NGI Sargasso program
we consider coherent with its main goals.
:::

::: {.column width=50%}
The NGI Sargasso grant 
will be used exclusively in tasks of 
**research and development**, including :

- Developping a WYSIWYG interface on top of Datami <b>to easily configure a Datami widget from scratch</b> for end users with low technical knowledge ;

- Developping a WYSIWYG interface on top of Gitlab / Github <b>to moderate and manage pull requests</b> for end users with low technical knowledge ;

- <b>Improving the ability to interact with third-party API</b> of services such as Github, Gitlab or other APIs for retrieving data, data structure, pushing commits or PR...

- <b>Better integration of Frictionless standards</b> ;

- Experiments with <b>new use cases and roadmap management</b> with the stakeholders.
:::

::::::

:::

--- -->

## Budget

::: {.text-center}
**Regrouped by category and milestones**
:::

|**Family**|**Category**|**Milestones**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|------------|------|------|---:|---:|
|TECH|Implementing|<b>Privacy frameworks</b>|🔴 high|▪️▪️▪️ hard|<b> 32 days</b>|<b>27 200 €</b>|
|TECH|Implementing|<b>Backend</b>|🔴 high|▪️▪️▪️ hard|<b> 15 days</b>|<b>12 750 €</b>|
|TECH|Implementing|<b>Backoffice - widget configuration</b>|🟠 medium|▪️ easy|<b> 8 days</b>|<b>6 800 €</b>|
|TECH|Implementing|<b>Backoffice - manage contributions</b>|🟡 low|▪️▪️ medium|<b> 22 days</b>|<b>18 700 €</b>|
|TECH|Implementing|<b>Data management</b>|🟡 low|▪️▪️ medium|<b> 15 days</b>|<b>12 750 €</b>|
|TECH|Implementing|<b>Data interaction</b>|🟠 medium|▪️ easy|<b> 6 days</b>|<b>5 100 €</b>|
|TECH|Implementing|<b>UI customization</b>|🟠 medium|▪️▪️▪️ hard|<b> 3 days</b>|<b>2 550 €</b>|
|TECH|Implementing|<b>Deployment / CI</b>|🔴 high|▪️ easy|<b> 10 days</b>|<b>8 500 €</b>|
|TECH|Implementing|<b>Refactoring</b>|🟡 low|▪️ easy|<b> 13 days</b>|<b>11 050 €</b>|
|TECH|Implementing|<b>Tests</b>|🔴 high|▪️▪️▪️ hard|<b> 7 days</b>|<b>5 950 €</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|🔴 high|▪️▪️▪️ hard|<b> 5 days</b>|<b>4 250 €</b>|
|||<b>TOTAL</b>|||**136 d.**|**115 600 €**|

---

## Detailed budget

---

### Detailed budget & roadmap

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>Privacy frameworks</b>|Implementation of international legal frameworks and schemas|🔴|▪️▪️|<b>4 d.</b>|<b>3 400 €</b>|
|TECH|Implementing|<b>Privacy frameworks</b>|Transparency performance scheme and consent protocol|🔴|▪️▪️|<b>4 d.</b>|<b>3 400 €</b>|
|TECH|Implementing|<b>Privacy frameworks</b>|Signalling interface (Privacy Broadcsasting) for digital and physical assessments|🔴|▪️|<b>8 d.</b>|<b>6 800 €</b>|
|TECH|Implementing|<b>Privacy frameworks</b>|Receipt and record generation for framework to the schema|🔴|▪️|<b>8 d.</b>|<b>6 800 €</b>|
|TECH|Implementing|<b>Privacy frameworks</b>|Open source technology and community identification and roadmap|🔴|▪️|<b>8 d.</b>|<b>6 800 €</b>|
|TECH|Implementing|<b>Backend</b>|Dedicated backend to serve git<>user requests|🔴|▪️▪️|<b>7 d.</b>|<b>5 950 €</b>|
|TECH|Implementing|<b>Backend</b>|Open data API|🟡|▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|TECH|Implementing|<b>Backend</b>|Third party services connectors (GAIA, IDS...)|🟡|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|TECH|Implementing|<b>Backoffice - widget configuration</b>|Interactive interface + preview|🟠|▪️▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|TECH|Implementing|<b>Backoffice - widget configuration</b>|Save new config to git repo|🟠|▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|TECH|Implementing|<b>Backoffice - manage contributions</b>|Authentication processes|🔴|▪️▪️▪️|<b>10 d.</b>|<b>8 500 €</b>|
|TECH|Implementing|<b>Backoffice - manage contributions</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>6 d.</b>|<b>5 100 €</b>|
|TECH|Implementing|<b>Backoffice - manage contributions</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|<b>5 100 €</b>|
|TECH|Implementing|<b>Data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|TECH|Implementing|<b>Data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>2 d.</b>|<b>1 700 €</b>|
|TECH|Implementing|<b>Data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|TECH|Implementing|<b>Data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|TECH|Implementing|<b>Data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>|<b>5 100 €</b>|
|TECH|Implementing|<b>UI customization</b>|Accessibility|🟠|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|TECH|Implementing|<b>Deployment / CI</b>|Deployment and CI/CD setup|🔴|▪️|<b>10 d.</b>|<b>8 500 €</b>|
|TECH|Implementing|<b>Refactoring</b>|JS package for Git interactions|🟡|▪️▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|TECH|Implementing|<b>Refactoring</b>|Typescript implementation|🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|TECH|Backlog|<b>Refactoring</b>|Migration to Vue3|🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|TECH|Implementing|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>|<b>5 950 €</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>2 d.</b>|<b>1 700 €</b>|
|BIZDEV|Adopting|<b>Project management</b>|Roadmap management|🟠|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
||||||<b>TOTAL</b>|**136 d.**|**115 600 €**|

---

## Budget analysis

::: {.text-center} 
**Priority · _vs_ · Difficulty**
:::

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
    In days
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high| 13 days| 14 days| 8 days|<b> 35 days</b>|
|🟠 medium| 10 days| 20 days|  days|<b> 30 days</b>|
|🟡 low| 26 days| 15 days| 2 days|<b> 43 days</b>|
|**TOTAL**|<b> 49 days</b>|<b> 49 days</b>|<b> 10 days</b>|**108 days**|

:::


::: {.column width=50%}

::: {.text-center}
<b>
    In euros    
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|11 050 €|11 900 €|6 800 €|<b>29 750 €</b>|
|🟠 medium|8 500 €|17 000 €|  €|<b>25 500 €</b>|
|🟡 low|22 100 €|12 750 €|1 700 €|<b>36 550 €</b>|
|**TOTAL**|<b>41 650 €</b>|<b>41 650 €</b>|<b>8 500 €</b>|**91 800 €**|

:::

::::::

<br>

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
    In proportions
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|12.0%|13.0%|7.4%|<b>32.4%</b>|
|🟠 medium|9.3%|18.5%|0.0%|<b>27.8%</b>|
|🟡 low|24.1%|13.9%|1.9%|<b>39.8%</b>|
|**TOTAL**|<b>45.4%</b>|<b>45.4%</b>|<b>9.3%</b>|**100.0%**|

:::

::::::
