# Summary

::: {.text-micro .summary}

- [Brief project description](#brief-project-description) <span class="summary-page">3.1</span></span>
- [NGI SARGASSO framework](#ngi-sargasso-framework)<span class="summary-page">4.1</span>
- [Challenges](#challenges)<span class="summary-page">5.1</span>
  - [Challenge #1](#challenge-1-make-updating-and-contributing-easier-and-secure) · Make updating and contributing easier and secure <span class="summary-page">6.1</span>
  - [Challenge #2](#challenge-2-make-open-data-more-intelligible) · Make open data more intelligible<span class="summary-page">7.1</span>
  - [Challenge #3](#challenge-3-build-a-solution-adapted-to-organizations-with-a-tight-budget) · Build a solution adapted to organizations with a tight budget <span class="summary-page">8.1</span>
- [Benchmark](#benchmark) <span class="summary-page">9.1</span>
- [Datami’s goals for the next months](#datamis-goals-for-the-next-months) <span class="summary-page">10</span>
  - [Goal #1](#goal-1-keep-a-global-coherence-of-a-freelibre-and-open-source-software-project) · Keep a global coherence of a FLOSS project <span class="summary-page">11.1</span>
  - [Goal #2](#goal-2-research-development-of-missing-core-features) · Research & development of missing core features <span class="summary-page">12.1</span>
  - [Goal #3](#goal-3-exploring-use-cases-for-new-open-data-communities) · Exploring use cases for new open data communities <span class="summary-page">13.1</span>
- [The team](#the-team) <span class="summary-page">14.1
- [Provisional budget](#provisional-budget) <span class="summary-page">15.1</span>
- [Provisional timelines](#provisional-timelines) <span class="summary-page">16.1</span>
- [Our sponsors and supports](#our-sponsors-in-2022-2023) <span class="summary-page">17</span>
- [Credits](#credits) <span class="summary-page">18</span>

:::
