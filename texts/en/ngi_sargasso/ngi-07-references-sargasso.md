
# References

---

## List of references

- [Kantara ANCR WG](https://kantara.atlassian.net/wiki/spaces/WA/overview)
- [Kantara ANCR GitHub (Transparency Scheme and AuthZ)](https://github.com/KantaraInitiative/WG-ANCR)
- [Kantara Consent Receipt v1,1 Specification](https://kantarainitiative.org/download/7902/)
- [Privacy as Expected Consent Gateway GitHub](https://github.com/PAECG/paecg.github.io)
- [Overlays Capture Architecture (OCA)](http://oca.colossi.network)
- [Datami](https://datami.multi.coop)