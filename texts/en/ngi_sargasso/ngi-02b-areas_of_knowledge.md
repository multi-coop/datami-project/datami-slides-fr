
# NGI Sargasso framework

The Datami project applies for the NGI Sargasso program in respect to its several dimensions :

- NGI Sargasso's **areas of knowledge**
- NGI Sargasso's **challenges**
- NGI Sargasso's **experiment duration**

## Areas of knowledge

- **Data sovereignty**
- **Decentralized technologies**
- **New Internet Commons**

## Challenges adressed

- **Sustainable Digital Infrastructure Across Continents** (CH1)
- **Inclusive Public Digital Spaces for Global Community Engagement** (CH3)
- **Interoperability and Standardisation Across Borders** (CH5)
- **Global Implementation of Decentralised Technologies** (CH6)

## Project duration

- **9 months experiment**

---

# Challenges

---
