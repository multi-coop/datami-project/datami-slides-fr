
# **Goal #1**<br>Research & development <br>of missing core features

**Reaching a full launchable version**

<br>

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}

Today Datami is still a project in its earliest developments. 
While we already developed a lot of its core features in mid-2022, 
some 
<b>
other key features remains to be implemented 
</b>
so Datami could jump a necessary gap
for a full market launch.
:::

::: {.column width=50%}

The new developments anticipated [in our global roadmap](https://datami.multi.coop/roadmap?locale=en) 
involve a real effort
in terms of research & development. 
Most of the features we noted over time in our roadmap
require to innovate in design and technical solutions.
:::

::::::

:::

---

## Datami technical properties

**What are the most innovative caracteristics of Datami already developped ?**

::: {.text-micro}
One of the most innovative approaches of the Datami project is its 
<b>
low-code and inclusive approach to open data.
</b>

Rather than creating a new platform from scratch, Datami relies on external platforms such as GitLab and GitHub for data storage and version control features, 
thus promoting a larger complementarity between FLOSS (Free/Libre and Open Source Software) solutions. 

This approach avoids small organisations to install and maintain servers,
and ultimately
<b>
helps making open data more affordable and transparent,
while still providing secure processes and sovereign control over the data. 
</b>

Additionally, our emphasis on structured and interoperable datasets through 
data modeling and 
<b>
adherence to standards such as Frictionless' 
</b>
is an approach that promotes a common standard for open data, 
making it easier to compare and to integrate datasets with each other
for further civic or scientific reuses.
:::

---

## The _stack_

:::::::::::::: {.columns}

::: {.column width="40%"}
The technical _stack_ is **entirely composed of _open source_ libraries**
::: 

::: {.column width="60%"}
- [`Vue.js (v2.x)`](https://v2.vuejs.org/v2/guide)
- [`VueX`](https://vuex.vuejs.org/)
- [`vue-custom-element`](https://github.com/karol-f/vue-custom-element)
- [`gray-matter`](https://www.npmjs.com/package/gray-matter)
- [`Showdown`](https://www.npmjs.com/package/showdown) + [`showdown-table extension`](https://github.com/showdownjs/table-extension#readme)
- [`Bulma`](https://bulma.io/) + [`Buefy`](https://buefy.org/)
- [`Material Design`](https://materialdesignicons.com/)
- [`Fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
- [`JSDiff`](https://github.com/kpdecker/jsdiff)
- [`Diff2html`](https://www.npmjs.com/package/diff2html)
- [`MapLibre`](https://maplibre.org)
- [`ApexCharts`](https://apexcharts.com)
:::

::::::::::::::

---

## Functional diagram  · current version

::: {.text-micro}

From a high-level point of view Datami's architecture is quite simple :

- Datasets, schemas, or configuration files are stored on third-party services such as Gitlab ; 
- Datami web components can retrieve this data and push commits and pull requests if needed.

Elements colored in **turquoise** symbolize features already existing in Datami.

::: {.no-mt .h-50}
![](images/roadmaps/archi-devs-en.png)
:::

:::

---

## Roadmap

**What are the features the grant will help to develop ?**

::: {.text-micro}
Datami is a project currently in a early phase stage of developments. 
So far we have put in place most of its core features, 
such as providing customazible frontend web components,
the ability to interact with Gitlab's / Github's / mediawiki's APIs to retrieve data and send contributions, 
and its ability to associate data models to open datasets.

<b>
That said some remaining core features are still in need to be developped
to make Datami fulfill its scope of promises. 
</b>

From the global roadmap of the Datami project 
we established a sub-roadmap specific to this program, 
roadmap we consider coherent with the program's main goals.  
:::

---

