
### The problems with implementing <br>open data for grassroot organisations 

::: {.text-micro}

:::::: {.columns}

::: {.column width=25%}
Local or small organizations are essential actors of a country's public life. 
They can either provide direct help or support to citizen, 
or intelligence and innovation about the topics they are specialized in.

By making public the information such organisation gather over time
can be a lever to raise public awareness from citizen or authorities. 
:::

::: {.column width=25%}
Yet opening data cannot be resumed to the act of publishing raw datasets on a website or on a platform.
Open data policies must target an audience, data must be kept updated regularly, 
displayed on solutions adapted to the targeted audiences and to the resources of the publisher...

In the case of small or local organisations the challenges 
laying beforehand and after in their open data policies
are severe and can often refrain them to act.
:::

::::::

:::

::: {.text-micro}

:::::: {.columns}

::: {.column width=25%}
**Lack of standardization**

Local or small organizations may struggle with
standardizing their data to make it more easily 
accessible and usable by others. 

This can limit the potential benefits of open data and 
<b>
make it difficult for others to use the data effectively.
</b>
:::

::: {.column width=25%}
**Data quality**<br>**and privacy concerns**

Local or small organizations may be hesitant
to release data due to concerns about data quality or privacy. 

They can lack the resources or expertise to 
<b>
properly manage and safeguard digital information.
</b>
:::

::: {.column width=25%}
**Lack of maintainability**<br>**or support from audiences**

Many local or small organizations may not fully 
anticipate that publishing data involves mobilizing their audiences
to help 
<b>
keeping the information up to date. 
</b>
:::

::: {.column width=25%}
**Limited resources**

Local or small organizations may not have 
the resources to establish an open data policy. 

This can include the 
<b>
lack of staff with expertise 
in open data, limited funding, and inadequate technological infrastructure
</b>
:::

::::::

:::

---

### How to help grassroot open data<br>becoming strong digital commons ?


::: {.text-micro}
Providing easy-to-use tools, designed for broad audiences, 
is one of the keys to foster open data and transparency 
from grassroot organisations : 
"code is law" and
the tooling, its interfaces and its design,
influences the kind of uses
one can have with a data.

::: {.text-center}
<b>
Open data must be though as a digital common to reach its full potential.
</b>
:::

Open data uses may be limited by the tools usually 
the data producers can afford or be able to put their hand on.  
The <b>challenges</b> for small organisations to lead strong open data policies are nothing but down to earth.
:::

:::::: {.columns}

::: {.column width=25%}
#### Challenge \#1 <br>**CONTRIBUTIBILITY**

::: {.text-micro}
Make 
<b>updating</b>
and 
<b>contributing</b>
easier for producers and contributors.

This topic includes the effort of standardization and interoperability.
:::
:::

::: {.column width=25%}
#### Challenge \#2 <br>**INTELLIGIBILITY**

::: {.text-micro}
Make open data more
<b>intelligible</b>
for any citizen.
:::
:::

::: {.column width=25%}
#### Challenge \#3 <br>**AFFORDABILITY**

::: {.text-micro}
Building solutions allowing any organisation to benefit from such features 
<b>with a tight budget</b>
:::
:::

::::::


# _Challenge #1_ <br> Make **updating** and **contributing** easier and secure


## A simplified process <br>for contributory data updates

**Helping all citizens to actively contribute to open data**

:::::::::::::: {.columns}

::: {.column width="40%"}
Handling data is a matter of habits, facilitating their manipulation for the greatest number means that you have to **adapt to the habits** of the greatest number.

<br>

::: {.text-micro}
Datami aims in that direction while providing 
<b>
secure processes
</b>
to make contributions openly and to moderate these. 

To allow everyone to easily contribute to the data, the view in the form of a table is still the most commonly adopted today.
:::

::: 

::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/table-view-01.png)
:::
:::

::::::::::::::

---

## Empower your teams and audiences <br>to improve data

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami includes a system for 
<br>
**contribution and moderation**
available in all the views.

<br>

::: {.text-micro}
Based on the _Git_ language, Datami widgets 
allow your audience to suggest improvements and contribute to the data
<b>
without the obligation to create an account.
</b>

Datami is based on external robust
services for data storage and version control
like Gitlab, and 
<b>
allows data producers to 
keep control of their data and manage contributions. 
</b>
:::

::: 

::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/datami-contribution-01.png)
:::
:::

::::::::::::::

---

## Structure your data <br>to make it interoperable

:::::::::::::: {.columns}

::: {.column width="40%"}
Your data may be associated with files to structure it, such as **data schema files**

<br>

::: {.text-micro}
By associating your data set with a data schema respecting 
<b>
international standards
</b>
(such as Frictionless standards)
you ensure that they can be correctly reused and improved
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-schemas-01.png)
:::

::::::::::::::

---

## Integrate Datami into your website<br>and any of your partners' sites 

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami uses **_web component_** technology: turnkey and customizable modules that you can add to an existing site

<br>

::: {.text-micro}
Datami _widgets_ are 
<b>
open source, 
</b>
simple to copy and paste,
without subscription, 
<b>
without additional cost.
</b>

The ability to embed a Datami web component
\- exposing one specific dataset -
onto several websites 
participates in promoting 
decentralized collaboration  
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/widget-copy-01.png)
:::

::::::::::::::


---

## You control your data,<br>Datami makes it understandable

Datami's is designed as an 
**interface between the citizen and your database**
<br>
in order to facilitate the link 
**between citizens and open data producers**


::: {.text-micro}

:::::: {.columns}

::: {.column width=60%}

::: {.text-center .img-medium .img-no-margin}
![](images/videos/datami-audiences-001.png)
:::

:::

::: {.column width=40%}
<b>
Datami doesn't store your data
</b>

Your data is stored within the tool of your choice (Github, Gitlab, database or API),
and you can choose to migrate to other tools whenever you want.

<br>

<b>
Using Datami is more affordable
</b>

You don't have to install nor maintain any dedicated _backend_,
therefore it's a thing less to care about.
:::

::::::

:::

---

## Discover the interfaces

<!-- <video
  id="datami-video-presentation"
  width="100%"
  height="85%"
  style="box-shadow: 0 0 20px #D7D7D7;"
  poster="https://raw.githubusercontent.com/multi-coop/datami-website-content/images/logos/logo-DATAMI-rect-colors-03.png"
  controls>
  <source
    src="https://raw.githubusercontent.com/multi-coop/datami-website-content/main/videos/DATAMI_TUTORIEL-FR.mp4#t=0,135"
    type="video/mp4">
</video> -->

::: {.img-medium .img-pdf .text-center}
![](images/videos/datami-video-02.png)
:::

::: {.text-center}
[Link to the video](https://datami.multi.coop/?locale=en)
:::













# _Challenge #2 _<br> Make open data more **intelligible**

## Enhance your open data with Datami

**An free and open source solution for grassroot data producers**<br>
**from the smallest organisations to local institutions**



<!-- :::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-micro}
Producing and publishing datasets are only the first steps in a open data policy.
:::
:::

::: {.column width="50%"}
::: {.text-micro}
To close the gap between local data producers and their publics a raw dataset 
should be intelligible for a broad range of users. 
:::
:::
:::::::::::::: -->


:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-border .img-pdf}
![](images/screenshots/messy-excel-02.png)
:::
:::

::: {.column width="50%"}
::: {.img-border .img-pdf}
![](images/screenshots/ODF-map-all.png)
<!-- ![](images/datami-views-panel-02.png) -->
:::
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
Your raw data 
<br>
<span class="text-secondary">without</span> 
Datami
:::
:::

::: {.column width="50%"}
::: {.text-center}
Your data more intelligible 
<br>
**with** 
Datami
:::
:::
::::::::::::::

---

## A quick tour of Datami

<!-- <video
  id="datami-video-presentation"
  width="100%"
  height="90%"
  style="box-shadow: 0 0 20px #D7D7D7;"
  poster="https://raw.githubusercontent.com/multi-coop/datami-website-content/images/logos/logo-DATAMI-rect-colors-03.png"
  controls>
  <source
    src="https://raw.githubusercontent.com/multi-coop/datami-website-content/main/videos/DATAMI_PRESENTATION-EN.mp4"
    type="video/mp4">
</video> -->

::: {.img-medium .img-pdf .text-center}
![](images/videos/datami-video-01.png)
:::

::: {.text-center}
[Link to the video](https://datami.multi.coop/?locale=en)
:::

---

## Your spreadsheets transformed into maps

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami allows you to create **customized interactive maps**

<br>

::: {.text-micro}
Your territorial data can be visualized in geographical form, whatever their themes
:::

::: 

::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/ping-map-detail-01.png)
:::
:::

::::::::::::::

---

## View your data from all angles

:::::::::::::: {.columns}

::: {.column width="40%"}
Your data can be viewed as 
**spreadsheets,** **maps,** 
miniature or detailed 
<br>
**lists of records,**
or **graphs**

<br>

::: {.text-micro}
All views are interactive and customizable to highlight all the specifics of your data
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-views-01.png)
:::

::::::::::::::


---

## Datami for data management <br> and exploration

:::::::::::::: {.columns}

::: {.column width="40%"}
**Exploring your data** allows you to better understand and manage your actions in the field

<br>

::: {.text-micro}
Datami allows you to set up personalized interactive data-visualizations, in order to make your data easily explorable
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-pilotage-01.png)
:::

::::::::::::::

--- 

## Adapt Datami to your needs

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-pdf}
![](images/screenshots/datami-sketches-01.png)
:::
:::

::: {.column width="50%"}
<!-- ::: {.img-pdf}
![&nbsp;](images/screenshots/datami-sketches-02.png)
::: -->

<!-- ![&nbsp;](images/datami-views-panel-03.png) -->
![](images/datami-views-panel-02.png)

:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
Your **sketches** <br>and your **datasets**...
:::
:::

::: {.column width="50%"}
::: {.text-center}
... implemented and enhanced <br>with **Datami**
:::
:::
::::::::::::::




# _Challenge #3_ <br>Build a solution adapted to organizations **with a tight budget**


## A 100% _open source_ toolkit

**How does Datami aims to be a full FLOSS project ?**

---

## An affordable and ethical solution<br> for structures with limited resources 

Datami provides a way 
for data producers from small-sized or local organisations
to **keep control** over their data, 
**engage** with their communities,
<br>
**without subscription nor infrastucture or maintaining costs.**

:::::: {.columns}

::: {.column width=30%}

::: {.text-micro .text-justify}
For a small structure
producing data of general interest 
it can be complicated to allocate resources 
to highlight datasets on your website 
or to call on your community to update them.

The cost of sharing, viewing, or contributing to data 
is often explained by the 
<b>
technical complexity
</b>
of such features,
the necessity to set up 
<b>
dedicated servers
</b>
as _backend_,
or costs of their 
<b>
maintenance
</b>.

<!-- Valorizing your open data and let your communities
contribute should not be limited by the amount of 
technical or financiary resources you are disposing of. -->

:::

:::


::: {.column width=70%}

::: {.img-medium .img-no-margin .text-center}
![](images/diagrams/diagram-benchmark-01.png)
:::
:::

::::::

::: {.text-micro .text-no-mt}
Datami's original architecture makes possible to 
<b>
get rid of a large part of the _backend_ server costs
</b>
<br>
while allowing customization dataset by dataset, 
as open contributions and fine control over the data.
:::

---

## A free and multi-purpose software

To make open data more accessible and more transparent 
we think 
<br>
data should be seen in three dimensions : **sharing,** **valorizing,** **opening**.

![&nbsp;](images/screenshots/datami-feats-matrix-en.png)

---

## A 100% Free/Libre and Open Source Software approach

Datami is a fully auditable and reusable **free licensed software** 
<br>
under AGPL-3.0 licence.

All the libraries used in the stack are also under free licences 
<br>
(the stack itself is presented later in this document).


:::::: {.columns .text-micro}

::: {.column width=40%}
To discover and learn how to use Datami
:::

::: {.column width=60%}
- Go to the [official Datami website](https://datami.multi.coop/?locale=en);
- Directly access the [Datami source code](https://gitlab.com/multi-coop/datami-project/datami);
- Consult the [technical documentation](https://datami-docs.multi.coop) and online tutorials;
- Call on the [Multi cooperative](https://multi.coop) for advice in data science and for training.

:::

::::::

---

### The official website

:::::::::::::: {.columns}

::: {.column width="40%"}
On our **official website** you will find **videos** of presentation, 
**examples,** 
as well as our **blog** space.

<br>

::: {.text-micro}
The website is currently translated into French and English.
:::

::: 

::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/datami-website-en.png)
:::
::: {.text-center}
🚀 [datami.multi.coop](https://datami.multi.coop/?locale=en)
:::
:::

::::::::::::::

---

### The source code

:::::::::::::: {.columns}

::: {.column width="40%"}
Our **source code is on Gitlab** under open source license

<br>

::: {.text-micro}
Do not hesitate to report bugs to us by suggesting new [_issues_](https://gitlab.com/multi-coop/datami-project/datami/-/issues), 
or to propose your ideas for any new features on our [ _roadmap_](https://gitlab.com/multi-coop/datami-project/datami/-/boards/4736577).

A [mirror repo](https://github.com/multi-coop/datami) 
is also automatically synced on Github.

Don't forget to 
<b>
leave a little ⭐️ 
</b>
on the repo
if you like the project!
:::
:::


::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/datami-gitlab.png)
:::
::: {.text-center}
💻<br>
[Repo Gitlab](https://gitlab.com/multi-coop/datami-project/datami)<br>
[Repo Github (mirror)](https://github.com/multi-coop/datami)

![](https://img.shields.io/badge/dynamic/json?color=turquoise&label=Gitlab%20stars%20%E2%98%85&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39268471)
![GitHub Repo stars](https://img.shields.io/github/stars/multi-coop/datami?label=Github%20stars&color=turquoise)

:::
:::

::::::::::::::

---

### The technical documentation site

:::::::::::::: {.columns}

::: {.column width="40%"}
Also visit our dedicated **documentation site**

<br>

::: {.text-micro}
The website is currently translated into French and English.

You will find different sections there: technical principles, tutorials, examples, description of the different widgets and their configuration elements...
:::

::: 

::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/datami-doc-website.png)
:::
::: {.text-center}
🔧 [datami-docs.multi.coop](https://datami-docs.multi.coop/?locale=en)
:::
:::

::::::::::::::



