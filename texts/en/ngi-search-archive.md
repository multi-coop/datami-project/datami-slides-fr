---
title: <strong>Project Datami</strong>![](images/logos/logo-DATAMI-rect-colors.png)
subtitle: '
  Visualise, edit, share, and contribute<br>
  <strong>Produce decentralized open data</strong><br>
  <strong>as inclusive digital commons</strong><br><br>
  <b>NGI SEARCH open call \#2</b>'
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _fullstack developer_
  </span><br>
  <span class="text-micro text-author-details">
    _co-founder of tech cooperative [multi.coop](https://multi.coop)_
  </span><br>
  <span class="img-cover">
    ![](images/logos/logo-MULTI-colored-063442-02-w120.png)
  </span>
'
date: <span class="emph-line">March 27th 2023</span>
title-slide-attributes:
  data-background-image: "images/sponsors/ngi_search-logo.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 5%"
  data-background-position: "right 10% top 37%, right 4% top 40%"

---

# Summary

::: {.text-micro .summary}

- [Brief project description](#brief-project-description) <span class="summary-page">3.1</span></span>
- [Scopes](#scopes-ethics-in-seach-and-discovery-new-ways-of-discovering-and-accessing-information)<br>&nbsp;· Ethics in seach and discovery<br>&nbsp;· New ways of discovering and accessing information <span class="summary-page">4.1</span>
  - [Challenge #1](#challenge-1-make-open-data-more-intelligible) · Make open data more intelligible <span class="summary-page">5.1</span>
  - [Challenge #2](#challenge-2-make-updating-and-contributing-easier) · Make updating and contributing easier <span class="summary-page">6.1</span>
  - [Challenge #3](#challenge-3-build-a-solutions-adapted-to-organizations-with-a-tight-budget) · Build a solutions adapted to organizations with a tight budget <span class="summary-page">7.1</span>
- [Benchmark](#benchmark) <span class="summary-page">8.1</span>
- [Datami’s goals for the next 12 months](#datamis-goals-for-the-next-12-months) <span class="summary-page">9.1</span>
  - [Goal #1](#goal-1-research-development-of-missing-core-features-1) · Research & development of missing core features <span class="summary-page">10.1</span>
  - [Goal #2](#goal-2-keep-a-global-coherence-of-a-freelibre-and-open-source-software-project-1) · Keep a global coherence of a FLOSS project <span class="summary-page">11.1</span>
  - [Goal #3](#goal-3-exploring-use-cases-for-new-open-data-communities-1) · Exploring use cases for new open data communities <span class="summary-page">12.1</span>
- [The team](#the-team) <span class="summary-page">13.1
- [Provisional budget](#provisional-budget) <span class="summary-page">14.1</span>
- [Provisional timelines](#provisional-timelines) <span class="summary-page">15.1</span>
- [Credits](#section) <span class="summary-page">16.1</span>

:::


# Brief project description


:::::: {.columns}

::: {.column width=65%}
::: {.img-test .text-center .text-micro}
![](images/datami-views-panel-03.png)
<!-- ![&nbsp;](images/logos/logo-DATAMI-rect-colors.png) -->

::: {.img-nano .mini-inline}
![](images/logos/logo-DATAMI-rect-colors-h75.png)&nbsp;&nbsp;[Datami website](https://datami.multi.coop/?locale=en)
:::

:::
:::

::: {.column .text-micro .text-justify width=35%}
<br>

Small organisations are at the same time the structures 
possesing the most accurate knowledge and informations
about specific topics or a territory,
but are also the kind of organisations 
with the 
<b>
most limited resources to open their data.
</b>

**Datami** open source project aims to 
<b>
help small and local organisations producing & sharing structured informations
</b>
by providing free, generic, and customizable web components for visualisation and contribution. 
Those components include a set of features
allowing each 
<b>
organisation's community 
to contribute to such decentralized open data.
</b>

Datami paves the way to<br>
**transition from open data**
<br>
**to inclusive digital commons**
<br>
for a broad range of audiences.

:::

::::::


---

### · Needs · <br>Open data policies for grassroot organisations


::: {.text-center}
**Make grassroot open data become strong digital commons**
:::


::: {.text-micro}

:::::: {.columns}

::: {.column .text-justify width=50%}
Small or medium-sized grassroots organisations often are the very same ones 
acting as first responders to local problems on the territory or on very specific topics. 


Data allows them to act more efficiently at the local level, 
but also to 
<b>
raise public awereness towards a subject :
</b>
environment, health, social aid, urbanism, transportation...

NGOs, non profit associations, small local public institutions... 
all have their own way to gather data and knowledge about 
the topics they are involved in.
More crucially than for national institutions 
grassroot structures often need 
<b>
the help of their own communities
to maintain and enrich their data,
</b>
but most of the time
small orgranisations produce open data
<b>
with very limited means.
</b>
:::

::: {.column .text-justify width=50%}

Publishing grassroot data as it's produced - i.e. in a decentralized manner -
is a matter of public information 
but also a matter of empowering citizen while building it.

Hence, to reach public awareness
those organisations must beforehand overcome 
complex technical, design, and uses challenges at their level.

In a word, sharing the information such organisations produce 
is a matter of public interest, 
but also an activity more and more
complicated to follow through due to
<b>
an economic context
resulting by a growing lack of resources
for grassroot organisations.
</b>

:::

::::::

:::


---

### · Problem ·<br>Limited resources of grassroot organisations

::: {.text-center}
**How to overcome the structural limit of means to open data ?**
:::

::: {.text-micro}
Following through an open data policy could get hard for small organisation because
<b>
publishing a dataset is rarely enough
</b>
... it's merely just a starting point.
Open data policies must aspire to improve the public's ability 
to 
<b>share</b>
the data, to 
<b>understand</b>
it, to 
<b>edit and contribute</b>
to it,
while assuring its 
<b>integrity</b>
and secure the 
<b>publishers' responsability.</b>
:::

:::::: {.columns .text-micro}

::: {.column .text-justify width=50%}
Open data policies led by small organisations
can quickly be put on hold due to the complexity of making 
open data attractive and accessible enough for broad audiences.

An open dataset must be adapted to very common uses, and anticipate the risk of
excluding "non-tech" publics so they could equally benefit from open & transparent information, 
but also so such publics could help improving information from grassroot.

To overcome this complexity open datasets must fulfil a set of technical needs, 
be shared via intermediary tools, platforms or developments often involving either 
overcosts, less control over the data, discouragement, or loss of time learning how to use such tools...
:::

::: {.column .text-justify width=50%}
We observed the most recurrent reasons causing open data policies to be put "on hold" were often to be found among the following :

- **Thight budget**
- **Lack of competences or technical difficulties**
- **Lack of time to curate and moderate contributions**
- **Lack of interest or impression of complexity**
- **Lack of open source tools for "non-tech" data producers' needs**
- **Fear of losing control or sovereignty over data**
:::

::::::


---

### · Solution · <br>Make open data and grassroot contributions<br>inclusive and affordable

::: {.img-no-margin .img-small .text-center}
![](images/datami-views-panel-04.png)
:::


:::::: {.columns .text-micro}

::: {.column width=25%}
<b>
  **More intelligible**
  <br>
  **an informative datasets**
</b>

::: {.text-justify}
Datami provides a variety of data-visualisations and views commonly used on internet
such as lists of cards, maps, graphs, diagrams... 
and also a variety of commons tools like interactive filters
you can customize for any specific dataset
:::
:::

::: {.column width=25%}
<b>
  **More easily open**
  <br>
  **to external contributions**
</b>

::: {.text-justify}
Datami relies on external platforms such as Gitlab or Gihub, 
thus also being able to use their APIs 
to send contributions onto datasets 
via merge requests on new branches.
:::
:::

::: {.column width=25%}
<b>
  **More structured**
  <br>
  **and interoperable datasets**
</b>

::: {.text-justify}
Datami includes the possibilty to associate data models to datasets.
For instance table schema standard (by Frictionless) is used as Datami's standard for csv tables.
:::
:::

::: {.column width=25%}
<b>
  **More affordable**
  <br>
  **and transparent**
</b>

::: {.text-justify}
Datami delagates to external platforms (like Gitlab)
the data storage and the versionning control features.
Doing so Datami stands for a larger complementarity between FLOSS solutions,
and for a low-code approach to open data.
:::
:::

::::::

---

### · Ambitions ·

::: {.text-micro}
Datami's project aims to adress the latter problem by considering 
**the future of open data must be even more distributed,**
as much in its production as in its maintenance or considering broader target audiences. 

We have the ambition to provide a FLOSS (Free/Libre and Open Source Software) toolbox for grassroot organisations,
allowing them to accelerate their open data policies 
based on a combination of open source technologies.
:::

:::::: {.columns .text-micro}

::: {.column .text-justify width=50%}
### VALORIZE

Enable organisations of all sizes 
<b>to produce open data & valorise their datasets</b> with their communities.

:::

::: {.column .text-justify width=50%}
### CONTRIBUTE

Empoyer organisations' audiences by  helping them to 
<b>contribute to open data freely</b>, easily, while respecting their privacy.

:::

::: {.column .text-justify width=50%}
### DISTRIBUTE

Help any organisation to create 
<b>interoperable & structured</b> datasets,
with the strict minimum of technical background. 

:::

::: {.column .text-justify width=50%}
### PROMOTE FLOSS

Promote <b>complementarity within FLOSS (Free/Libre and Open Source Software) robust initiatives</b>, 
notably by the use of Git / Gitlab as a backend for data storage and version control.
:::

::::::

::: {.text-micro .text-center}
As you will discover later in this document we segmented those ambitions into three challenges : 
<br>
**intelligibility** &nbsp;
**contributibility** &nbsp;
**affordability**
:::

---

### · Innovation / differentiation · 1/2

::: {.text-micro}

:::::: {.columns}

::: {.column .text-justify width=50%}
There is nowadays a large variety of solutions - either open source or proprietary - for data visualisation or edition.
Those solutions can either be provided as Saas or stand alone. 

Most common ones are Metabase or Grist in the open source category, or Airtable and GSheet in the proprietary section (*).

Those existing solutions still have two common factors :

- They suppose to <b>use, or install and maintain a backend server ;</b>

- They often <b>lack or provide limited features for contribution and moderation.</b>

:::

::: {.column .text-justify width=50%}
**Datami's approach intends to do "more with less"** : 
we intended to provide more features for contribution and moderation on a dataset, 
whereas liberating us from developping ourselves a complex backend for data storage and/or version control.

The solution we found was quite "on the nose" : 
the best tools we can think of today for making or managing contributions - and then to moderate them - 
are the git language and platforms relying on git like Gitlab.

That said and to our knowledge,
<b>
we didn't find existing open source solutions 
adressing at the same time the needs 
we encountered
</b>
talking to small organisations
hoping to produce open data for their audiences :
a tool
<b>
simple to apprehend,
affordable, 
helping audiences to propose contributions,
with variety and quality in terms visualisations and design.
</b>

:::

::::::

(*) _An extensive benchmark is provided later in this document._

:::

---

### · Innovation / differentiation · 2/2

::: {.text-micro}

<b>
To adress the needs of open data's grassroot producers
we designed a software architecture 
<br>
based on a serie of simple principles
we follow altogether
</b>

:::::: {.columns .text-justify}

::: {.column width=25%}
**Use existing platforms**<br>**as backends**

We use platforms such as 
Gitlab / Github / Bitbucket / SourceForge / external APIs... 
<b>
as backends for data storage and version control.
</b>

Such platforms are proved to be robust,
we only account for a few,
some are FLOSS (Free/Libre and Open Source Software),
and they already provide all the tools
needed for version control and moderation.

Thus we can store either datasets
or any other file (such as configuration files, schemas, text files...)
on those platforms independantly from Datami's application.
:::

::: {.column width=25%}
**Interfaces you can embed**

The spaces where small orgaizations 
need to valorize their data is
where their target audience goes,
usually the organization's own website. 

It is very common small for organizations to use
Wordpress or on-the-shelf solutions for their website,
but some are custom-made.

The use of 
<b>
web components 
</b>
technology and web standards (or "widgets") is the choice 
we made to 
<b>
ensure the ability to embed data onto any website.
</b>
:::

::: {.column width=25%}
**Keep the data structure**<br>**at all steps**

Data interoperability is a principle we implemented
in Datami with the use of standards, 
such as Frictionless for tables and json files.

Any table displayed with Datami can be 
<b>
associated with a local or distant schema, 
</b>
in addition to configuration files. 

Doing so, Datami can display datasets
accordingly to its schema,
but also provide pre-rendered forms
to help for open contributions.
:::

::: {.column width=25%}
**You can display the data**<br>**as you need to**

Genericity and customisation are core intentions of Datami.
We search for a 
<b>
balance between the need to customize 
</b>
the UI/UX of the widgets,
<b>
and the need for a versatile tool
</b>
adaptable to a large range of data structures or properties.

We distinguish several types of files : 
end-user data (csv, text, json, API requests...),
schemas files,
and configuration files. 

What makes a dataset unique
can be set up either in its schema or configuration files,
or even in the web component's html itself. 
:::

::::::

:::






# **_Scopes_** <br>· Ethics in seach and discovery<br>· New ways of discovering and accessing information


---

### The problems with implementing <br>open data for grassroot organisations 

::: {.text-micro}

:::::: {.columns}

::: {.column width=25%}
Local or small organizations are esential actors of a country's public life. 
They can either provide direct help or support to citizen, 
or intelligence and innovation about the topics they are specialized in.

By making public the information such organisation gather over time
can be a lever to raise public awareness from citizen or authorities. 
:::

::: {.column width=25%}
Yet opening data cannot be resumed to the act of publishing raw datasets on a website or on a platform.
Open data policies must target an audience, data must be kept updated regularly, 
displayed on solutions adapted to the targeted audiences and to the resources of the publisher...

In the case of small or local organisations the challenges 
laying beforehand and after in their open data policies
are severe and can often refrain them to act.
:::

::::::

:::

::: {.text-micro}

:::::: {.columns}

::: {.column width=25%}
**Lack of standardization**

Local or small organizations may struggle with
standardizing their data to make it more easily 
accessible and usable by others. 

This can limit the potential benefits of open data and 
<b>
make it difficult for others to use the data effectively.
</b>
:::

::: {.column width=25%}
**Data quality**<br>**and privacy concerns**

Local or small organizations may be hesitant
to release data due to concerns about data quality or privacy. 

They can lack the resources or expertise to 
<b>
properly manage and safeguard digital information.
</b>
:::

::: {.column width=25%}
**Lack of maintainability**<br>**or support from audiences**

Many local or small organizations may not fully 
anticipate that publishing data involves mobilizing their audiences
to help 
<b>
keeping the information up to date. 
</b>
:::

::: {.column width=25%}
**Limited resources**

Local or small organizations may not have 
the resources to establish an open data policy. 

This can include the 
<b>
lack of staff with expertise 
in open data, limited funding, and inadequate technological infrastructure
</b>
:::

::::::

:::

---

### How to help grassroot open data<br>becoming strong digital commons ?


::: {.text-micro}
Providing easy-to-use tools, designed for broad audiences, 
is one of the keys to foster open data and transparency 
from grassroot organisations : 
"code is law" and
the tooling, its interfaces and its design,
influences the kind of uses
one can have with a data.

::: {.text-center}
<b>
Open data must be though as a digital common to reach its full potential.
</b>
:::

Open data uses may be limited by the tools usually 
the data producers can afford or be able to put their hand on.  
The <b>challenges</b> for small organisations to lead strong open data policies are nothing but down to earth.
:::


:::::: {.columns}

::: {.column width=25%}
#### Challenge \#1 <br>**INTELLIGIBILITY**

::: {.text-micro}
Make open data more
<b>intelligible</b>
for any citizen.
:::
:::

::: {.column width=25%}
#### Challenge \#2 <br>**CONTRIBUTIBILIY**

::: {.text-micro}
Make 
<b>updating</b>
and 
<b>contributing</b>
easier for producers and contributors.

This topic includes the effort of standardization and interoperability.
:::
:::

::: {.column width=25%}
#### Challenge \#3 <br>**AFFORDABILITY**

::: {.text-micro}
Building solutions allowing any organisation to benefit from such features 
<b>with a tight budget</b>
:::
:::

::::::

---

## _Challenge #1 _

### Make open data more **intelligible** for citizen

--- 

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-border .img-pdf}
![](images/screenshots/messy-excel-02.png)
:::
:::

::: {.column width="50%"}
::: {.img-border .img-pdf}
![](images/screenshots/ODF-map-all.png)
:::
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
Your raw data <br>**without** Datami
:::
:::

::: {.column width="50%"}
::: {.text-center}
Your data more intelligible <br>**with** Datami
:::
:::
::::::::::::::

---

## _Challenge #2 _

### Make **updating** and **contributing** easier <br>for producers and contributors

---

:::::::::::::: {.columns}

::: {.column width="40%"}
Handling data is a matter of habits, facilitating their manipulation for the greatest number means that you have to **adapt to the habits** of the greatest number

<br>

::: {.text-micro}
To allow everyone to easily contribute to the data, the view in the form of a table is still the most commonly adopted today.
:::

::: 

::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/table-view-01.png)
:::
:::

::::::::::::::

---

## _Challenge #3_

### Building solutions allowing any organisation<br> to benefit from such features **with a tight budget**

---

<!-- ::: {.text-center}
**Lack of resources** ·&nbsp;
**lack of skills** ·&nbsp;
**lack of time**
::: -->


:::::::::::::: {.columns}
::: {.column width="30%"}
::: {.text-micro}
::: {.text-justify}
The cost of the usual technical solutions for sharing / viewing / contributing to data is often explained by the technical **complexity** of these functionalities
:::
:::
:::
::: {.column width="30%"}
::: {.text-micro}
::: {.text-justify style="padding: 0 1.5em 0 1.5em"}
Another cost is related to the need to set up dedicated **servers** in _backend_ or very specific configurations
:::
:::
:::
::: {.column width="30%"}
::: {.text-micro}
::: {.text-justify}
The **maintenance** of applications or servers often generates significant costs
:::
:::
:::
::::::::::::::


::: {.img-medium .img-no-margin .text-center}
![](images/diagrams/diagram-benchmark-01.png)
:::

::: {.text-micro}
:::::::::::::: {.columns}
::: {.column width=50%}
When you are a small or medium-sized structure (association, community, etc.) 
and you produce data of general interest, 
it can be complicated to highlight them on your site 
or to call on your community to put them up to date.
:::
::: {.column width=50%}
Datami's original architecture makes it possible to 
<b>
get rid of a large part of these _backend_ server costs
</b>
while allowing customization dataset by dataset
:::
::::::
:::




# _Challenge #1 _<br> Make open data more **intelligible**

## Enhance your open data with Datami

**An free and open source solution for all data producers**<br>
**from the smallest organisations to local institutions**

---

## A quick tour of Datami

<!-- <video
  id="datami-video-presentation"
  width="100%"
  height="90%"
  style="box-shadow: 0 0 20px #D7D7D7;"
  poster="https://raw.githubusercontent.com/multi-coop/datami-website-content/images/logos/logo-DATAMI-rect-colors-03.png"
  controls>
  <source
    src="https://raw.githubusercontent.com/multi-coop/datami-website-content/main/videos/DATAMI_PRESENTATION-EN.mp4"
    type="video/mp4">
</video> -->

::: {.img-medium .img-pdf .text-center}
![](images/videos/datami-video-01.png)
:::

::: {.text-center}
[Link to the video](https://datami.multi.coop/?locale=en)
:::

---

## Your spreadsheets transformed into maps

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami allows you to create **customized interactive maps**

<br>

::: {.text-micro}
Your territorial data can be visualized in geographical form, whatever their themes
:::

::: 

::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/ping-map-detail-01.png)
:::
:::

::::::::::::::

---

## View your data from all angles

:::::::::::::: {.columns}

::: {.column width="40%"}
Your data can be viewed as 
**spreadsheets,** **maps,** 
miniature or detailed 
<br>
**lists of records,**
or **graphs**

<br>

::: {.text-micro}
All views are interactive and customizable to highlight all the specifics of your data
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-views-01.png)
:::

::::::::::::::


---

## Datami for data management <br> and exploration

:::::::::::::: {.columns}

::: {.column width="40%"}
**Exploring your data** allows you to better understand and manage your actions in the field

<br>

::: {.text-micro}
Datami allows you to set up personalized interactive data-visualizations, in order to make your data easily explorable
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-pilotage-01.png)
:::

::::::::::::::

--- 

## Adapt Datami to your needs

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/datami-sketches-01.png)
:::
:::

::: {.column width="50%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/datami-sketches-02.png)
:::
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
Your **sketches** <br>and your **datasets**...
:::
:::

::: {.column width="50%"}
::: {.text-center}
... implemented and enhanced <br>with **Datami**
:::
:::
::::::::::::::



# _Challenge #2_ <br> Make **updating** and **contributing** easier


## A simplified process <br>for contributory data updates

**Helping all citizens to actively contribute to open data**

---

## Empower your teams and audiences <br>to improve data

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami includes a system for 
<br>
**contribution and moderation**

<br>

::: {.text-micro}
Based on the _Git_ language, Datami widgets 
allow your audience to suggest improvements and contribute to the data
<b>
without creating an account.
</b>

Datami is based on external robust
services for data storage and version control
like Gitlab and 
<b>
allows data producers to 
keep control of their data and manage contributions. 
</b>
:::

::: 

::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/datami-contribution-01.png)
:::
:::

::::::::::::::

---

## Structure your data <br>to make it interoperable

:::::::::::::: {.columns}

::: {.column width="40%"}
Your data may be associated with files to structure it, such as **data schema files**

<br>

::: {.text-micro}
By associating your data set with a data schema respecting 
<b>
international standards
</b>
(such as Frictionless standards)
you ensure that they can be correctly reused and improved
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-schemas-01.png)
:::

::::::::::::::

---

## Integrate Datami into your website<br>and any of your partners' sites 

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami uses **_web component_** technology: turnkey and customizable modules that you can add to an existing site

<br>

::: {.text-micro}
Datami _widgets_ are 
<b>
open source, 
</b>
simple to copy and paste,
without subscription, 
<b>
without additional cost.
</b>

The ability to embed a Datami web component
\- exposing one specific dataset -
onto several websites 
participates in promoting 
decentralized collaboration  
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/widget-copy-01.png)
:::

::::::::::::::

---

## Discover the interfaces

<!-- <video
  id="datami-video-presentation"
  width="100%"
  height="85%"
  style="box-shadow: 0 0 20px #D7D7D7;"
  poster="https://raw.githubusercontent.com/multi-coop/datami-website-content/images/logos/logo-DATAMI-rect-colors-03.png"
  controls>
  <source
    src="https://raw.githubusercontent.com/multi-coop/datami-website-content/main/videos/DATAMI_TUTORIEL-FR.mp4#t=0,135"
    type="video/mp4">
</video> -->

::: {.img-medium .img-pdf .text-center}
![](images/videos/datami-video-02.png)
:::

::: {.text-center}
[Link to the video](https://datami.multi.coop/?locale=en)
:::

---

## You control your data,<br>Datami makes it understandable

Datami's is designed as an 
**interface between the citizen and your database**
<br>
in order to facilitate the link 
**between citizens and open data producers**

<br>

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
<b>
Datami doesn't store your data
</b>

Your data is stored within the tool of your choice (Github, Gitlab, database or API),
and you can choose to migrate to other tools whenever you want.
:::

::: {.column width=50%}
<b>
Using Datami is more affordable
</b>

You don't have to install nor maintain any dedicated _backend_,
therefore it's a thing less to care about.
:::

::::::

:::



# _Challenge #3_ <br>Build a solutions adapted to organizations **with a tight budget**


## A 100% _open source_ toolkit

**How does Datami aims to be a full FLOSS project ?**

---

## A free and multi-purpose software

To make open data more accessible and more transparent we think it needs to help in 3 dimensions : **sharing,** **valorizing,** **opening**.

![&nbsp;](images/screenshots/datami-feats-matrix-en.png)

---

## A 100% Free/Libre and Open Source Software approach

Datami is a fully auditable and reusable **free licensed software** under AGPL-3.0 licence.

All the libraries used in the stack are also under free licences (the stack itself is presented later in this document).


:::::: {.columns .text-micro}

::: {.column width=40%}
To discover and learn how to use Datami
:::

::: {.column width=60%}
- Go to the [official Datami website](https://datami.multi.coop/?locale=en);
- Directly access the [Datami source code](https://gitlab.com/multi-coop/datami-project/datami);
- Consult the [technical documentation](https://datami-docs.multi.coop) and online tutorials;
- Call on the [Multi cooperative](https://multi.coop) for advice in data science and for training.

:::

::::::

---

## An affordable and ethical solution<br> for structures with limited resources 

Datami provides a way 
for data producers from small-sized or local organisations
to **keep control** over their data, 
**engage** with their communities,
<br>
**without subscription nor infrastucture or maintaining costs.**

:::::: {.columns}

::: {.column width=25%}

::: {.text-micro}
Valorizing your open data and let your communities
contribute should not be limited by the amount of 
technical or financiary resources you are disposing of.
:::

:::

::: {.column width=75%}
::: {.img-medium .img-no-margin .text-center}
![](images/diagrams/diagram-benchmark-01.png)
:::
:::

::::::


---

### The official website

:::::::::::::: {.columns}

::: {.column width="40%"}
On our **official website** you will find **videos** of presentation, **examples,** as well as our **blog** space

<br>

::: {.text-micro}
The website is currently translated into French and English.
:::

::: 

::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/datami-website-en.png)
:::
::: {.text-center}
🚀 [datami.multi.coop](https://datami.multi.coop/?locale=en)
:::
:::

::::::::::::::

---

### The source code

:::::::::::::: {.columns}

::: {.column width="40%"}
Our **source code is on Gitlab** under _open source_ license

<br>

::: {.text-micro}
Do not hesitate to report _bugs_ to us by suggesting new [_issues_](https://gitlab.com/multi-coop/datami-project/datami/-/issues), 
or to propose your ideas for any new features on our [ _roadmap_](https://gitlab.com/multi-coop/datami-project/datami/-/boards/4736577).

A [mirror repo](https://github.com/multi-coop/datami) is also automatically synced on Github.

Don't forget to 
<b>
leave a little ⭐️ 
</b>
on the repo
if you like the project!
:::
:::


::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/datami-gitlab.png)
:::
::: {.text-center}
💻<br>
[Repo Gitlab](https://gitlab.com/multi-coop/datami-project/datami)<br>
[Repo Github (miror)](https://github.com/multi-coop/datami)

![](https://img.shields.io/badge/dynamic/json?color=turquoise&label=gitlab%20stars%20%E2%98%85&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39268471)

:::
:::

::::::::::::::

---

### The technical documentation site

:::::::::::::: {.columns}

::: {.column width="40%"}
Also visit our dedicated **documentation site**

<br>

::: {.text-micro}
The website is currently translated into French and English.

You will find different sections there: technical principles, tutorials, examples, description of the different widgets and their configuration elements...
:::

::: 

::: {.column width="60%"}
::: {.img-pdf}
![&nbsp;](images/screenshots/datami-doc-website.png)
:::
::: {.text-center}
🔧 [datami-docs.multi.coop](https://datami-docs.multi.coop/?locale=en)
:::
:::

::::::::::::::







# Benchmark

**How does Datami differs from other solutions ?**

---

::: {.text-center}
There are several data visualization and editing solutions <br>sharing similarities with [Datami](https://datami.multi.coop/?locale=en)

Here are some of the most popular solutions

<br>

::: {.text-micro}
**Those benchmarks are given for information only**

<br>

Do not hesitate to let us know by writing to
<br>
[contact@multi.coop](mailto:contact@multi.coop)
<br>
if you wish to make any corrections or additions
:::

:::

---

### Benchmark · Datavisualisation tools


| Solution               | Solution type    | Langages                            | Difficulty            | Saas    | Official website                                                 |
| ------------------     | ---------------- | ----------------------------------- | ---                   | ---     | ---                                                              |
| **Gogocarto**          | Open source      | Custom                              | Easy                  | Yes    | [https://gogocarto.fr/projects](https://gogocarto.fr/projects)   |
| **Umap**               | Open source      | Custom                              | Facile                | Yes    | [https://umap.openstreetmap.fr/](https://umap.openstreetmap.fr/) |
| **Lizmap**             | Open source      | Propre langage de requête           | Medium                | Yes    | [https://www.lizmap.com](https://www.lizmap.com)                 |
| **Apache Superset**    | Open source      | SQL                                 | Medium                | Yes    | [https://superset.apache.org/](https://superset.apache.org/)     |
| **Apache Zeppelin**    | Open source      | Several programmation languages     | Hard                  | No     | [https://zeppelin.apache.org/](https://zeppelin.apache.org/)     |
| **BIRT**               | Open source      | Java, JavaScript                    | Hard                  | No     | [https://www.eclipse.org/birt/](https://www.eclipse.org/birt/)   |
| **FineReport**         | Open source      | Java, JavaScript                    | Medium                | No     | [https://www.finereport.com/en](https://www.finereport.com/en)   |
| **Grafana**            | Open source      | Custom request language             | Hard                  | No     | [https://grafana.com/](https://grafana.com/)                     |
| **Metabase**           | Open source      | SQL                                 | Easy                  | Yes    | [https://www.metabase.com/](https://www.metabase.com/)           |
| **Redash**             | Open source      | SQL                                 | Medium                | No     | [https://redash.io/](https://redash.io/)                         |
| **Datasette**          | Open source      | SQL                                 | Medium                | No     | [https://datasette.io/](https://datasette.io/)                   |
| **LightDash**          | Open source      | Dbt                                 | Medium                | Yes    | [https://www.lightdash.com/](https://www.lightdash.com/)         |
| **Google Data Studio** | Free (but not open) | SQL                              | Easy                  | Yes    | [https://datastudio.google.com/](https://datastudio.google.com/) |
| **Datawrapper**        | Commercial       | API, CSV, GSheet                    | Easy                  | Yes    | [https://www.datawrapper.de/](https://www.datawrapper.de/)       |
| **Google Looker**      | Commercial       | LookML                              | Hard                  | Yes    | [https://looker.com/](https://looker.com/)                       |
| **Microsoft Power BI** | Commercial       | DAX et M                            | Medium                | Yes    | [https://powerbi.microsoft.com/](https://powerbi.microsoft.com/) |
| **QlikView**           | Commercial       | Custom script language              | Hard                  | Yes    | [https://www.qlik.com/](https://www.qlik.com/)                   |
| **Tableau**            | Commercial       | Custom                              | Medium                | Yes    | [https://www.tableau.com/](https://www.tableau.com/)             |

---

### Benchmark · Online editing tools

| Solution            | Solution type | Langages                 | Difficulty             | Saas | Targeted public                                                              | Official webiste                                                             |
| ------------------- | ------------  | ------------------------ | ---                    | ---  | -------------------------------------------------------------------------    | ---                                                                          |
| **Apache Zeppelin** | Open source | Scala, Python, R, SQL      | Hard                   | No   | Developers and professional users                                            | [https://zeppelin.apache.org/](https://zeppelin.apache.org/)                 |
| **Baserow**         | Open source | Python, Javascript, Vue.js | Moyen                  | Yes  | Developers and professional users                                            | [https://baserow.io/](https://baserow.io/)                                   |
| **Grist**           | Open source | Python                     | Easy                   | Yes  | Businesses, non-profit organizations, governments, universities, researchers | [https://getgrist.com/](https://getgrist.com/)                               |
| **Metabase**        | Open source | Java, Clojure              | Medium                 | Yes  | Startups, businessees, non-profit organizations                              | [https://www.metabase.com/](https://www.metabase.com/)                       |
| **LockoKit**        | Open source | ...                        | Medium                 | No   | Developers and professional user                                             | [https://locokit.io/](https://locokit.io/)                                   |
| **NoCodB**          | Open source | Javascript, Node.js        | Easy                   | Yes  | Developers and professional users                                            | [https://nocodb.com/](https://nocodb.com/)                                   |
| **Gsheet**          | -           | None                       | Easy                   | Yes  | Companies, teams, freelancers, SMEs                                          | [https://www.google.com/sheets/about/](https://www.google.com/sheets/about/) |
| **Airtable**        | Commercial  | None                       | Easy                   | Yes  | Companies, teams, freelancers, SMEs                                          | [https://airtable.com/](https://airtable.com/)                               |
| **Qlikview**        | Commercial  | None                       | Medium                 | Yes  | Large corporations, financial institutions                                   | [https://www.qlik.com/us/](https://www.qlik.com/us/)                         |

---

### Benchmark · Features comparison

::: {.text-micro}
Among all the solutions we have just listed, some can be compared with **Datami**'s main features.
:::

| Solution            | Open source  | Readyness | Table view | Cards view  | Map view   | Dataviz view | Edition  | Moderation  | Configuration interface     | Data sources        | Backend              | Widget  | Official website                               |
| -----------------   | :---:        | ---       | :---:      | :---:       | :---:      | :---:        | :---:    | :---:       | :---:                       | ------------------  | -------------------- | :---:   | ---                                            |
| **Datami**          | ✅           | ⭐⭐      | ✅         | ✅          | ✅         | ✅           | ✅       | ✅          | ❌ (for now)                | API ext. (Git)      | Git platforms / APIs | ✅      | [Website](https://datami.multi.coop/?locale=en/)          |
| **Metabase**        | ✅           | ⭐⭐      | ✅         | ❓          | ✅         | ✅           | ⚠️        | ❌          | ✅                          | SQL, connectors     | server / APIs        | ✅      | [Website](https://www.metabase.com/)           |
| **Gogocarto**       | ✅           | ⭐⭐⭐    | ✅         | ✅          | ✅         | ❌           | ✅       | ❌          | ✅                          | proper              | server / APIs        | ✅      | [Website](https://gogocarto.fr/projects)       |
| **Lizmap**          | ✅           | ⭐        | ⚠️          | ❌          | ✅         | ✅           | ✅       | ❌          | ✅                          | proper              | server / ❓          | ✅      | [Website](https://www.lizmap.com)              |
| **Umap**            | ✅           | ⭐⭐⭐    | ❌         | ❌          | ✅         | ❌           | ✅       | ❌          | ✅                          | ...                 | server / ❓          | ✅      | [Website](https://umap.openstreetmap.fr/)      |
| **Grist**           | ✅           | ⭐        | ✅         | ✅          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / APIs        | ❌      | [Website](https://getgrist.com/)               |
| **Baserow**         | ✅           | ⭐⭐      | ✅         | ✅          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / ❓          | ❌      | [Website](https://baserow.io/)                 |
| **LockoKit**        | ✅           | ⭐⭐      | ✅         | ❌          | ❌         | ❌           | ✅       | ❓          | ✅                          | proper              | server / ❓          | ❌      | [Website](https://locokit.io/)                 |
| **NoCodB**          | ✅           | ⭐        | ✅         | ❌          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / ❓          | ❌      | [Website](https://nocodb.com/)                  |
| **Apache Superset** | ✅           | ⭐⭐      | ✅         | ❌          | ❌         | ✅           | ✅       | ❓          | ✅                          | SQL                 | server / Saas        | ❓      | [Website](https://superset.apache.org/)         |
| **Datawrapper**     | 🔒           | ⭐⭐⭐    | ✅         | ❓          | ✅         | ✅           | ❌       | ❌          | ✅                          | SQL, connectors     | Saas                 | ✅      | [Website](https://www.datawrapper.de/)          |
| **Airtable**        | 🔒           | ⭐⭐⭐    | ✅         | ✅          | ❌         | ⚠️            | ✅       | ⚠️           | ✅                          | proper, connectors  | Saas                 | ✅      | [Website](https://airtable.com/)                |
| **Gsheet**          | 🔒           | ⭐⭐⭐    | ✅         | ❌          | ❌         | ✅           | ✅       | ✅          | ✅                          | proper, connectors  | Saas                 | ✅      | [Website](https://www.google.com/sheets/about/) |


::: {.text-micro .text-center}
The ❓ indicates the information is to be completed<br>
The ⚠️ that the functionality can be implemented either in a roundabout way or as a hack
:::

---

Although these solutions share some features with [Datami](https://datami.multi.coop/?locale=en), they can differ significantly in terms of cost, complexity and specific features

It is important to take into account the **specific needs of each project** before choosing the most suitable online data visualization and editing solution

---

## Benchmark · Datami's main differentiating innovations

Datami project is built around the intention of making work together three specifications in terms of web development.

:::::: {.columns}

::: {.column width=33%}

**Open contributions &**<br>**moderation**

::: {.text-micro}
At the difference of the open source solutions
we identified Datami aims to provide 
a simple system allowing 
<b>
at the same time :
</b>

- a system of contribution respecting users' rights to privacy ;
- a system of moderation of contributions by data producers ;
- simple contribution and moderation interfaces managable by common citizen. 
:::
:::

::: {.column width=33%}
**End-user &**<br>**human-centric design**

::: {.text-micro}
Datami target audiences are citizen with a low technical background,
but eager to share their knowledge.

We want to make access to open data more inclusive 
and to give users an incentive to contribute.

Datami focuses on providing 
<b>
interfaces and data-visualisations
corresponding to common uses and habits.
</b>
:::
:::

::: {.column width=33%}
**Affordability &**<br>**technical simplicity**

::: {.text-micro}
Datami's architecture relies on the existence of robust FLOSS (Free/Libre and Open Source Software) solutions
for data storage and version control.

We designed Datami as a frontend solution only for open data production and valorisation.

Datami can be reused as a FLOSS solution
<b>
without needing to be installed on a server
</b>
and therefore be more easily mutualised. 
:::
:::

::::::



# Datami's goals for the next months

**Our main objectives to make Datami a gamechanger**<br>**in producing decentralized open data**

---

## **Goal #1**<br> Research & development <br>of missing core features

**Reaching a full launchable version**

<br>

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}

Today Datami is still a project in its earliest developments. 
While we already developed a lot of its core features in mid-2022, 
some 
<b>
other key features remains to be implemented 
</b>
so Datami could jump a necessary gap
for a full market launch.
:::

::: {.column width=50%}

The new developments anticipated in our roadmap involve a real effort
in terms of research & development. 
Most of the features we noted over time in our global roadmap
require to innovate in design and technical solutions
:::

::::::

:::


---

## **Goal #2**<br> Keep a global coherence of a<br> Free/Libre and Open Source Software project

**Enhance the appropriability of the solution by developpers and data producers**
<br>
**and explore a market segment**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}

Datami is a FLOSS project deep to its bones.
We are fully commited to produce good functional and auditable code, 
but we also value the other dimensions of FLOSS approach : 

- Provide UI/UX improving inclusiveness of the solution ;
- Empower future users with an extensive documentation ;
- Being able to communicate clearly about Datami's proposition value ;

:::

::: {.column width=50%}
UX and UI design are key dimensions to improve so 
<b>
any user, with or without technical knowledget,
can be welcome and incentived to contribute to open data and building digital commons of information. 
</b>

The technical documentation is a worksite in itself, as of the Datami's landing website.
Both are utterly important to 
<b>
help users engage in using broadly Datami.
</b>
:::

::::::

By converging on the several aspects of this goal we believe 
we may then 
<b>
explore further a forgotten market segment.
</b>
This segment is represented by all small-sized or local organisations 
wishing to produce open data respectfully of their comminities rights to privacy,
and despite limited resources and skills.

:::

---

## **Goal #3**<br>Exploring use cases<br>for new open data communities

**Empower communities by providing Datami for new use cases**


::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}

Since its beginning mid-2022 Datami users list
accounts for a more than dozen organisations,
some public other private, 
each of them having similar and specific expectations.

<b>
Specific needs 
</b>
are those about representing correctly a dataset
with a editorial point of view,
a curation customized depending on the datasets properties
and data producers needs. 

<b>
Similar needs
</b>
are those about having simple visualisation, 
affordable solution, lower the level technical knowledge both for contributors as much for data producers themselves.

:::

::: {.column width=50%}

Datami was developed step by step, each step corresponding 
to a 
<b>
"fit" between our roadmap and new actors needs 
in term of open data valorisation towards their communities. 
</b>
We believe the best way to develop a digital project lays in 
iterating as we propose the solution to new users.

To perfect Datami we found no better way 
than building each of its features according to new use cases, 
new actors eager to open their data to their audiences.

New use cases bring new challenges, each in their own way.

<b>
For Datami project the goal is to be able to answer to new needs
but in the frame of a global roadmap while doing so.
</b>
:::

::::::

:::


# **Goal #1**<br>Research & development <br>of missing core features

**Reaching a full launchable version**

---

## Datami technical properties

**What are the most innovative caracteristics of Datami already developped ?**

One of the most innovative approaches of the Datami project is its 
<b>
low-code and inclusive approach to open data.
</b>

Rather than creating a new platform from scratch, Datami relies on external platforms such as GitLab and GitHub for data storage and version control features, 
thus promoting a larger complementarity between FLOSS (Free/Libre and Open Source Software) solutions. 
This approach avoids small organisations to install and maintain servers,
and ultimately
<b>
helps making open data more affordable and transparent. 
</b>

Additionally, our emphasis on structured and interoperable datasets through 
data modeling and 
<b>
adherence to standards such as Frictionless' Table schema 
</b>
is an approach that promotes a common standard for open data, 
making it easier to compare and to integrate datasets with each other
for civic or scientific further reuses.

---

## The _stack_

:::::::::::::: {.columns}

::: {.column width="40%"}
The technical _stack_ is **entirely composed of _open source_ libraries**
::: 

::: {.column width="60%"}
- [`Vue.js (v2.x)`](https://v2.vuejs.org/v2/guide)
- [`VueX`](https://vuex.vuejs.org/)
- [`vue-custom-element`](https://github.com/karol-f/vue-custom-element)
- [`gray-matter`](https://www.npmjs.com/package/gray-matter)
- [`Showdown`](https://www.npmjs.com/package/showdown) + [`showdown-table extension`](https://github.com/showdownjs/table-extension#readme)
- [`Bulma`](https://bulma.io/) + [`Buefy`](https://buefy.org/)
- [`Material Design`](https://materialdesignicons.com/)
- [`Fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
- [`JSDiff`](https://github.com/kpdecker/jsdiff)
- [`Diff2html`](https://www.npmjs.com/package/diff2html)
- [`MapLibre`](https://maplibre.org)
- [`ApexCharts`](https://apexcharts.com)
:::

::::::::::::::

---

## Functional diagram  · current version

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
Elements colored in **turquoise** symbolize features already existing in Datami.
:::

::::::

:::

::: {.no-mt .h-50}
![](images/roadmaps/archi-devs-en.png)
:::


---

## Roadmap

**What are the features the NGI grant will help to develop ?**

::: {.text-micro}
Datami is a project currently in a early phase stage of developments. 
So far we have put in place most of its core features, 
such as providing customazible frontend web components,
the ability to dialog with Gitlab's / Github's / mediawiki's APIs to retrieve data and send contributions, 
and its ability to associate data models to open datasets.

<b>
That said some remaining core features are still in need to be developped
to make Datami fulfill its scope of promises. 
</b>

From the global roadmap of the Datami project 
we established a sub-roadmap specific to NGI search open call, 
roadmap we consider coherent with the main goals of the program.  
:::

---

### Technical challenges in future developments

The remaining and most challenging core features in need for developments include

:::::: {.columns}

::: {.column width=33%}

**Complete third-party**<br>**APIs integration**

::: {.text-micro}
Datami already includes the ability to
use Gitlab / Github / mediawiki APIS.

In addition to those services' APIS 
we plan to 
<b>
generalize the ability to retrieve and interact 
with other third-party services.
</b>

Those other external APIs might be 
Open Street Map,
SourceForge, 
or other REST APIs respecting international standards 
for common uses (request a dataset's facet for instance). 

We named that milestone "More data sources".
:::
:::

::: {.column width=33%}

**Online configuration**<br>**interfaces**

::: {.text-micro}
Since we begun to develop Datami 
we recurrently received the demand for 
having 
<b>
simple interfaces to set up 
new widgets for specific open datasets.
</b>

This demand - as simple it may seems - 
implies a strong effort in R&D, 
equally in its UX design dimension than in
its technical side to store and secure 
configuration files on one or several repositories.
:::
:::

::: {.column width=33%}

**Online moderation**<br>**interfaces**

::: {.text-micro}
Another demand that emerged from current users
is for simple interfaces for managing contributions' moderation by data producers.

Gitlab merge requests moderation interfaces
are mainly designed for people with a technical background higher than usual.
As we expect to let 
<b>
citizen contribute and data producers moderate these contributions
we can't be expecting them to adapt themselves to Gitlab's interfaces.
</b>

Developping such moderation interfaces has to
include R&D in UI/UX design and similar technical responses
than for the configuration interfaces.
:::
:::

::::::

---

### Milestones

:::::: {.columns}

::: {.column width=40%}

::: {.text-micro}
We identified several milestones corresponding to 
the missing core features remaining to develop
to make Datami fully responding to users' needs.


The detail of the tasks we anticipated for each milestone
is precised later in the document 
in the sections about the
<b>
provisional budget
</b>
and  the
<b>
provisional timelines.
</b>

:::

:::

::: {.column width=60%}

|**Family**|**Category**|**Milestones**|**Priority**|**Difficulty**|
|:---:|:---:|------------|------|------|
|TECH|Implementing|<b>More data sources</b>|🔴 high|▪️ easy|
|TECH|Implementing|<b>Online widget configuration</b>|🔴 high|▪️▪️▪️ hard|
|TECH|Implementing|<b>Manage contribution widget</b>|🔴 high|▪️▪️▪️ hard|
|TECH|Implementing|<b>Better UX - data management</b>|🟡 low|▪️▪️ medium|
|TECH|Implementing|<b>Better UX - data interaction</b>|🟡 low|▪️▪️ medium|
|TECH|Implementing|<b>Better UX - maps</b>|🟡 low|▪️▪️ medium|
|TECH|Implementing|<b>Better UI - new views</b>|🟡 low|▪️▪️ medium|
|TECH|Implementing|<b>Refactoring</b>|🟠 medium|▪️▪️▪️ hard|
|TECH|Implementing|<b>Tests</b>|🟡 low|▪️▪️▪️ hard|
|BIZDEV|OnBoarding|<b>Project management</b>|🔴 high|▪️ easy|
|BIZDEV|Adopting|<b>Community management</b>|🔴 high|▪️▪️▪️ hard|

:::

::::::


---

### Functional diagram · Future developments

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
Elements colored in **turquoise** symbolize features already existing in Datami.

Elements in <span class="text-secondary">orange</span> 
symbolize the main milestones to be developped 
as described in the roadmap 
(cf. sections about our 
<b>
provisional budget
</b>
and 
<b>
provisional timelines
</b>
later in this document).

:::

::: {.column width=50% .mt-default-bis}
- <soan class="text-secondary">A</soan> : <b>Online widget configuration</b>
- <soan class="text-secondary">B</soan> : <b>Manage contribution widget</b>
- <soan class="text-secondary">C</soan> : <b>More data sources</b>
:::

::::::

:::

::: {.no-mt .h-50}
![&nbsp;](images/roadmaps/archi-devs-roadmap-en.png)
:::









# **Goal #2**<br>Keep a global coherence of a<br> Free/Libre and Open Source Software project

**Explore a forgotten market segment and**
<br>
**enhance the appropriability of the solution by developpers and data producers**

---

## Existing open source audits

**We were audited in late 2022
by the cabinet Smile**
<br>
on the open source dimensions of the Datami project. 


:::::: {.columns}

::: {.column width=30%}

::: {.text-micro}
Smile helped us formalising different recommandations 
about 
<b>
Datami's open source strategies, its functional roadmap, or even about the project's governance.
</b>

::: {.text-center}
![](images/audits/smile-logo.png)
:::

All the documents related to this audit can be asked for 
by contacting us at : 
[contact@multi.coop](mailto:contact@multi.coop)
:::

:::

::: {.column width=70%}
![&nbsp;](images/audits/smile-01.png)
:::

::::::

---

## Explore a forgotten market segment

**Given the solutions on the market Datami has a role to play**<br>**for small-sized data producers**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
The benchmark we conducted tends to show us that 
only proprietary solutions had targeted or/and convinced 
a broad range of organisations to use their solution.

Open source solutions for edition or data-visualisation
are usually targeting audiences with 
higher technical background than average.

Furthermore open source solutions when not used in Saas
all need to install and maintain a backend service
to keep a sovereign use of said solutions.

Those caracteristics usually 
exclude small-sized or local organisations 
with mimited skills or resources from adopting
open source solutions while producing open data.

:::

::: {.column width=50%}

We believe Datami may have an unique place 
in this market landscape due to the 
specifications we prioritized in its development :

- <b>it's designed for a broad audience ;</b>
- <b>it's designed to make open contributions ;</b>
- <b>it includes a moderation process ;</b>
- <b>it has no specific backend ;</b>
- <b>it's open source ;</b>
:::

::::::

:::

---

## Build a coherent commercial offer

**We aim to propose different services aroud Datami**

<br>

::: {.text-micro}

:::::::::::::: {.columns}

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-setup.png)

::: {.text-center}
**Datami setup**
:::

Multi can provide an economic package
for a few days for support in setting up Datami
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-pack.png)

::: {.text-center}
**Datami as free software** 
:::

Use Datami as it is based on the documentation, 
you are free!
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-custom_dev.png)

::: {.text-center}
**Custom developments** 
:::

We can help you responding to you needs
using Datami while adding new features 
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-setup-interface.png)

::: {.text-center}
**Online configuration** \*
:::

Use a simple interface to setup yourself the 
widget you need for your datasets.

_\* This offer is currently under construction_

:::

::::::::::::::

::: 

::: {.text-micro}
In order to make Datami as accessible as possible, our principle is to 
<b>
share design and development costs.
</b>
<br>
All developments - even minimal ones - contributing to improve Datami thus ultimately benefit all users.
:::

---

## Maintain the documentation up to date

**The technical documentation remains a keystone of any FLOSS project**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
We started making the documentation as clear as possible 
since the very beginning of the project.

Keeping up with the documentation as the rest of the project
continues in its other dimensions
(business model, iterations, debugging, ...)
is a real challenge.
:::

::: {.column width=50%}

<b>
We adapted our roadmap 
so every new iteration and new feature development 
includes now its own documentation time 
into its provisional timing.
</b>
:::

::::::

:::

---

## Enhance the bug reporting & <br>new features demands processes

:::::: {.columns}

::: {.column width=50%}

**As for any FLOSS project**
<br>
**issues and improvements**
<br>
**can be publicly reported**

::: {.text-micro}
Our source code is fully open sourced 
on Gitlab with a miror repository on Github.

We are continuously improving our documentation 
but we are well aware we could do more 
to help users present and future 
in participating more actively in the project.
:::

:::

::: {.column width=50%}

::: {.text-micro}
There are some leads we mentionned in our global roadmap
as means to improve the way demands may be brought back to us :

- <b>Issues templates</b>
- <b>Merge requests templates</b>
- <b>Contributing & rules of conduct files</b>
- <b>Welcome pack</b>
- <b>Governance diagram</b>
:::
:::

::::::

---

## Translate documentation & application <br>into other languages

**Datami was from the start designed to be internationalized.**


::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}

For the moment Datami project - including 
the application, the official website, and the technical documentation -
are translated into French and English.

<b>
We value the language as a mean to make our solution 
more accessible and more inclusive, either for target audiences or data producers.
</b>
:::

::: {.column width=50%}

We included in our roadmap
the translation to spanish as one of our next major improvements.
That being, we also consider translating to other languages (such as German or Italian)
as possibilities, depending on the needs of our next use cases. 

:::

::::::

:::

---

## Make the repositories more visible <br>to FLOSS communities

**We are starting to implement a communication strategy**<br>**towards open source communities**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}

Even if they are essential to the project 
we know that "just" having a new open source solution, 
its technical documentation, and a value proposition, 
may not be enough to alone encourage developpers to contribute.

:::

::: {.column width=50%}

We draw and wa are currently testing different 
<b>
strategies to promote Datami
to open data and open source communities,
</b>
such in the vue.js, datavisualization, javascript, or other potential 
low-level users and contributors communities on Reddit, Twitter, Linkedin, or DevTo...

:::

::::::

:::






# **Goal #3**<br> Exploring use cases<br>for new open data communities

**Empower communities by providing Datami for new use cases**

---

## Users & contributors profiles

We identified three main user profiles for Datami, <br>
depending on their role in 
<b>
producing open data as inclusive digital commons.
</b>

:::::: {.columns .text-justify}

::: {.column width="33%"}

**Data producers**

::: {.text-micro}
As we mentionned throughout this document
we target small and local organisations
with limited resources 
but in dire need to produce and share information
about the topic thez are specialized in.  

Due to their lack of resources 
they also need their communities 
to participate in the consolidation of the 
datasets published in the first place.
Data producers are ultimatlely responsible
of the data content and publication.

<b>
Thus data producers first expectation is
to encourage their communities to contribute,
but also to be able to moderate these grassroot contributions.
</b>

:::

:::

::: {.column width="33%"}

**Citizen**

::: {.text-micro}
By citizen we mean here any invidual 
potentially concerned by informations published
by the data producers.

Using such a generic term we want to highlight the fact we consider 
that any individual should have the right to participate in 
contributing to information of general interest, 
whomever this information is produced by.

<b>
This consideration implies to build interfaces 
accessible to any individual, regardless of their
sociological or technological background. 
</b>
:::

:::

::: {.column width="33%"}

**Low level**<br>**contributors**

::: {.text-micro}
This family of users have more technical background than average, 
but their field of technical expertise can vary.

Some could posses a level of know-how in vue.js for instance, 
and contribute directly to Datami's source code.

Others can have knowledge on data models or standards,
such as Frictionless',
and may then contribute to help data producers
in structuring their data 
or make it more interoperable.

<b>
To adress and include this profile 
Datami project must 
give a special importance to all its documentation.
</b>
:::

:::

::::::

---

## Our first users

**Since late 2022 we implemented Datami for a dozen organisations,**<br>**some public and other private**

::: {.text-micro}

Each organisation may at the time have had one or several datasets to expose and share.
<br>
The needs we covered were related to data-visualisation, cartography, embeddibility, reproductibility... 
<br>
<b>
The expression of needs along the way participated to build our roadmap,
</b>
feature demand by feature demand.

:::::: {.columns}

::: {.column width=35%}

Current use cases include datasets focusing on a variety of topics of public interest :

- Indicators about places providing public digital services (France services) ;
- Indicator on digital parental fragility ;
- Open data producers ;
- Third places characteristics ;
- Alternative mobility projects ...


:::

::: {.column .img-no-margin  width=65%}
![](images/clients/clients.png)
:::

::::::

:::


---

## Analytics

**We use the open souce solution Matomo as our main analytics platform.**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
The metrics are visible for everyone 
on this [link](https://multi.matomo.cloud/index.php?module=CoreHome&action=index&idSite=1&period=range&date=previous30#?period=range&date=2022-08-28,2023-03-25&category=Dashboard_Dashboard&subcategory=1&idSite=1)
and are set to preserve the privacy of Datami's users.

Our metrics shows 
<b>
a slow but continuous trend of growth in the using of Datami
</b>
amongst our first users.
:::

::: {.column width=50%}

We also observed the peaks in our metrics may be related
in events - either organized by multi or by stakeholders -
boosting up the visibility of datasets exposed with Datami
towards their target audiences.
:::

::::::

:::

::: {.img-pdf}
![](images/matomo/matomo-datami_widget-02.png)
:::

---

## Prospection strategy

**Finding new use cases and promoting Datami**

::: {.text-micro}

We are actively participating to several events to promote Datami and looking out for new use cases.

:::::: {.columns .text-justify}

::: {.column width=50%}
For instance we presented Datami to 
the "[NEC](https://numerique-en-communs.fr/la-programmation-2022/boite-a-outils-et-bonnes-pratiques-de-la-recolte-au-partage-pour-saisir-manipuler-analyser-et-partager-la-donnee/)" event (_Digital In Commons_) in Lens city in late 2022, 
we will organize a workshop during the "[Journées du Logiciel Libre](https://jdll.org/user/pages/03.programme/JdLL_2023-programme.pdf)" in Lyon in April 2023, 
or to the [OW2 annual conferences](https://www.ow2con.org/view/2023/) in Paris next June.
:::

::: {.column width=50%}
We are also directly in contact with several national organisations in France 
and currently working with them to find ways implement Datami 
given their needs.
:::

::::::

:::

<b>
With the act of applying for a NGI grant we hope we could make a difference 
by devoting more resources to this market exploration.
</b>






# The team

**Who are the people behind Datami ?**

---

## A project by the tech cooperative multi

<br>

:::::::::::::: {.columns}

::: {.column width="30%"}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)

::: {.text-center}
[https://multi.coop](https://multi.coop)
:::

:::

::: {.column width="70%"}
Our cooperative contributes to the development of **digital commons** and associated services, by bringing together a community of professionals working for a **digital of general interest**
::: 

::::::::::::::

---

## Datami's team members

**Dedicated, experienced and multi-disciplinary profesionals**

---

## Julien Paris

### Developer  · product owner

:::::::::::::: {.columns}

::: {.column .img-team .img-pdf width=25%}
![](images/team/julien-paris-01.png)

::: {.text-nano}
&nbsp; ✉️ [julien.paris@multi.coop](mailto:julien.paris@multi.coop)
:::

:::

::: {.column .text-micro width=75%}
After initial training as a DPLG architect, then having carried out cultural projects in international cultural cooperation (Spanish embassy in Mexico, French embassy in Egypt), doing research (CNRS doctoral student in Turkey), I finally switched to digital as a _fullstack_ developer.

I only develop open source softwares, thus hoping to support the **open data and free software movement**. I am particularly interested in _data visualization_ and open data contribution processes.

Since 2015 I have **directed and developed digital projects** for french ministerial institutions (ANCT, Bercy, CGET, Agence Bio), inter-ministerial (DINUM), associations and think tanks involved in general interest (Mednum , PiNG, Ternum, Rhinocc, Décider Ensemble), and for other public structures such as a public mediatheque nearby Nantes.

I participated in Etalab's "General Interest Entrepreneur" program in 2018. I joined Johan Richer in 2021 to initiate the transformation of Jailbreak into a tech cooperative, which then became multi in 2022.
:::

::::::::::::::

---

## Johan Richer

### Open data consultant · product manager

:::::::::::::: {.columns}

::: {.column .img-pdf .img-team width=25%}

![](images/team/johan-richer.jpg)

::: {.text-nano}
&nbsp; ✉️ [johan.richer@multi.coop](mailto:johan.richer@multi.coop)
:::

:::

::: {.column .text-micro width=75%}
Coming from a training in political sociology and international relations and after a stint at the Franch Ministry of Foreign Affairs, I discovered digital by participating in various collaborative mapping initiatives for humanitarian purposes in France and abroad: CartONG, MapFugees, Missing Maps …

I then joined Etalab, the French agency responsible for opening public data, to work on open government topics and create a **“digital toolbox”**

It was with other Etalab alumni that I founded Jailbreak in 2017 with the ambition to put free software, open data, open government and agile methods **to serve the general interest **. Jailbreak will become multi with the transition to SCOP in 2022.

I am also co-founder and treasurer of Code for France.
:::

::::::::::::::

---

## Amélie Rondot

### Developer · data analyst

:::::::::::::: {.columns}

::: {.column .img-team .img-pdf width=25%}
![](images/team/amelie-rondot-square.jpg)

::: {.text-nano}
&nbsp; ✉️ [amelie.rondot@multi.coop](mailto:amelie.rondot@multi.coop)
:::

:::

::: {.column .text-micro width=75%}
I was trained as a hydraulics engineer and worked for several years in the fields of drinking water and sanitation in project management and water services management companies. Then, after having worked for two years as a bicycle mechanic, I discovered a strong interest for digital development. 
**So I started a new reconversion to become a developer.**

A first experience of one year and a half in DevOps within the start-up meteo*swift 
allowed me to develop my skills in backend development and in 
deployment and maintenance of microservices on the cloud.

Driven by the desire to contribute to the **development of open source software and to work in an ethical environment,** I was seduced by the way multi works and the projects carried by the team.
:::

::::::::::::::

---

## Multi.coop · References

:::::::::::::: {.columns}

::: {.column .text-center width=25%}
**Vitrine**<br>&nbsp;

::: {.img-pdf}
![](images/references/vitrine1.png)
:::

::: {.text-nano .text-justify}
To easily generate a website using content retrieved from third party pages.

Originally, its main use case was for organizing hackathons, to present its projects and participants, datasets, tools, etc.

[For more info](https://gitlab.com/multi-coop/vitrine)
:::

:::

::: {.column .text-center width=25%}
**catalogue**<br>**.data.gouv.fr**

::: {.img-pdf}
![](images/references/cataloguedatagouv.png)
:::

::: {.text-nano .text-justify}
catalog.data.gouv.fr is an online service that allows organizations to create, manage and open their dataset catalogs.

This project follows an investigation of which [the synthesis](https://multi-coop.gitlab.io/slides/investigation-catalogue/synthese.html) is an example of the methodology we propose.

[For more info](https://catalogue.data.gouv.fr/)
:::

:::

::: {.column .text-center width=25%}
**Validata**<br>&nbsp;

::: {.img-pdf}
![](images/references/validata-01.png)
:::

::: {.text-nano .text-justify}
Validata aims to offer a platform for validating open data.

It is aimed at French local authorities that want to validate 
the quality and interoperability of the data they publish using an external tool. 
It also allows data warehouse managers to qualiﬁer the integrity of the data 
they wish to exploit before importing it into a multi-source database.

[For more info](https://validata.fr/)

:::

:::

::: {.column .text-center width=25%}
**DBnomics**<br>&nbsp;

::: {.img-pdf}
![](images/references/dbnomics-01.png)
:::

::: {.text-nano .text-justify}
DBnomics is one of the largest economic databases in the world, 
aggregating hundreds of millions of time series from dozens of sources 
and making them available through a single API.

[For more info](https://db.nomics.world/)

:::

:::

::::::::::::::







# Provisional budget

**What will be used the grant for?**

---

### Global budget 

::: {.text-micro .text-justify}

:::::: {.columns}

::: {.column width=50%}

The global roadmap of Datami encompasses many insights and demands we had throughout our exchanges with our users and clients.
<b>
For each demand we derived the corresponding features in need for developments
and packed them into major categories and milestones.
</b>

:::

::: {.column width=50%}

This work of discretizing the needs into features allows us
to draw a quite precise idea of the resources to mobilize
while keeping a lean approach in terms of project management.

:::

::::::

:::


### Legend

::: { .text-micro}
- <b>Family</b> : Family of the task between Tech (technical) / Bizdev (business development) ;
- <b>Category</b> : Category of the task between Onboarding / Implementing / Adopting / Backlog ;
- <b>Milestone</b> : Sub-family of tasks aiming to develop a specific feature ;
- <b>Dev + man.</b> : Development and project management time estimate ;
- Symbols used in the following tables represent either a certain level of priority or difficulty.
:::

<hr>

:::::: {.columns}

::: {.column width=35%}
::: { .text-micro}
<b>Priority</b> is an estimation of the criticity / need of a particular feature as expressed by users or clients.
:::
:::

::: {.column .text-micro width=15%}

| Symbol    | Meaning |
| :-------: | ------  |
| 🔴        | High    |
| 🟠        | Medium  |
| 🟡        | Low     |

:::

::: {.column width=35%}
::: { .text-micro}
<b>Difficulty</b> is an estimation of the complexity to realize the feature by the team, 
a higher level of complexity also being synonymous of a higher risk of unexpected problems to resolve along the way.   
:::
:::

::: {.column .text-micro width=15%}

| Symbol | Meaning |
| :----  | ------  |
| ▪️▪️▪️    | Hard    |
| ▪️▪️     | Medium  |
| ▪️      | Easy    |

:::


:::::::

---

## Global budget

::: {.text-center} 
**Regrouped by category and milestones**
:::

|**Family**|**Category**|**Milestones**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|------------|------|------|---:|---:|
|TECH|Implementing|<b>More data sources</b>|🔴 high|▪️▪️ medium|<b> 15 days</b>|<b>12 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|🔴 high|▪️▪️▪️ hard|<b> 15 days</b>|<b>12 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|🔴 high|▪️▪️▪️ hard|<b> 16 days</b>|<b>12 800 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|🟡 low|▪️▪️ medium|<b> 37 days</b>|<b>29 600 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|🟡 low|▪️▪️ medium|<b> 26 days</b>|<b>20 800 €</b>|
|TECH|Implementing|<b>Better UX - maps</b>|🟡 low|▪️▪️ medium|<b> 15 days</b>|<b>12 000 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|🟠 medium|▪️ easy|<b> 4 days</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UI - new views</b>|🟡 low|▪️▪️ medium|<b> 19 days</b>|<b>15 200 €</b>|
|TECH|Implementing|<b>Refactoring</b>|🟠 medium|▪️▪️▪️ hard|<b> 18 days</b>|<b>14 400 €</b>|
|TECH|Implementing|<b>Tests</b>|🟡 low|▪️▪️▪️ hard|<b> 7 days</b>|<b>5 600 €</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|🔴 high|▪️ easy|<b> 3 days</b>|<b>2 400 €</b>|
|BIZDEV|OnBoarding|<b>Translations</b>|🟡 low|▪️ easy|<b> 2 days</b>|<b>1 600 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|🔴 high|▪️▪️▪️ hard|<b> 7 days</b>|<b>5 600 €</b>|
|||<b>TOTAL</b>|||**184 d.**|**147 200 €**|

---

## Detailed budget

---

### Detailed budget & roadmap · 1/2

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Backlog|<b>More data sources</b>|Connector to SourceForge|🟡|▪️▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Backlog|<b>More data sources</b>|Connector to BitBucket|🟡|▪️▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>10 d.</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>10 d.</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>10 d.</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>4 d.</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Add / drag-drop a picture in a cell|🟡|▪️▪️|<b>6 d.</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>12 d.</b>|<b>9 600 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Full screen - debug|🟠|▪️▪️|<b>6 d.</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Range filter|🟡|▪️|<b>4 d.</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Change width column|🟠|▪️▪️|<b>8 d.</b>|<b>6 400 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Helper at loader|🟡|▪️|<b>2 d.</b>|<b>1 600 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>|<b>4 800 €</b>|


---

### Detailed budget & roadmap · 2/2

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>Better UX - maps</b>|Add or edit geojson objects|🟡|▪️▪️▪️|<b>15 d.</b>|<b>12 000 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>4 d.</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UI - new views</b>|Agenda view|🟡|▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Adopting|<b>Better UI - new views</b>|Graphs view with D3js|🟡|▪️▪️|<b>10 d.</b>|<b>8 000 €</b>|
|TECH|Adopting|<b>Better UI - new views</b>|Simultaneous dataviz + map on same view|🟡|▪️▪️|<b>4 d.</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Refactoring</b>|Put all git* requests into a package|🟡|▪️▪️▪️|<b>8 d.</b>|<b>6 400 €</b>|
|TECH|Implementing|<b>Refactoring</b>|Migration to Typescript|🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Backlog|<b>Refactoring</b>|Migration to Vue3|🟡|▪️▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Implementing|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>|<b>5 600 €</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>3 d.</b>|<b>2 400 €</b>|
|BIZDEV|OnBoarding|<b>Translations</b>|Translate to Spanish|🔴|▪️|<b>2 d.</b>|<b>1 600 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>4 d.</b>|<b>3 200 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Set up showcases|🟡|▪️▪️|<b>3 d.</b>|<b>2 400 €</b>|

<br>

|&nbsp;|&nbsp;|&nbsp;|&nbsp;|&nbsp;|&nbsp;|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|------|------------|:---:|---|---:|---:|
||||||<b>TOTAL</b>|**184 d.**|**147 200 €**|


--- 

## Budget analysis

::: {.text-center} 
**Priority · _vs_ · Difficulty**
:::

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
    In days
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high| 20 days| 8 days| 13 days|<b> 41 days</b>|
|🟠 medium| 17 days| 27 days|  days|<b> 44 days</b>|
|🟡 low| 51 days| 37 days| 11 days|<b> 99 days</b>|
|**TOTAL**|<b> 88 days</b>|<b> 72 days</b>|<b> 24 days</b>|**184 days**|

:::


::: {.column width=50%}

::: {.text-center}
<b>
    In euros    
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|16 000 €|6 400 €|10 400 €|<b>32 800 €</b>|
|🟠 medium|13 600 €|21 600 €|  €|<b>35 200 €</b>|
|🟡 low|40 800 €|29 600 €|8 800 €|<b>79 200 €</b>|
|**TOTAL**|<b>70 400 €</b>|<b>57 600 €</b>|<b>19 200 €</b>|**147 200 €**|

:::

::::::








# Provisional timelines

**A lean approach for iterative and incremental developments**

---

## Timelines

<br>

::: {.text-center}

<b>
The following timelines are
to be considered as only 
provisional.
</b>

These timelines may be adapted depending on
<br> 
the context, our resources, 
and the priority of demands 
<br>
emerging from  new use cases. 
:::


---

### Timeline · Technical tasks

::: {.ngi-timeline}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|T3|T4|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|TECH|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||||
|TECH|<b>More data sources</b>|Connector to SourceForge|🟡|▪️▪️▪️|<b>5 d.</b>|||||||<span class="cell-bg">x</span>||
|TECH|<b>More data sources</b>|Connector to BitBucket|🟡|▪️▪️▪️|<b>5 d.</b>|||||||<span class="cell-bg">x</span>||
|TECH|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>10 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>5 d.</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>10 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||||
|TECH|<b>Better UX - data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>10 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>4 d.</b>|||||<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data management</b>|Add / drag-drop a picture in a cell|🟡|▪️▪️|<b>6 d.</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>12 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data interaction</b>|Full screen - debug|🟠|▪️▪️|<b>6 d.</b>|||||<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data interaction</b>|Range filter|🟡|▪️|<b>4 d.</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data interaction</b>|Change width column|🟠|▪️▪️|<b>8 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||||
|TECH|<b>Better UX - data interaction</b>|Helper at loader|🟡|▪️|<b>2 d.</b>||<span class="cell-bg">x</span>|||||||
|TECH|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>||||<span class="cell-bg">x</span>|||||
|TECH|<b>Better UX - maps</b>|Add or edit geojson objects|🟡|▪️▪️▪️|<b>15 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UI - new views</b>|Agenda view|🟡|▪️|<b>5 d.</b>||||<span class="cell-bg">x</span>|||||
|TECH|<b>Better UI - new views</b>|Graphs view with D3js|🟡|▪️▪️|<b>10 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Better UI - new views</b>|Simultaneous dataviz + map on same view|🟡|▪️▪️|<b>4 d.</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Refactoring</b>|Put all git* requests into a package|🟡|▪️▪️▪️|<b>8 d.</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Refactoring</b>|Migration to Typescript|🟠|▪️▪️▪️|<b>5 d.</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Refactoring</b>|Migration to Vue3|🟡|▪️▪️▪️|<b>5 d.</b>|||||<span class="cell-bg">x</span>||||
|TECH|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||



:::

---

### Timeline · Bizdev tasks

::: {.ngi-timeline}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|T3|T4|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|BIZDEV|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|BIZDEV|<b>Translations</b>|Translate to Spanish|🔴|▪️|<b>2 d.</b>||<span class="cell-bg">x</span>|||||||
|BIZDEV|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|BIZDEV|<b>Community management</b>|Set up showcases|🟡|▪️▪️|<b>3 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||

:::








#

::: {.text-center}
**Thanks for your attention !**
:::

<br>

::::::::::::: {.columns}

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::
:::

::: {.column width="20%"}
::: {.text-micro .text-center}
[Datami](https://datami.multi.coop/?locale=en)

is a project led by the cooperative

[multi](https://multi.coop)
:::
:::

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)
:::
:::

:::::::::::::


::: {.text-center}
**contact@multi.coop**
:::

---

## Our sponsors in 2022

Datami was **laureate of the Plan France Relance 2022** and has benefited from the support of the following organizations

![&nbsp;](images/sponsors/sponsors.png)

---

::: {.text-center}
[Slides source](https://gitlab.com/multi-coop/datami-project/datami-slides-fr)
:::

