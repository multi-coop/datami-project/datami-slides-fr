
# Our sponsors in 2022-2023

<br>

:::{.text-micro}
Datami project was **laureate of the "Plan France Relance 2022"**
and has benefited from the support of the following organizations
![&nbsp;](images/sponsors/sponsors.png)

Julien Paris (Datami's product owner and lead developer) is **laureate of the 1st NGI Enrichers** european fellowship program.
He will stay for 6 months in Montreal (Canada) in 2024, in collaboration with the Concordia University and Mitacs.

:::{.text-center}
:::{.columns}
:::{.column width="33%"}
![&nbsp;](images/sponsors/ngi_enrichers-logo.png)
:::
:::{.column width="33%"}
![&nbsp;](images/sponsors/logo-concordia-university-montreal.png)
:::
:::{.column width="33%"}
![&nbsp;](images/sponsors/mitacs-logo.png)
:::
:::
:::
:::

# {#credits name='credits}

::: {.text-center}
**Thanks for your attention !**
:::

<br>

::::::::::::: {.columns}

::: {.column width="40%"}
::: {.img-nano .text-center}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::
:::

::: {.column width="20%"}
::: {.text-micro .text-center}
[Datami](https://datami.multi.coop/?locale=en)

is a project
led by the
<br>
digital cooperative

[multi](https://multi.coop)
:::
:::

::: {.column width="40%"}
::: {.img-nano .text-center}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)
:::
:::

:::::::::::::

::: {.text-center}
**contact@multi.coop**
:::

<!-- ---

::: {.text-center}
[Slides source](https://gitlab.com/multi-coop/datami-project/datami-slides-fr)
::: -->
