---
title: Datami
subtitle: <strong>Rétroplanning 2023</strong>
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _développeur fullstack_
  </span><br>
  <span class="text-micro text-author-details">
    _co-fondateur de la [coopérative multi](https://multi.coop)_
  </span>
'
date: <span class="emph-line">14 mars 2023</span><br>Plan en pressant <span class="text-nano">`echap`</span><br>Versions &colon; [fr](https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-fr.html) / [en](https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-en.html)
title-slide-attributes:
  data-background-image: "images/logos/logo-DATAMI-rect-colors.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 5%"
  data-background-position: "right 10% top 40%, right 4% top 40%"
---

# GLOBAL

---

**Global - tech**

---

<table class="table-retroplanning">
    <thead>
        <tr>
            <th style="border-top: none; border-left: none; border-right: none;"></th>
            <th style="border-top: none; border-left: none; border-right: none;"></th>
            <th style="border-top: none; border-left: none; border-right: none;"></th>
            <th style="border-top: none; border-left: none; border-right: none;"></th>
            <th colspan=4 style="border-top: none; border-left: none; border-right: none;">2023</th>
            <th colspan=4 style="border-top: none; border-left: none; border-right: none;">2024</th>
        </tr>
    </thead>
    <tbody>
        <tr style="background:whitesmoke; font-weight:bold;">
            <td>**Milestones**</td>
            <td>**Features**</td>
            <td>**Priority**</td>
            <td>**Difficulty**</td>
            <td>T1</td>
            <td>T2</td>
            <td>T3</td>
            <td>T4</td>
            <td>T1</td>
            <td>T2</td>
            <td>T3</td>
            <td>T4</td>
        </tr>
        <!--  -->
        <tr>
            <td rowspan=5 style="font-weight:bold;">
                More data sources
            </td>
            <td>
            Connect to external APIs - generic
            </td>
            <td class=text-center>🔴</td>
            <td>▪️▪️</td>
            <td></td>
            <td colspan=3 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">15 d.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
            Connect to external APIs - OSM
            </td>
            <td class=text-center>🟡</td>
            <td>▪️</td>
            <td></td>
            <td></td>
            <td colspan=3 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">8 d.</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
            Activity pub integration
            </td>
            <td class=text-center>🟡</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=3 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">18 d.</td>
        </tr>
        <tr>
            <td>
            Connector to SourceForge
            </td>
            <td class=text-center>🟡</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">5 d.</td>
            <td></td>
        </tr>
        <tr>
            <td>
            Connector to BitBucket
            </td>
            <td class=text-center>🟡</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">5 d.</td>
            <td></td>
        </tr>
        <!--  -->
        <tr style="background:whitesmoke;">
            <td rowspan=2 style="font-weight:bold;">
                Online widget configuration
            </td>
            <td>Interactive interface + preview</td>
            <td class=text-center>🔴</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td colspan=3 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">20 d.</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>Save new config to git repo</td>
            <td class=text-center>🔴</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=2 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">10 d.</td>
            <td></td>
            <td></td>
        </tr>
        <!--  -->
        <tr>
            <td rowspan=2 style="font-weight:bold;">
                Manage contribution widget
            </td>
            <td>Interface loading last PRs from repo</td>
            <td class=text-center>🔴</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=3 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">17 d.</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Accept / moderate PR + messages</td>
            <td class=text-center>🔴</td>
            <td>▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=2 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">12 d.</td>
            <td></td>
            <td></td>
        </tr>
        <!--  -->
        <tr style="background:whitesmoke;">
            <td rowspan=2 style="font-weight:bold;">
                Protect widget with password
            </td>
            <td>Protect access before showing widget</td>
            <td class=text-center>🟠</td>
            <td>▪️</td>
            <td></td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">6 d.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>Special token for protected widget</td>
            <td class=text-center>🟠</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">6 d.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <!--  -->
        <tr>
            <td rowspan=5 style="font-weight:bold;">
                Better UX - data management
            </td>
            <td>Add a new column + update schema</td>
            <td class=text-center>🟠</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td colspan=2 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">10 d.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Drag & drop CSV to widget</td>
            <td class=text-center>🟡</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=2 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">10 d.</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Save CSV file to Git repo</td>
            <td class=text-center>🟡</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">4 d.</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Add / drag-drop a picture in a cell</td>
            <td class=text-center>🟡</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">6 d.</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Cache user changes / branch until pushing  </td>
            <td class=text-center>🟠</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=3 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">12 d.</td>
            <td></td>
            <td></td>
        </tr>
        <!--  -->
        <tr style="background:whitesmoke;">
            <td rowspan=6 style="font-weight:bold;">
                Better UX - data interaction
            </td>
            <td>Full screen - debug</td>
            <td class=text-center>🟠</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">6 d.</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>Range filter</td>
            <td class=text-center>🟡</td>
            <td>▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">4 d.</td>
            <td></td>
            <td></td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>Change width column</td>
            <td class=text-center>🟠</td>
            <td>▪️▪️</td>
            <td></td>
            <td colspan=2 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">8 d.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>Helper at loader</td>
            <td class=text-center>🟡</td>
            <td>▪️</td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">2 d.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>Better integration of Frictionless data packages</td>
            <td class=text-center>🟡</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=4 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">12 d.</td>
            <td></td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>
            Export as pdf
            </td>
            <td class=text-center>🟡</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=2 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">12 d.</td>
        </tr>
        <!--  -->
        <tr>
            <td rowspan=2 style="font-weight:bold;">
                Better UX - maps
            </td>
            <td>Inject data to vector tiles on map view</td>
            <td class=text-center>🟡</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=2 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">8 d.</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Add or edit geojson objects</td>
            <td class=text-center>🟡</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=5 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">32 d.</td>
        </tr>
        <!--  -->
        <tr style="background:whitesmoke;">
            <td rowspan=2 style="font-weight:bold;">
                Better UI - customization
            </td>
            <td>Custom styles / CSS / logos</td>
            <td class=text-center>🟠</td>
            <td>▪️</td>
            <td></td>
            <td colspan=2 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">4 d.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>
            Accessibility
            </td>
            <td class=text-center>🟠</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td colspan=5 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">12 d.</td>
            <td></td>
        </tr>
        <!--  -->
        <tr>
            <td rowspan=3 style="font-weight:bold;">
                Better UI - new views
            </td>
            <td>Agenda view</td>
            <td class=text-center>🟡</td>
            <td>▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">8 d.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Graphs view with D3js</td>
            <td class=text-center>🟠</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=2 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">10 d.</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
            Simultaneous dataviz + map on same view
            </td>
            <td class=text-center>🟡</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=2 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">9 d.</td>
            <td></td>
        </tr>
        <!--  -->
        <tr style="background:whitesmoke;">
            <td rowspan=3 style="font-weight:bold;">
                Refactoring
            </td>
            <td>Put all git* requests into a package</td>
            <td class=text-center>🟡</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=4 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">15 d.</td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>Migration to Typescript</td>
            <td class=text-center>🟠</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=3 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">15 d.</td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>Migration to Vue3</td>
            <td class=text-center>🟠</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=3 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">18 d.</td>
            <td></td>
        </tr>
        <!--  -->
        <tr style="border-bottom: 1px solid grey;">
            <td rowspan=1 style="font-weight:bold;">
                Tests
            </td>
            <td>Functional & unit tests</td>
            <td class=text-center>🟡</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td colspan=4 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">18 d.</td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>

---

**Global - bizdev**


<table class="table-retroplanning">
    <thead>
        <tr>
            <th style="border-top: none; border-left: none; border-right: none;"></th>
            <th style="border-top: none; border-left: none; border-right: none;"></th>
            <th style="border-top: none; border-left: none; border-right: none;"></th>
            <th style="border-top: none; border-left: none; border-right: none;"></th>
            <th colspan=4 style="border-top: none; border-left: none; border-right: none;">2023</th>
            <th colspan=4 style="border-top: none; border-left: none; border-right: none;">2024</th>
        </tr>
    </thead>
    <tbody>
        <tr style="background:whitesmoke; font-weight:bold;">
            <td>**Milestones**</td>
            <td>**Features**</td>
            <td>**Priority**</td>
            <td>**Difficulty**</td>
            <td>T1</td>
            <td>T2</td>
            <td>T3</td>
            <td>T4</td>
            <td>T1</td>
            <td>T2</td>
            <td>T3</td>
            <td>T4</td>
        </tr>
        <!--  -->
        <tr>
            <td rowspan=1 style="font-weight:bold;">
                Project management
            </td>
            <td>
            Events, coordination
            </td>
            <td class=text-center>🔴</td>
            <td>▪️▪️</td>
            <td></td>
            <td colspan=5 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">20 d.</td>
            <td></td>
            <td></td>
        </tr>
        <!--  -->
        <tr style="background:whitesmoke;">
            <td rowspan=2 style="font-weight:bold;">
                Translations
            </td>
            <td>
            Translate to Spanish
            </td>
            <td class=text-center>🔴</td>
            <td>▪️</td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">2 d.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>
            Translate to Italian
            </td>
            <td class=text-center>🟡</td>
            <td>▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=1 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">2 d.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        <!--  -->
        <tr>
            <td rowspan=3 style="font-weight:bold;">
                Community management
            </td>
            <td>
            Community management
            </td>
            <td class=text-center>🔴</td>
            <td>▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=5 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">25 d.</td>
        </tr>
        <tr>
            <td>
            Roadmap management
            </td>
            <td class=text-center>🟠</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td colspan=6 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">10 d.</td>
        </tr>
        <tr>
            <td>
            Set up showcases
            </td>
            <td class=text-center>🟡</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td colspan=4 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">12 d.</td>
            <td></td>
            <td></td>
        </tr>
        <!--  -->
        <tr style="background:whitesmoke;">
            <td rowspan=2 style="font-weight:bold;">
                Business model
            </td>
            <td>
            Update website
            </td>
            <td class=text-center>🟠</td>
            <td>▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=3 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">12 d.</td>
            <td></td>
            <td></td>
        </tr>
        <tr style="background:whitesmoke;">
            <td>
            Business model 
            </td>
            <td class=text-center>🔴</td>
            <td>▪️▪️▪️</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan=3 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">12 d.</td>
            <td></td>
            <td></td>
        </tr>
        <!--  -->
        <tr>
            <td rowspan=1 style="font-weight:bold;">
                Design
            </td>
            <td>
            UX workshops
            </td>
            <td class=text-center>🟠</td>
            <td>▪️▪️</td>
            <td></td>
            <td></td>
            <td colspan=5 style="background:lightyellow; border-left:1px solid grey;border-right:1px solid grey;">15 d.</td>
            <td></td>
        </tr>
    </tbody>
</table>



# NGI_SEARCH

---

**NGI_SEARCH - Tech**


---


**NGI_SEARCH - Bizdev**

