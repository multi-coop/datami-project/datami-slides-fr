---
title: <strong>Datami - budgets</strong>
subtitle: '
  <strong>BUDGETS AND TIMELINES TABLES</strong><br><br>
  NGI PROGRAMS
'
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _fullstack developer_
  </span><br>
  <span class="text-micro text-author-details">
    _co-founder of [tech cooperative multi](https://multi.coop)_
  </span><br>
  <span class="img-cover">
    ![](images/logos/logo-MULTI-colored-063442-02-w120.png)
  </span>
  <span class="img-cover">
    ![](images/logos/logo-DATAMI-rect-colors.png)
  </span>
'
date: <span class="emph-line">March 21th 2023</span><br>Plan by pressing <span class="text-nano">`esc`</span><br>Versions &colon; [fr](https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-fr.html) / [en](https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-en.html)
title-slide-attributes:
  data-background-image: "images/sponsors/ngi_search-logo.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 5%"
  data-background-position: "right 10% top 37%, right 4% top 40%"
---








# GLOBAL <br> Provisional budget

**Budgets for full roamap**

---


## GLOBAL - Global budget

::: {.text-center} 
**Regrouped by milestones and category**
:::


|**Family**|**Category**|**Milestones**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|------------|------|------|---:|---:|
|TECH|Implementing|<b>More data sources</b>|🔴 high|▪️▪️ medium|<b> 51 days</b>|<b>40 800 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|🔴 high|▪️▪️▪️ hard|<b> 30 days</b>|<b>24 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|🔴 high|▪️▪️▪️ hard|<b> 29 days</b>|<b>23 200 €</b>|
|TECH|Implementing|<b>Protect widget with password</b>|🟠 medium|▪️ easy|<b> 12 days</b>|<b>9 600 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|🟡 low|▪️▪️ medium|<b> 42 days</b>|<b>33 600 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|🟡 low|▪️▪️ medium|<b> 44 days</b>|<b>35 200 €</b>|
|TECH|Implementing|<b>Better UX - maps</b>|🟡 low|▪️▪️ medium|<b> 40 days</b>|<b>32 000 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|🟠 medium|▪️ easy|<b> 16 days</b>|<b>12 800 €</b>|
|TECH|Implementing|<b>Better UI - new views</b>|🟡 low|▪️▪️ medium|<b> 27 days</b>|<b>21 600 €</b>|
|TECH|Implementing|<b>Refactoring</b>|🟠 medium|▪️▪️▪️ hard|<b> 48 days</b>|<b>38 400 €</b>|
|TECH|Implementing|<b>Tests</b>|🟡 low|▪️▪️▪️ hard|<b> 18 days</b>|<b>14 400 €</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|🔴 high|▪️ easy|<b> 20 days</b>|<b>16 000 €</b>|
|BIZDEV|OnBoarding|<b>Translations</b>|🟡 low|▪️ easy|<b> 4 days</b>|<b>3 200 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|🔴 high|▪️▪️▪️ hard|<b> 47 days</b>|<b>37 600 €</b>|
|BIZDEV|Adopting|<b>Business model</b>|🔴 high|▪️▪️▪️ hard|<b> 24 days</b>|<b>19 200 €</b>|
|BIZDEV|Adopting|<b>Design</b>|🟠 medium|▪️▪️ medium|<b> 15 days</b>|<b>12 000 €</b>|
|||<b>TOTAL</b>|||**467 days**|**373 600 €**|


---

## GLOBAL - Detailed budget & roadmap · 1/2


|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>15 days</b>|<b>12 000 €</b>|
|TECH|Backlog|<b>More data sources</b>|Connect to external APIs - OSM|🟡|▪️|<b>8 days</b>|<b>6 400 €</b>|
|TECH|Backlog|<b>More data sources</b>|Activity pub integration|🟡|▪️▪️▪️|<b>18 days</b>|<b>14 400 €</b>|
|TECH|Backlog|<b>More data sources</b>|Connector to SourceForge|🟡|▪️▪️▪️|<b>5 days</b>|<b>4 000 €</b>|
|TECH|Backlog|<b>More data sources</b>|Connector to BitBucket|🟡|▪️▪️▪️|<b>5 days</b>|<b>4 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>20 days</b>|<b>16 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>10 days</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>17 days</b>|<b>13 600 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>12 days</b>|<b>9 600 €</b>|
|TECH|Implementing|<b>Protect widget with password</b>|Protect access before showing widget|🟠|▪️|<b>6 days</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Protect widget with password</b>|Special token for protected widget|🟠|▪️▪️|<b>6 days</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>10 days</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>10 days</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>4 days</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Add / drag-drop a picture in a cell|🟡|▪️▪️|<b>6 days</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>12 days</b>|<b>9 600 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Full screen - debug|🟠|▪️▪️|<b>6 days</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Range filter|🟡|▪️|<b>4 days</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Change width column|🟠|▪️▪️|<b>8 days</b>|<b>6 400 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Helper at loader|🟡|▪️|<b>2 days</b>|<b>1 600 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>12 days</b>|<b>9 600 €</b>|
|TECH|Backlog|<b>Better UX - data interaction</b>|Export as pdf|🟡|▪️▪️▪️|<b>12 days</b>|<b>9 600 €</b>|




---

## GLOBAL - Detailed budget & roadmap · 2/2

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>Better UX - maps</b>|Inject data to vector tiles on map view|🟡|▪️▪️|<b>8 days</b>|<b>6 400 €</b>|
|TECH|Implementing|<b>Better UX - maps</b>|Add or edit geojson objects|🟡|▪️▪️▪️|<b>32 days</b>|<b>25 600 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|Custom styles / CSS / logos|🟠|▪️|<b>4 days</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>12 days</b>|<b>9 600 €</b>|
|TECH|Implementing|<b>Better UI - new views</b>|Agenda view|🟡|▪️|<b>8 days</b>|<b>6 400 €</b>|
|TECH|Adopting|<b>Better UI - new views</b>|Graphs view with D3js|🟡|▪️▪️|<b>10 days</b>|<b>8 000 €</b>|
|TECH|Adopting|<b>Better UI - new views</b>|Simultaneous dataviz + map on same view|🟡|▪️▪️|<b>9 days</b>|<b>7 200 €</b>|
|TECH|Implementing|<b>Refactoring</b>|Put all git* requests into a package|🟡|▪️▪️▪️|<b>15 days</b>|<b>12 000 €</b>|
|TECH|Implementing|<b>Refactoring</b>|Migration to Typescript|🟠|▪️▪️▪️|<b>15 days</b>|<b>12 000 €</b>|
|TECH|Backlog|<b>Refactoring</b>|Migration to Vue3|🟡|▪️▪️▪️|<b>18 days</b>|<b>14 400 €</b>|
|TECH|Implementing|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>18 days</b>|<b>14 400 €</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>20 days</b>|<b>16 000 €</b>|
|BIZDEV|OnBoarding|<b>Translations</b>|Translate to Spanish|🔴|▪️|<b>2 days</b>|<b>1 600 €</b>|
|BIZDEV|Backlog|<b>Translations</b>|Translate to Italian|🟡|▪️|<b>2 days</b>|<b>1 600 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Community management|🔴|▪️|<b>25 days</b>|<b>20 000 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>10 days</b>|<b>8 000 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Set up showcases|🟡|▪️▪️|<b>12 days</b>|<b>9 600 €</b>|
|BIZDEV|Adopting|<b>Business model</b>|Update website|🟠|▪️|<b>12 days</b>|<b>9 600 €</b>|
|BIZDEV|Adopting|<b>Business model</b>|Business model |🔴|▪️▪️▪️|<b>12 days</b>|<b>9 600 €</b>|
|BIZDEV|Adopting|<b>Design</b>|UX workshops|🟠|▪️▪️|<b>15 days</b>|<b>12 000 €</b>|



--- 

### GLOBAL - Budget analysis

::: {.text-center} 
**priority · _vs_ · difficulty**
:::


:::::: {.columns}
::: {.column width=50%}
::: {.text-center}
<b>
In days
</b>
:::


|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high| 49 days| 45 days| 39 days|<b> 133 days</b>|
|🟠 medium| 27 days| 67 days| 22 days|<b> 116 days</b>|
|🟡 low| 135 days| 59 days| 24 days|<b> 218 days</b>|
|**TOTAL**|<b> 211 days</b>|<b> 171 days</b>|<b> 85 days</b>|**467 days**|

:::

::: {.column width=50%}
::: {.text-center}
<b>
In euros
</b>
:::


|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|39 200 €|36 000 €|31 200 €|<b>106 400 €</b>|
|🟠 medium|21 600 €|53 600 €|17 600 €|<b>92 800 €</b>|
|🟡 low|108 000 €|47 200 €|19 200 €|<b>174 400 €</b>|
|**TOTAL**|<b>168 800 €</b>|<b>136 800 €</b>|<b>68 000 €</b>|**373 600 €**|



:::
::::::

---

## GLOBAL - Timeline · Technical tasks

::: {.ngi-timeline}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|T3|T4|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|TECH|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>15 days</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||||
|TECH|<b>More data sources</b>|Connect to external APIs - OSM|🟡|▪️|<b>8 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>More data sources</b>|Activity pub integration|🟡|▪️▪️▪️|<b>18 days</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>More data sources</b>|Connector to SourceForge|🟡|▪️▪️▪️|<b>5 days</b>|||||||<span class="cell-bg">x</span>||
|TECH|<b>More data sources</b>|Connector to BitBucket|🟡|▪️▪️▪️|<b>5 days</b>|||||||<span class="cell-bg">x</span>||
|TECH|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>20 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>10 days</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>17 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>12 days</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Protect widget with password</b>|Protect access before showing widget|🟠|▪️|<b>6 days</b>|||<span class="cell-bg">x</span>||||||
|TECH|<b>Protect widget with password</b>|Special token for protected widget|🟠|▪️▪️|<b>6 days</b>||||<span class="cell-bg">x</span>|||||
|TECH|<b>Better UX - data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>10 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||||
|TECH|<b>Better UX - data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>10 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>4 days</b>|||||<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data management</b>|Add / drag-drop a picture in a cell|🟡|▪️▪️|<b>6 days</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>12 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data interaction</b>|Full screen - debug|🟠|▪️▪️|<b>6 days</b>|||||<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data interaction</b>|Range filter|🟡|▪️|<b>4 days</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data interaction</b>|Change width column|🟠|▪️▪️|<b>8 days</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||||
|TECH|<b>Better UX - data interaction</b>|Helper at loader|🟡|▪️|<b>2 days</b>||<span class="cell-bg">x</span>|||||||
|TECH|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>12 days</b>||||<span class="cell-bg">x</span>|||||
|TECH|<b>Better UX - data interaction</b>|Export as pdf|🟡|▪️▪️▪️|<b>12 days</b>|||||||<span class="cell-bg">x</span>||
|TECH|<b>Better UX - maps</b>|Inject data to vector tiles on map view|🟡|▪️▪️|<b>8 days</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - maps</b>|Add or edit geojson objects|🟡|▪️▪️▪️|<b>32 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UI - customization</b>|Custom styles / CSS / logos|🟠|▪️|<b>4 days</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||||
|TECH|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>12 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UI - new views</b>|Agenda view|🟡|▪️|<b>8 days</b>||||<span class="cell-bg">x</span>|||||
|TECH|<b>Better UI - new views</b>|Graphs view with D3js|🟡|▪️▪️|<b>10 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Better UI - new views</b>|Simultaneous dataviz + map on same view|🟡|▪️▪️|<b>9 days</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Refactoring</b>|Put all git* requests into a package|🟡|▪️▪️▪️|<b>15 days</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Refactoring</b>|Migration to Typescript|🟠|▪️▪️▪️|<b>15 days</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Refactoring</b>|Migration to Vue3|🟡|▪️▪️▪️|<b>18 days</b>|||||<span class="cell-bg">x</span>||||
|TECH|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>18 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||


:::


---

### GLOBAL - Timeline · Bizdev tasks

::: {.ngi-timeline}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|T3|T4|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|BIZDEV|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>20 days</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|BIZDEV|<b>Translations</b>|Translate to Spanish|🔴|▪️|<b>2 days</b>||<span class="cell-bg">x</span>|||||||
|BIZDEV|<b>Translations</b>|Translate to Italian|🟡|▪️|<b>2 days</b>||||<span class="cell-bg">x</span>|||||
|BIZDEV|<b>Community management</b>|Community management|🔴|▪️|<b>25 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|BIZDEV|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>10 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|BIZDEV|<b>Community management</b>|Set up showcases|🟡|▪️▪️|<b>12 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|BIZDEV|<b>Business model</b>|Update website|🟠|▪️|<b>12 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|BIZDEV|<b>Business model</b>|Business model |🔴|▪️▪️▪️|<b>12 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|BIZDEV|<b>Design</b>|UX workshops|🟠|▪️▪️|<b>15 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||

:::








# NGI_SEARCH <br> Provisional budget

**Up to 150 k€**

[Program - apply](https://enrichers.ngi.eu/apply-here/)

---


## NGI_SEARCH - Global budget

::: {.text-center} 
**Regrouped by milestones and category**
:::


|**Family**|**Category**|**Milestones**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|------------|------|------|---:|---:|
|TECH|Implementing|<b>More data sources</b>|🔴 high|▪️▪️ medium|<b> 15 days</b>|<b>12 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|🔴 high|▪️▪️▪️ hard|<b> 15 days</b>|<b>12 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|🔴 high|▪️▪️▪️ hard|<b> 16 days</b>|<b>12 800 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|🟡 low|▪️▪️ medium|<b> 37 days</b>|<b>29 600 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|🟡 low|▪️▪️ medium|<b> 26 days</b>|<b>20 800 €</b>|
|TECH|Implementing|<b>Better UX - maps</b>|🟡 low|▪️▪️ medium|<b> 15 days</b>|<b>12 000 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|🟠 medium|▪️ easy|<b> 4 days</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UI - new views</b>|🟡 low|▪️▪️ medium|<b> 19 days</b>|<b>15 200 €</b>|
|TECH|Implementing|<b>Refactoring</b>|🟠 medium|▪️▪️▪️ hard|<b> 18 days</b>|<b>14 400 €</b>|
|TECH|Implementing|<b>Tests</b>|🟡 low|▪️▪️▪️ hard|<b> 7 days</b>|<b>5 600 €</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|🔴 high|▪️ easy|<b> 3 days</b>|<b>2 400 €</b>|
|BIZDEV|OnBoarding|<b>Translations</b>|🟡 low|▪️ easy|<b> 2 days</b>|<b>1 600 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|🔴 high|▪️▪️▪️ hard|<b> 7 days</b>|<b>5 600 €</b>|
|||<b>TOTAL</b>|||**184 d.**|**147 200 €**|


---

## NGI_SEARCH - Detailed budget & roadmap · 1/2

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Backlog|<b>More data sources</b>|Connector to SourceForge|🟡|▪️▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Backlog|<b>More data sources</b>|Connector to BitBucket|🟡|▪️▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>10 d.</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>10 d.</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>10 d.</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>4 d.</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Add / drag-drop a picture in a cell|🟡|▪️▪️|<b>6 d.</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>12 d.</b>|<b>9 600 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Full screen - debug|🟠|▪️▪️|<b>6 d.</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Range filter|🟡|▪️|<b>4 d.</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Change width column|🟠|▪️▪️|<b>8 d.</b>|<b>6 400 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Helper at loader|🟡|▪️|<b>2 d.</b>|<b>1 600 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>|<b>4 800 €</b>|




---

## NGI_SEARCH - Detailed budget & roadmap · 2/2

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>Better UX - maps</b>|Add or edit geojson objects|🟡|▪️▪️▪️|<b>15 d.</b>|<b>12 000 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>4 d.</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Better UI - new views</b>|Agenda view|🟡|▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Adopting|<b>Better UI - new views</b>|Graphs view with D3js|🟡|▪️▪️|<b>10 d.</b>|<b>8 000 €</b>|
|TECH|Adopting|<b>Better UI - new views</b>|Simultaneous dataviz + map on same view|🟡|▪️▪️|<b>4 d.</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Refactoring</b>|Put all git* requests into a package|🟡|▪️▪️▪️|<b>8 d.</b>|<b>6 400 €</b>|
|TECH|Implementing|<b>Refactoring</b>|Migration to Typescript|🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Backlog|<b>Refactoring</b>|Migration to Vue3|🟡|▪️▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Implementing|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>|<b>5 600 €</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>3 d.</b>|<b>2 400 €</b>|
|BIZDEV|OnBoarding|<b>Translations</b>|Translate to Spanish|🔴|▪️|<b>2 d.</b>|<b>1 600 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>4 d.</b>|<b>3 200 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Set up showcases|🟡|▪️▪️|<b>3 d.</b>|<b>2 400 €</b>|

<br>

|&nbsp;|&nbsp;|&nbsp;|&nbsp;|&nbsp;|&nbsp;|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|------|------------|:---:|---|---:|---:|
||||||<b>TOTAL</b>|**184 d.**|**147 200 €**|


--- 

## NGI_SEARCH - Budget analysis

::: {.text-center}
<b>
**priority · _vs_ · difficulty**
</b>
:::


:::::: {.columns}
::: {.column width=50%}
::: {.text-center}
<b>
In days
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high| 20 days| 8 days| 13 days|<b> 41 days</b>|
|🟠 medium| 17 days| 27 days|  days|<b> 44 days</b>|
|🟡 low| 51 days| 37 days| 11 days|<b> 99 days</b>|
|**TOTAL**|<b> 88 days</b>|<b> 72 days</b>|<b> 24 days</b>|**184 days**|

:::

::: {.column width=50%}
::: {.text-center}
<b>
In euros
</b>
:::


|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|16 000 €|6 400 €|10 400 €|<b>32 800 €</b>|
|🟠 medium|13 600 €|21 600 €|  €|<b>35 200 €</b>|
|🟡 low|40 800 €|29 600 €|8 800 €|<b>79 200 €</b>|
|**TOTAL**|<b>70 400 €</b>|<b>57 600 €</b>|<b>19 200 €</b>|**147 200 €**|


:::
::::::


---

### NGI_SEARCH - Timeline · Technical tasks

::: {.ngi-timeline}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|T3|T4|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|TECH|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||||
|TECH|<b>More data sources</b>|Connector to SourceForge|🟡|▪️▪️▪️|<b>5 d.</b>|||||||<span class="cell-bg">x</span>||
|TECH|<b>More data sources</b>|Connector to BitBucket|🟡|▪️▪️▪️|<b>5 d.</b>|||||||<span class="cell-bg">x</span>||
|TECH|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>10 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>5 d.</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>10 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||||
|TECH|<b>Better UX - data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>10 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>4 d.</b>|||||<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data management</b>|Add / drag-drop a picture in a cell|🟡|▪️▪️|<b>6 d.</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>12 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data interaction</b>|Full screen - debug|🟠|▪️▪️|<b>6 d.</b>|||||<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data interaction</b>|Range filter|🟡|▪️|<b>4 d.</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data interaction</b>|Change width column|🟠|▪️▪️|<b>8 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||||
|TECH|<b>Better UX - data interaction</b>|Helper at loader|🟡|▪️|<b>2 d.</b>||<span class="cell-bg">x</span>|||||||
|TECH|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>||||<span class="cell-bg">x</span>|||||
|TECH|<b>Better UX - maps</b>|Add or edit geojson objects|🟡|▪️▪️▪️|<b>15 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UI - new views</b>|Agenda view|🟡|▪️|<b>5 d.</b>||||<span class="cell-bg">x</span>|||||
|TECH|<b>Better UI - new views</b>|Graphs view with D3js|🟡|▪️▪️|<b>10 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Better UI - new views</b>|Simultaneous dataviz + map on same view|🟡|▪️▪️|<b>4 d.</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Refactoring</b>|Put all git* requests into a package|🟡|▪️▪️▪️|<b>8 d.</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Refactoring</b>|Migration to Typescript|🟠|▪️▪️▪️|<b>5 d.</b>||||||<span class="cell-bg">x</span>|||
|TECH|<b>Refactoring</b>|Migration to Vue3|🟡|▪️▪️▪️|<b>5 d.</b>|||||<span class="cell-bg">x</span>||||
|TECH|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||



:::

---

### NGI_SEARCH - Timeline · Bizdev tasks

::: {.ngi-timeline}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|T3|T4|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|BIZDEV|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|BIZDEV|<b>Translations</b>|Translate to Spanish|🔴|▪️|<b>2 d.</b>||<span class="cell-bg">x</span>|||||||
|BIZDEV|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|BIZDEV|<b>Community management</b>|Set up showcases|🟡|▪️▪️|<b>3 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||

:::






# NGI_ZERO_CORE <br> Provisional budget

**Up to 50 k€**

[Program - apply](https://enrichers.ngi.eu/apply-here/)

---


## NGI_ZERO_CORE - Global budget

::: {.text-center} 
**Regrouped by milestones and category**
:::

...

---

## NGI_ZERO_CORE - Detailed budget & roadmap · 1/2

...

---

## NGI_ZERO_CORE - Detailed budget & roadmap · 2/2

...


--- 

## NGI_ZERO_CORE - Budget analysis

::: {.text-center}
<b>
**priority · _vs_ · difficulty**
</b>
:::


:::::: {.columns}
::: {.column width=50%}
::: {.text-center}
<b>
In days
</b>
:::


:::

::: {.column width=50%}
::: {.text-center}
<b>
In euros
</b>
:::


:::
::::::











# NGI_ZERO_ENTRUST <br> Provisional budget

**Up to 50 k€**

[Program - apply](https://nlnet.nl/propose/)

---


## NGI_ZERO_ENTRUST - Global budget

::: {.text-center} 
**Regrouped by milestones and category**

|**Family**|**Category**|**Milestones**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|------------|------|------|---:|---:|
|TECH|Implementing|<b>More data sources</b>|🔴 high|▪️▪️ medium|<b> 10 days</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|🔴 high|▪️▪️▪️ hard|<b> 15 days</b>|<b>12 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|🔴 high|▪️▪️▪️ hard|<b> 13 days</b>|<b>10 400 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|🟡 low|▪️▪️ medium|<b> 6 days</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|🟠 medium|▪️ easy|<b> 4 days</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Tests</b>|🟡 low|▪️▪️▪️ hard|<b> 3 days</b>|<b>2 400 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|🔴 high|▪️▪️▪️ hard|<b> 3 days</b>|<b>2 400 €</b>|
|BIZDEV|Adopting|<b>Design</b>|🟠 medium|▪️▪️ medium|<b> 5 days</b>|<b>4 000 €</b>|
|||<b>TOTAL</b>|||**59 d.**|**47 200 €**|

:::



---

## NGI_ZERO_ENTRUST - Detailed budget & roadmap · 1/2

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|:---:|---------|------------|:---:|---|---:|---:|
|TECH|Implementing|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>10 d.</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>10 d.</b>|<b>8 000 €</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>7 d.</b>|<b>5 600 €</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>|<b>4 800 €</b>|
|TECH|Implementing|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>4 d.</b>|<b>3 200 €</b>|
|TECH|Implementing|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>3 d.</b>|<b>2 400 €</b>|
|BIZDEV|Adopting|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>3 d.</b>|<b>2 400 €</b>|
|BIZDEV|Adopting|<b>Design</b>|UX workshops|🟠|▪️▪️|<b>5 d.</b>|<b>4 000 €</b>|
||||||<b>TOTAL</b>|**59 d.**|**47 200 €**|

---

## NGI_ZERO_ENTRUST - Detailed budget & roadmap · 2/2

...

--- 

## NGI_ZERO_ENTRUST - Budget analysis

::: {.text-center}
<b>
**priority · _vs_ · difficulty**
</b>
:::


:::::: {.columns}
::: {.column width=50%}
::: {.text-center}
<b>
In days
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high| 17 days| 15 days| 6 days|<b> 38 days</b>|
|🟠 medium|  days| 12 days|  days|<b> 12 days</b>|
|🟡 low| 9 days|  days|  days|<b> 9 days</b>|
|**TOTAL**|<b> 26 days</b>|<b> 27 days</b>|<b> 6 days</b>|**59 days**|


:::

::: {.column width=50%}
::: {.text-center}
<b>
In euros
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|13 600 €|12 000 €|4 800 €|<b>30 400 €</b>|
|🟠 medium|  €|9 600 €|  €|<b>9 600 €</b>|
|🟡 low|7 200 €|  €|  €|<b>7 200 €</b>|
|**TOTAL**|<b>20 800 €</b>|<b>21 600 €</b>|<b>4 800 €</b>|**47 200 €**|


:::
::::::



---

### NGI_ZERO_ENTRUST - Timeline · Technical tasks

::: {.ngi-timeline}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|T3|T4|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|TECH|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>10 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||||
|TECH|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>10 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>5 d.</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>7 d.</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>6 d.</b>||||<span class="cell-bg">x</span>|||||
|TECH|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>3 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|BIZDEV|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>3 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|BIZDEV|<b>Design</b>|UX workshops|🟠|▪️▪️|<b>5 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||

:::
















# NGI_ENRICHERS <br> Provisional budget

**Up to 25 800 €**

[Program - apply](https://enrichers.ngi.eu/apply-here/)

---


## NGI_ENRICHERS - Global budget

::: {.text-center} 
**Regrouped by milestones and category**
:::



|**Family**|**Category**|**Milestones**|**Priority**|**Difficulty**|
|:---:|:---:|------------|------|------|
|TECH|Implementing|<b>More data sources</b>|🔴 high|▪️▪️ medium|
|TECH|Implementing|<b>Online widget configuration</b>|🔴 high|▪️▪️▪️ hard|
|TECH|Implementing|<b>Manage contribution widget</b>|🔴 high|▪️▪️▪️ hard|
|TECH|Implementing|<b>Protect widget with password</b>|🟠 medium|▪️ easy|
|TECH|Implementing|<b>Better UX - data management</b>|🟡 low|▪️▪️ medium|
|TECH|Implementing|<b>Better UX - data interaction</b>|🟡 low|▪️▪️ medium|
|TECH|Implementing|<b>Better UX - maps</b>|🟡 low|▪️▪️ medium|
|TECH|Implementing|<b>Better UI - customization</b>|🟠 medium|▪️ easy|
|TECH|Implementing|<b>Better UI - new views</b>|🟡 low|▪️▪️ medium|
|TECH|Implementing|<b>Refactoring</b>|🟠 medium|▪️▪️▪️ hard|
|TECH|Implementing|<b>Tests</b>|🟡 low|▪️▪️▪️ hard|
|BIZDEV|OnBoarding|<b>Project management</b>|🔴 high|▪️ easy|
|BIZDEV|OnBoarding|<b>Translations</b>|🟡 low|▪️ easy|
|BIZDEV|Adopting|<b>Community management</b>|🔴 high|▪️▪️▪️ hard|
|BIZDEV|Adopting|<b>Business model</b>|🔴 high|▪️▪️▪️ hard|
|BIZDEV|Adopting|<b>Design</b>|🟠 medium|▪️▪️ medium|



---

## NGI_ENRICHERS - Detailed budget & roadmap · 1/2

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|
|:---:|:---:|---------|------------|:---:|---|---:|
|TECH|Implementing|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>15 days</b>|
|TECH|Backlog|<b>More data sources</b>|Connect to external APIs - OSM|🟡|▪️|<b>8 days</b>|
|TECH|Backlog|<b>More data sources</b>|Activity pub integration|🟡|▪️▪️▪️|<b>18 days</b>|
|TECH|Backlog|<b>More data sources</b>|Connector to SourceForge|🟡|▪️▪️▪️|<b>5 days</b>|
|TECH|Backlog|<b>More data sources</b>|Connector to BitBucket|🟡|▪️▪️▪️|<b>5 days</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>20 days</b>|
|TECH|Implementing|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>10 days</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>17 days</b>|
|TECH|Implementing|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>12 days</b>|
|TECH|Implementing|<b>Protect widget with password</b>|Protect access before showing widget|🟠|▪️|<b>6 days</b>|
|TECH|Implementing|<b>Protect widget with password</b>|Special token for protected widget|🟠|▪️▪️|<b>6 days</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>10 days</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>10 days</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>4 days</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Add / drag-drop a picture in a cell|🟡|▪️▪️|<b>6 days</b>|
|TECH|Implementing|<b>Better UX - data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>12 days</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Full screen - debug|🟠|▪️▪️|<b>6 days</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Range filter|🟡|▪️|<b>4 days</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Change width column|🟠|▪️▪️|<b>8 days</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Helper at loader|🟡|▪️|<b>2 days</b>|
|TECH|Implementing|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>12 days</b>|
|TECH|Backlog|<b>Better UX - data interaction</b>|Export as pdf|🟡|▪️▪️▪️|<b>12 days</b>|



---

## NGI_ENRICHERS - Detailed budget & roadmap · 2/2

|**Family**|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|
|:---:|:---:|---------|------------|:---:|---|---:|
|TECH|Implementing|<b>Better UX - maps</b>|Inject data to vector tiles on map view|🟡|▪️▪️|<b>8 days</b>|
|TECH|Implementing|<b>Better UX - maps</b>|Add or edit geojson objects|🟡|▪️▪️▪️|<b>32 days</b>|
|TECH|Implementing|<b>Better UI - customization</b>|Custom styles / CSS / logos|🟠|▪️|<b>4 days</b>|
|TECH|Implementing|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>12 days</b>|
|TECH|Implementing|<b>Better UI - new views</b>|Agenda view|🟡|▪️|<b>8 days</b>|
|TECH|Adopting|<b>Better UI - new views</b>|Graphs view with D3js|🟡|▪️▪️|<b>10 days</b>|
|TECH|Adopting|<b>Better UI - new views</b>|Simultaneous dataviz + map on same view|🟡|▪️▪️|<b>9 days</b>|
|TECH|Implementing|<b>Refactoring</b>|Put all git* requests into a package|🟡|▪️▪️▪️|<b>15 days</b>|
|TECH|Implementing|<b>Refactoring</b>|Migration to Typescript|🟠|▪️▪️▪️|<b>15 days</b>|
|TECH|Backlog|<b>Refactoring</b>|Migration to Vue3|🟡|▪️▪️▪️|<b>18 days</b>|
|TECH|Implementing|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>18 days</b>|
|BIZDEV|OnBoarding|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>20 days</b>|
|BIZDEV|OnBoarding|<b>Translations</b>|Translate to Spanish|🔴|▪️|<b>2 days</b>|
|BIZDEV|Backlog|<b>Translations</b>|Translate to Italian|🟡|▪️|<b>2 days</b>|
|BIZDEV|Adopting|<b>Community management</b>|Community management|🔴|▪️|<b>25 days</b>|
|BIZDEV|Adopting|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>10 days</b>|
|BIZDEV|Adopting|<b>Community management</b>|Set up showcases|🟡|▪️▪️|<b>12 days</b>|
|BIZDEV|Adopting|<b>Business model</b>|Update website|🟠|▪️|<b>12 days</b>|
|BIZDEV|Adopting|<b>Business model</b>|Business model |🔴|▪️▪️▪️|<b>12 days</b>|
|BIZDEV|Adopting|<b>Design</b>|UX workshops|🟠|▪️▪️|<b>15 days</b>|
||||||<b>TOTAL</b>|**467 days**|


--- 

## NGI_ENRICHERS - Budget analysis


::: {.text-center} 
**priority · _vs_ · difficulty**
:::


:::::: {.columns}
::: {.column width=50%}

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high| 49 days| 45 days| 39 days|<b> 133 days</b>|
|🟠 medium| 27 days| 67 days| 22 days|<b> 116 days</b>|
|🟡 low| 135 days| 59 days| 24 days|<b> 218 days</b>|
|**TOTAL**|<b> 211 days</b>|<b> 171 days</b>|<b> 85 days</b>|**467 days**|
:::

::::::
