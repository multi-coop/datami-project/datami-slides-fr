




# Provisional budget

**What will the grant be used for ?**

---

### Global budget 

::: {.text-micro .text-justify}

:::::: {.columns}

::: {.column width=50%}

The global roadmap of Datami encompasses many insights and demands we had throughout our exchanges with our users and clients.
<b>
For each demand we derived the corresponding features in need for developments
and packed them into major categories and milestones.
</b>

:::

::: {.column width=50%}

This work of discretizing the needs into features allows us
to draw a quite precise idea of the resources to mobilize
while keeping a lean approach in terms of project management.

:::

::::::

:::


### Legend

::: { .text-micro}
- <b>Family</b> : Family of the task between Tech (technical) / Bizdev (business development) ;
- <b>Category</b> : Category of the task between Onboarding / Implementing / Adopting / Backlog ;
- <b>Milestone</b> : Sub-family of tasks aiming to develop a specific feature ;
- <b>Dev + man.</b> : Development and project management time estimate ;
- Symbols used in the following tables represent either a certain level of priority or difficulty.
:::

<hr>

:::::: {.columns}

::: {.column width=35%}
::: { .text-micro}
- <b>Priority</b> is an estimation of the criticity / need of a particular feature as expressed by users or clients.
:::
:::

::: {.column .text-micro width=15%}

| Symbol    | Meaning |
| :-------: | ------  |
| 🔴        | High    |
| 🟠        | Medium  |
| 🟡        | Low     |

:::

::: {.column width=35%}
::: { .text-micro}
- <b>Difficulty</b> is an estimation of the complexity to realize the feature by the team, 
a higher level of complexity also being synonymous of a higher risk of unexpected problems to resolve along the way.   
:::
:::

::: {.column .text-micro width=15%}

| Symbol | Meaning |
| :----  | ------  |
| ▪️▪️▪️    | Hard    |
| ▪️▪️     | Medium  |
| ▪️      | Easy    |

:::


:::::::

---


