
# Benchmark

**How does Datami differs from other solutions ?**

---

::: {.text-center}
There are several data visualization and editing solutions <br>sharing [similarities with Datami](https://datami.multi.coop/benchmark?locale=en)

Here are some of the most popular solutions

<br>

::: {.text-micro}
**Those benchmarks are given for information only**

<br>

Do not hesitate to let us know by writing to
<br>
[contact@multi.coop](mailto:contact@multi.coop)
<br>
if you wish to make any corrections or additions
:::

:::

---

### Benchmark · Datavisualisation tools


| Solution               | Solution type    | Langages                            | Difficulty            | Saas    | Official website                                                 |
| ------------------     | ---------------- | ----------------------------------- | ---                   | ---     | ---                                                              |
| **Gogocarto**          | Open source      | Custom                              | Easy                  | Yes    | [https://gogocarto.fr/projects](https://gogocarto.fr/projects)   |
| **Umap**               | Open source      | Custom                              | Facile                | Yes    | [https://umap.openstreetmap.fr/](https://umap.openstreetmap.fr/) |
| **Lizmap**             | Open source      | Propre langage de requête           | Medium                | Yes    | [https://www.lizmap.com](https://www.lizmap.com)                 |
| **Apache Superset**    | Open source      | SQL                                 | Medium                | Yes    | [https://superset.apache.org/](https://superset.apache.org/)     |
| **Apache Zeppelin**    | Open source      | Several programmation languages     | Hard                  | No     | [https://zeppelin.apache.org/](https://zeppelin.apache.org/)     |
| **BIRT**               | Open source      | Java, JavaScript                    | Hard                  | No     | [https://www.eclipse.org/birt/](https://www.eclipse.org/birt/)   |
| **FineReport**         | Open source      | Java, JavaScript                    | Medium                | No     | [https://www.finereport.com/en](https://www.finereport.com/en)   |
| **Grafana**            | Open source      | Custom request language             | Hard                  | No     | [https://grafana.com/](https://grafana.com/)                     |
| **Metabase**           | Open source      | SQL                                 | Easy                  | Yes    | [https://www.metabase.com/](https://www.metabase.com/)           |
| **Redash**             | Open source      | SQL                                 | Medium                | No     | [https://redash.io/](https://redash.io/)                         |
| **Datasette**          | Open source      | SQL                                 | Medium                | No     | [https://datasette.io/](https://datasette.io/)                   |
| **LightDash**          | Open source      | Dbt                                 | Medium                | Yes    | [https://www.lightdash.com/](https://www.lightdash.com/)         |
| **Google Data Studio** | Free (but not open) | SQL                              | Easy                  | Yes    | [https://datastudio.google.com/](https://datastudio.google.com/) |
| **Datawrapper**        | Commercial       | API, CSV, GSheet                    | Easy                  | Yes    | [https://www.datawrapper.de/](https://www.datawrapper.de/)       |
| **Google Looker**      | Commercial       | LookML                              | Hard                  | Yes    | [https://looker.com/](https://looker.com/)                       |
| **Microsoft Power BI** | Commercial       | DAX et M                            | Medium                | Yes    | [https://powerbi.microsoft.com/](https://powerbi.microsoft.com/) |
| **QlikView**           | Commercial       | Custom script language              | Hard                  | Yes    | [https://www.qlik.com/](https://www.qlik.com/)                   |
| **Tableau**            | Commercial       | Custom                              | Medium                | Yes    | [https://www.tableau.com/](https://www.tableau.com/)             |

---

### Benchmark · Online editing tools

| Solution            | Solution type | Langages                 | Difficulty             | Saas | Targeted public                                                              | Official webiste                                                             |
| ------------------- | ------------  | ------------------------ | ---                    | ---  | -------------------------------------------------------------------------    | ---                                                                          |
| **Apache Zeppelin** | Open source | Scala, Python, R, SQL      | Hard                   | No   | Developers and professional users                                            | [https://zeppelin.apache.org/](https://zeppelin.apache.org/)                 |
| **Baserow**         | Open source | Python, Javascript, Vue.js | Moyen                  | Yes  | Developers and professional users                                            | [https://baserow.io/](https://baserow.io/)                                   |
| **Grist**           | Open source | Python                     | Easy                   | Yes  | Businesses, non-profit organizations, governments, universities, researchers | [https://getgrist.com/](https://getgrist.com/)                               |
| **Metabase**        | Open source | Java, Clojure              | Medium                 | Yes  | Startups, businessees, non-profit organizations                              | [https://www.metabase.com/](https://www.metabase.com/)                       |
| **LockoKit**        | Open source | ...                        | Medium                 | No   | Developers and professional user                                             | [https://locokit.io/](https://locokit.io/)                                   |
| **NoCodB**          | Open source | Javascript, Node.js        | Easy                   | Yes  | Developers and professional users                                            | [https://nocodb.com/](https://nocodb.com/)                                   |
| **Gsheet**          | -           | None                       | Easy                   | Yes  | Companies, teams, freelancers, SMEs                                          | [https://www.google.com/sheets/about/](https://www.google.com/sheets/about/) |
| **Airtable**        | Commercial  | None                       | Easy                   | Yes  | Companies, teams, freelancers, SMEs                                          | [https://airtable.com/](https://airtable.com/)                               |
| **Qlikview**        | Commercial  | None                       | Medium                 | Yes  | Large corporations, financial institutions                                   | [https://www.qlik.com/us/](https://www.qlik.com/us/)                         |

---

### Benchmark · Features comparison

::: {.text-micro}
Among all the solutions we have just listed, some can be compared with **Datami**'s main features.
:::

| Solution            | Open source  | Readyness | Table view | Cards view  | Map view   | Dataviz view | Edition  | Moderation  | Configuration interface     | Data sources        | Backend              | Widget  | Official website                               |
| -----------------   | :---:        | ---       | :---:      | :---:       | :---:      | :---:        | :---:    | :---:       | :---:                       | ------------------  | -------------------- | :---:   | ---                                            |
| **Datami**          | ✅           | ⭐⭐      | ✅         | ✅          | ✅         | ✅           | ✅       | ✅          | ❌ (for now)                | API ext. (Git)      | Git platforms / APIs | ✅      | [Website](https://datami.multi.coop/?locale=en/)          |
| **Metabase**        | ✅           | ⭐⭐      | ✅         | ❓          | ✅         | ✅           | ⚠️        | ❌          | ✅                          | SQL, connectors     | server / APIs        | ✅      | [Website](https://www.metabase.com/)           |
| **Gogocarto**       | ✅           | ⭐⭐⭐    | ✅         | ✅          | ✅         | ❌           | ✅       | ❌          | ✅                          | proper              | server / APIs        | ✅      | [Website](https://gogocarto.fr/projects)       |
| **Lizmap**          | ✅           | ⭐        | ⚠️          | ❌          | ✅         | ✅           | ✅       | ❌          | ✅                          | proper              | server / ❓          | ✅      | [Website](https://www.lizmap.com)              |
| **Umap**            | ✅           | ⭐⭐⭐    | ❌         | ❌          | ✅         | ❌           | ✅       | ❌          | ✅                          | ...                 | server / ❓          | ✅      | [Website](https://umap.openstreetmap.fr/)      |
| **Grist**           | ✅           | ⭐        | ✅         | ✅          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / APIs        | ❌      | [Website](https://getgrist.com/)               |
| **Baserow**         | ✅           | ⭐⭐      | ✅         | ✅          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / ❓          | ❌      | [Website](https://baserow.io/)                 |
| **LockoKit**        | ✅           | ⭐⭐      | ✅         | ❌          | ❌         | ❌           | ✅       | ❓          | ✅                          | proper              | server / ❓          | ❌      | [Website](https://locokit.io/)                 |
| **NoCodB**          | ✅           | ⭐        | ✅         | ❌          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / ❓          | ❌      | [Website](https://nocodb.com/)                  |
| **Apache Superset** | ✅           | ⭐⭐      | ✅         | ❌          | ❌         | ✅           | ✅       | ❓          | ✅                          | SQL                 | server / Saas        | ❓      | [Website](https://superset.apache.org/)         |
| **Datawrapper**     | 🔒           | ⭐⭐⭐    | ✅         | ❓          | ✅         | ✅           | ❌       | ❌          | ✅                          | SQL, connectors     | Saas                 | ✅      | [Website](https://www.datawrapper.de/)          |
| **Airtable**        | 🔒           | ⭐⭐⭐    | ✅         | ✅          | ❌         | ⚠️            | ✅       | ⚠️           | ✅                          | proper, connectors  | Saas                 | ✅      | [Website](https://airtable.com/)                |
| **Gsheet**          | 🔒           | ⭐⭐⭐    | ✅         | ❌          | ❌         | ✅           | ✅       | ✅          | ✅                          | proper, connectors  | Saas                 | ✅      | [Website](https://www.google.com/sheets/about/) |


::: {.text-micro .text-center}
The ❓ indicates the information is to be completed<br>
The ⚠️ that the functionality can be implemented either in a roundabout way or as a hack
:::

---

Although these solutions share some features with [Datami](https://datami.multi.coop/?locale=en), they can differ significantly in terms of cost, complexity and specific features

It is important to take into account the **specific needs of each project** before choosing the most suitable online data visualization and editing solution

---

## Benchmark · Datami's main differentiating innovations

Datami project is built around the intention of making work together three specifications in terms of web development.

:::::: {.columns}

::: {.column width=33%}

**Open contributions &**<br>**moderation**

::: {.text-micro}
At the difference of the open source solutions
we identified Datami aims to provide 
a simple system allowing 
<b>
at the same time :
</b>

- a system of contribution respecting users' rights to privacy ;
- a system of moderation of contributions by data producers ;
- simple contribution and moderation interfaces managable by common citizen. 
:::
:::

::: {.column width=33%}
**End-user &**<br>**human-centric design**

::: {.text-micro}
Datami target audiences are citizen with a low technical background,
but eager to share their knowledge.

We want to make access to open data more inclusive 
and to give users an incentive to contribute.

Datami focuses on providing 
<b>
interfaces and data-visualisations
corresponding to common uses and habits.
</b>
:::
:::

::: {.column width=33%}
**Affordability &**<br>**technical simplicity**

::: {.text-micro}
Datami's architecture relies on the existence of robust FLOSS (Free/Libre and Open Source Software) solutions
for data storage and version control.

We designed Datami as a frontend solution only for open data production and valorisation.

Datami can be reused as a FLOSS solution
<b>
without needing to be installed on a server
</b>
and therefore be more easily mutualised. 
:::
:::

::::::

