


# The team

**Who are the people behind Datami ?**

---

## A project by the tech cooperative multi

<br>

:::::::::::::: {.columns}

::: {.column width="30%"}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)

::: {.text-center}
[https://multi.coop](https://multi.coop)
:::

:::

::: {.column width="70%"}
Our cooperative contributes to the development of **digital commons** and associated services, by bringing together a community of professionals working for a **digital of general interest**
::: 

::::::::::::::

---

## Datami's team members

**Dedicated, experienced and multi-disciplinary profesionals**

---

## Julien Paris

### Developer  · product owner

:::::::::::::: {.columns}

::: {.column .img-team .img-pdf width=25%}
![](images/team/julien-paris-01.png)

::: {.text-nano}
&nbsp; ✉️ [julien.paris@multi.coop](mailto:julien.paris@multi.coop)
:::

:::

::: {.column .text-micro width=75%}
After initial training as a DPLG architect, then having carried out cultural projects in international cultural cooperation (Spanish embassy in Mexico, French embassy in Egypt), doing research (CNRS doctoral student in Turkey), I finally switched to digital as a _fullstack_ developer.

I only develop open source softwares, thus hoping to support the **open data and free software movement.** 
I am particularly interested in data visualization and open data contribution processes.

Since 2015 I have **directed and developed digital projects** for french ministerial institutions (ANCT, Bercy, CGET, Agence Bio), inter-ministerial (DINUM), associations and think tanks involved in general interest (Mednum , PiNG, Ternum, Rhinocc, Décider Ensemble), and for other public structures such as a public mediatheque nearby Nantes.

I participated in Etalab's "General Interest Entrepreneurs" program in 2018. I joined Johan Richer in 2021 to initiate the transformation of Jailbreak into a tech cooperative, which then became multi in 2022.
:::

::::::::::::::

---

## Johan Richer

### Open data consultant · product manager

:::::::::::::: {.columns}

::: {.column .img-pdf .img-team width=25%}

![](images/team/johan-richer.jpg)

::: {.text-nano}
&nbsp; ✉️ [johan.richer@multi.coop](mailto:johan.richer@multi.coop)
:::

:::

::: {.column .text-micro width=75%}
Coming from a training in political sociology and international relations and after a stint at the Franch Ministry of Foreign Affairs, I discovered digital by participating in various collaborative mapping initiatives for humanitarian purposes in France and abroad: CartONG, MapFugees, Missing Maps …

I then joined Etalab, the French agency responsible for opening public data, to work on open government topics and create a **“digital toolbox”**

It was with other Etalab alumni that I founded Jailbreak in 2017 with the ambition to put 
free software, open data, open government and agile methods 
**to serve the general interest.** 
Jailbreak will become multi with the transition to SCOP in 2022.

I am also co-founder and treasurer of Code for France.
:::

::::::::::::::

---

## Amélie Rondot

### Developer · data analyst

:::::::::::::: {.columns}

::: {.column .img-team .img-pdf width=25%}
![](images/team/amelie-rondot-square.jpg)

::: {.text-nano}
&nbsp; ✉️ [amelie.rondot@multi.coop](mailto:amelie.rondot@multi.coop)
:::

:::

::: {.column .text-micro width=75%}
I was trained as a hydraulics engineer and worked for several years in the fields of drinking water and sanitation in project management and water services management companies. Then, after having worked for two years as a bicycle mechanic, I discovered a strong interest for digital development. 
**So I started a new reconversion to become a developer**

A first experience of one year and a half in DevOps within the start-up meteo*swift 
allowed me to develop my skills in backend development and in 
deployment and maintenance of microservices on the cloud.

Driven by the desire to contribute to the **development of open source software and to work in an ethical environment,** I was seduced by the way multi works and the projects carried by the team.
:::

::::::::::::::

---
