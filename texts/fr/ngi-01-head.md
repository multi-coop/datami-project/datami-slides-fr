---
title: <strong>Datami</strong>![](images/logos/logo-DATAMI-rect-colors.png)
subtitle: "
  Visualiser, éditer, partager, et contribuer<br>
  <strong>Produire de l'open data décentralisé</strong><br>
  <strong>inclusif & en commun</strong><br><br>"
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _développeur fullstack_
  </span><br>
  <span class="text-micro text-author-details">
    _co-fondateur de la coopérative [multi.coop](https://multi.coop)_
  </span><br>
  <span class="img-cover">
    ![](images/logos/logo-MULTI-colored-063442-02-w120.png)
  </span>'
date: <span class="emph-line">31 Mars 2023</span>
title-slide-attributes:
  # data-background-image: "images/sponsors/ngi_zero_entrust-logo.jpg"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 5%"
  data-background-position: "right 10% top 37%, right 4% top 40%"

---

# Sommaire

::: {.text-micro .summary}

- [Brève description du projet](#breve-description-du-projet) <span class="summary-page">3.1</span></span>
- [Contributing to the vision of the Next Generation Internet](#contributing-to-the-vision-of-the-next-generation-internet)<br>&nbsp;· Decentralized internet<br>&nbsp;· Digital commons <br>&nbsp;· Trustworthy open data<br>&nbsp;· Data sovereignty<span class="summary-page">4.1</span>
  - [Challenge #1](#challenge-1-make-open-data-more-intelligible) · Make open data more intelligible <span class="summary-page">5.1</span>
  - [Challenge #2](#challenge-2-make-updating-and-contributing-easier-and-secure) · Make updating and contributing easier and secure <span class="summary-page">6.1</span>
  - [Challenge #3](#challenge-3-build-a-solution-adapted-to-organizations-with-a-tight-budget) · Build a solution adapted to organizations with a tight budget <span class="summary-page">7.1</span>
- [Benchmark](#benchmark) <span class="summary-page">8.1</span>
- [Datami’s goals for the next 12 months](#datamis-goals-for-the-next-12-months) <span class="summary-page">9.1</span>
  - [Goal #1](#goal-1-research-development-of-missing-core-features) · Research & development of missing core features <span class="summary-page">10.1</span>
  - [Goal #2](#goal-2-keep-a-global-coherence-of-a-freelibre-and-open-source-software-project) · Keep a global coherence of a FLOSS project <span class="summary-page">11.1</span>
  - [Goal #3](#goal-3-exploring-use-cases-for-new-open-data-communities) · Exploring use cases for new open data communities <span class="summary-page">12.1</span>
- [The team](#the-team) <span class="summary-page">13.1
- [Provisional budget](#provisional-budget) <span class="summary-page">14.1</span>
- [Provisional timelines](#provisional-timelines) <span class="summary-page">15.1</span>
- [Credits](#credits) <span class="summary-page">16.1</span>

:::

