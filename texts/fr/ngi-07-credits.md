

# {#credits name='credits}

::: {.text-center}
**Thanks for your attention !**
:::

<br>

::::::::::::: {.columns}

::: {.column width="40%"}
::: {.img-nano .text-center}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::
:::

::: {.column width="20%"}
::: {.text-micro .text-center}
[Datami](https://datami.multi.coop/?locale=en)

is a project
led by the 
<br>
digital cooperative

[multi](https://multi.coop)
:::
:::

::: {.column width="40%"}
::: {.img-nano .text-center}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)
:::
:::

:::::::::::::


::: {.text-center}
**contact@multi.coop**
:::

---

## Our sponsors in 2022

Datami was **laureate of the Plan France Relance 2022** 
<br>
and has benefited from the support of the following organizations

![&nbsp;](images/sponsors/sponsors.png)

<!-- ---

::: {.text-center}
[Slides source](https://gitlab.com/multi-coop/datami-project/datami-slides-fr)
::: -->

