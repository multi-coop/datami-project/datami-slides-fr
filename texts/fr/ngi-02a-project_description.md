
# Brève description du projet

:::::: {.columns}

::: {.column width=65%}
::: {.img-test .text-center .text-micro .img-no-margin}
![](images/datami-views-panel-06.png)
<!-- ![&nbsp;](images/logos/logo-DATAMI-rect-colors.png) -->

::: {.img-nano .mini-inline}
<!-- ![](images/logos/logo-DATAMI-rect-colors-h75.png)&nbsp;&nbsp;[Datami website](https://datami.multi.coop/?locale=en) -->
[Official website](https://datami.multi.coop/?locale=en)
:::

:::
:::

::: {.column .text-micro .text-justify width=35%}
<br>

Les petites organisations sont à la fois des structures
qui possèdent les connaissances et les informations les plus précises
sur des sujets spécifiques ou sur un territoire,
mais sont également le type d'organisation
avec les
<b>
ressources les plus limitées pour ouvrir leurs données.
</b>

Le projet open source **Datami** vise à
<b>
aider les organisations à produire et partager localement des données structurées
</b>
en fournissant un ensemble de composants web gratuits, génériques et personnalisables
pour la visualisation et la contribution.
Ces composants incluent des fonctionnalités
permettant à 
<b>
chaque citoyen de
contribuer à ces données ouvertes décentralisées.
</b>

L'ambition de Datami est d'opérer une<br>
**une transition des données ouvertes**
<br>
**vers des biens communs numériques inclusifs**
<br>
pour le grand public.

:::

::::::


---

### · Besoins · <br>L'open data pour les organisations locales


::: {.text-center}
**Envisager l'open data locale comme autant de communs numériques**
:::


::: {.text-micro}

:::::: {.columns}

::: {.column .text-justify width=50%}
Les organisations locales sont souvent les mêmes à 
agir comme premiers répondants à des problèmes locaux sur le territoire ou sur des sujets bien précis.


Faire des bases de données thématiques permet d'agir plus efficacement au niveau local,
mais aussi à
<b>
sensibiliser les publics à un sujet :
</b>
environnement, santé, aide sociale, urbanisme, transports...

ONG, associations à but non lucratif, petites institutions publiques locales...
chacune a a propre façon de recueillir des données et des connaissances sur
les sujets sur lesquels elle intervient.
Ce genre de structure locale a également souvent besoin de
<b>
l'aide de sa propre communauté pour
maintenir et enrichir les données,
</b>
mais doivent malgré tout composer
<b>
avec des moyens très limités.
</b>
:::

::: {.column .text-justify width=50%}

Produire et publier des données de terrain - c'est-à-dire de manière décentralisée -
est une question de bonne information des citoyens
mais pose aussi la question de la place des citoyens dans la construction de cette information.

Pour sensibiliser leurs publics
les organisations cherchant à produire et publier des données 
doivent au préalable surmonter
des défis techniques, de conception et d'utilisation complexes à leur niveau.

Permettre de partager l'information que ces organisations produisent
est une question d'intérêt public,
mais aussi une activité de plus en plus
compliquée à réaliser en raison d'
<b>
un manque de ressources
pour ces organisations locales.
</b>

:::

::::::

:::


---

### · Problème ·<br>Les ressources limitantes des organisations locales

::: {.text-center}
**Comment produire de l'open data avec peu de ressources ?**
:::

::: {.text-micro}
Mettre en place une politique d'open data peut se révéler être une tâche ardue
pour des petites organisations car
<b>
publier un jeu de données ne suffit pas
</b>
... c'est en général juste un premier pas.
Les projets d'open data impliquent souvent de devoir s'adresser à différents types de publics
et de inciter à 
<b>partager</b>
les données, à 
<b>mieux comprendre</b>
les enjeux, à 
<b>contribuer</b>
à leur production,
tout cela en assurant que les données conservent leur
<b>intégrité</b>,
et que les producteurs puissent garder une 
<b>
souveraineté
</b>
sur leurs propres données,
dont ils sont en dernier ressort
<b>responsables.</b>
:::

:::::: {.columns .text-micro}

::: {.column .text-justify width=50%}
Les projets d'open data menés par de petites organisations
peuvent aisément être stoppés en raison de la complexité à produire
des jeux de données à la fois compréhensibles et accessibles 
pour le grand public.

Dans ce contexte un jeu de données ouvert se doit d'être adapté à des usages communs, 
et doit anticiper le risque d'exclure les publics "non tech" afin que ceux-ci puissent
aussi profiter ou/et contribuer à une information locale ouverte et transparente.

Afin de surmonter cette complexité les jeux de données ouverts nécessitent de respecter 
un ensemble de critères techniques. 
Cependant le fait d'être partagés via des outils intermédiaires, des plateformes ou des développements impliquent soit surcoûts, une perte de contrôle sur leur hébergement, ou encore du découragement ou de la perte de temps lors de la prise en main des outils...
:::

::: {.column .text-justify width=50%}
Les structures locales qui cherchent à mener des projets d'open data mais qui se trouvent à devoir « mettre en pause » ces projets citent souvent comme raisons  :

- **Budget serré**
- **Manque de compétences ou difficultés techniques**
- **Manque de temps pour organiser et modérer les contributions**
- **Manque d'intérêt ou impression de complexité**
- **Manque d'outils open source pour les producteurs de données de base**
- **Peur de perdre le contrôle ou la souveraineté sur les données**
:::

::::::


---

### · Solution · <br>Make open data and grassroot contributions<br>inclusive and affordable

:::::: {.columns .text-micro}

::: {.column width=25%}

<br><br>

::: {.img-no-margin .img-mini .text-center}
![](images/logos/logo-DATAMI-title.png)
:::

:::

::: {.column width=75%}

::: {.img-no-margin .img-small .text-center}
![](images/datami-views-panel-05.png)
:::

:::

::::::

:::::: {.columns .text-micro}

::: {.column width=25%}
<b>
  **More intelligible**
  <br>
  **an informative datasets**
</b>

::: {.text-justify}
Datami provides a variety of data-visualisations and views commonly used on internet
such as lists of cards, maps, graphs, diagrams... 
and also a variety of commons tools like interactive filters
you can customize for any specific dataset
:::
:::

::: {.column width=25%}
<b>
  **More easily open**
  <br>
  **to external contributions**
</b>

::: {.text-justify}
Datami relies on external platforms such as Gitlab or Gihub, 
thus also being able to use their APIs 
to send contributions onto datasets 
via merge requests on new branches.
:::
:::

::: {.column width=25%}
<b>
  **More structured**
  <br>
  **and interoperable datasets**
</b>

::: {.text-justify}
Datami includes the possibilty to associate data models to datasets.
For instance table schema standard (by Frictionless) is used as Datami's standard for csv tables.
:::
:::

::: {.column width=25%}
<b>
  **More affordable, sovereign**
  <br>
  **and transparent**
</b>

::: {.text-justify}
Datami delagates to external platforms (like Gitlab)
the data storage and the versionning control features.
Doing so Datami stands for a larger complementarity between FLOSS solutions,
and for a low-code approach to open data.
:::
:::

::::::

---

### · Ambitions ·

::: {.text-micro}
Datami's project aims to adress the latter problem by considering 
**the future of open data must be even more distributed,**
as much in its production as in its maintenance or considering broader target audiences. 

We have the ambition to provide a FLOSS (Free/Libre and Open Source Software) toolbox for grassroot organisations,
allowing them to accelerate their open data policies 
based on a combination of open source technologies.
:::

:::::: {.columns .text-micro}

::: {.column .text-justify width=50%}
### VALORIZE

Enable organisations of all sizes 
<b>to produce open data & valorise their datasets</b> with their communities.

:::

::: {.column .text-justify width=50%}
### CONTRIBUTE

Empower organisations' audiences by  helping them to 
<b>contribute to open data freely</b>, easily, while respecting their privacy.

:::

::: {.column .text-justify width=50%}
### DISTRIBUTE

Help any organisation to create 
<b>interoperable & structured</b> datasets,
with the strict minimum of technical background. 

:::

::: {.column .text-justify width=50%}
### PROMOTE FLOSS

Promote <b>complementarity within FLOSS (Free/Libre and Open Source Software) robust initiatives</b>, 
notably by the use of Git / Gitlab as a backend for data storage and version control.
:::

::::::

::: {.text-micro .text-center}
As you will discover later in this document we segmented those ambitions into three challenges : 
<br>
**intelligibility** &nbsp;
**contributibility** &nbsp;
**affordability**
:::

---

### · Innovation / differentiation · 1/2

::: {.text-micro}

:::::: {.columns}

::: {.column .text-justify width=50%}
There is nowadays a large variety of solutions - either open source or proprietary - for data visualisation or edition.
Those solutions can either be provided as Saas or stand alone. 

Most common ones are Metabase or Grist in the open source category, or Airtable and GSheet in the proprietary section (*).

Those existing solutions still have two common factors :

- They suppose either to <b>use a backend server you don't have sovereignty over ;</b> or to <b>install and maintain a backend server yourself</b> ;

- They often <b>lack or provide limited features for contribution and moderation.</b>

:::

::: {.column .text-justify width=50%}
**Datami's approach intends to do "more with less"** : 
we intended to provide more features for contribution and moderation on a dataset, 
whereas liberating us from developping ourselves a complex backend for data storage and/or version control.

The solution we found was quite "on the nose" : 
the best tools we can think of today for making or managing contributions - and then to moderate them - 
are the git language and platforms relying on git like Gitlab.

That said and to our knowledge,
<b>
we didn't find existing open source solutions 
adressing at the same time the needs 
we encountered
</b>
talking to small organisations
hoping to produce open data for their audiences :
a tool
<b>
simple to apprehend,
affordable, 
helping audiences to propose contributions,
with variety and quality in terms visualisations and design.
</b>

:::

::::::

(*) _An [extensive benchmark](https://datami.multi.coop/benchmark?locale=en) is provided later in this document._

:::

---

### · Innovation / differentiation · 2/2

::: {.text-micro}

<b>
To adress the needs of open data's grassroot producers
we designed a software architecture 
<br>
based on a serie of simple principles
we follow altogether
</b>

:::::: {.columns .text-justify}

::: {.column width=25%}
**Use existing platforms**<br>**as backends**

We use platforms such as 
Gitlab / Github / Bitbucket / SourceForge / external APIs... 
<b>
as backends for data storage and version control.
</b>

Such platforms are proved to be robust,
we only account for a few,
some are FLOSS (Free/Libre and Open Source Software),
and they already provide all the tools
needed for version control and moderation.

Thus we can store either datasets
or any other file (such as configuration files, schemas, text files...)
on those platforms independantly from Datami's application.
:::

::: {.column width=25%}
**Embed your own**<br>**interfaces**

The spaces where small orgaizations 
need to valorize their data is
where their target audience goes,
usually the organization's own website. 

It is very common small for organizations to use
Wordpress or on-the-shelf solutions for their website,
but some are custom-made.

The use of 
<b>
web components 
</b>
technology and web standards (or "widgets") is the choice 
we made to 
<b>
ensure the ability to embed data onto any website.
</b>
:::

::: {.column width=25%}
**Keep the data structure**<br>**at all steps**

Data interoperability is a principle we implemented
in Datami with the use of standards, 
such as Frictionless for tables and json files.

Any table displayed with Datami can be 
<b>
associated with a local or distant schema, 
</b>
in addition to configuration files. 

Doing so, Datami can display datasets
accordingly to its schema,
but also provide pre-rendered forms
to help for open contributions.
:::

::: {.column width=25%}
**Display the data**<br>**as you need to**

Genericity and customisation are core intentions of Datami.
We search for a 
<b>
balance between the need to customize 
</b>
the UI/UX of the widgets,
<b>
and the need for a versatile tool
</b>
adaptable to a large range of data structures or properties.

We distinguish several types of files : 
end-user data (csv, text, json, API requests...),
schemas files,
and configuration files. 

What makes a dataset unique
can be set up either in its schema or configuration files,
or even in the web component's html itself. 
:::

::::::

:::


