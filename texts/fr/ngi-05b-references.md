
## Multi.coop · References

:::::::::::::: {.columns}

::: {.column .text-center width=25%}
**Vitrine**<br>&nbsp;

::: {.img-pdf}
![](images/references/vitrine1.png)
:::

::: {.text-nano .text-justify}
To easily generate a website using content retrieved from third party pages.

Originally, its main use case was for organizing hackathons, to present its projects and participants, datasets, tools, etc.

[For more info](https://gitlab.com/multi-coop/vitrine)
:::

:::

::: {.column .text-center width=25%}
**catalogue**<br>**.data.gouv.fr**

::: {.img-pdf}
![](images/references/cataloguedatagouv.png)
:::

::: {.text-nano .text-justify}
catalog.data.gouv.fr is an online service that allows organizations to create, manage and open their dataset catalogs.

This project follows an investigation of which [the synthesis](https://multi-coop.gitlab.io/slides/investigation-catalogue/synthese.html) is an example of the methodology we propose.

[For more info](https://catalogue.data.gouv.fr/)
:::

:::

::: {.column .text-center width=25%}
**Validata**<br>&nbsp;

::: {.img-pdf}
![](images/references/validata-01.png)
:::

::: {.text-nano .text-justify}
Validata aims to offer a platform for validating open data.

It is aimed at French local authorities that want to validate 
the quality and interoperability of the data they publish using an external tool. 
It also allows data warehouse managers to qualiﬁer the integrity of the data 
they wish to exploit before importing it into a multi-source database.

[For more info](https://validata.fr/)

:::

:::

::: {.column .text-center width=25%}
**DBnomics**<br>&nbsp;

::: {.img-pdf}
![](images/references/dbnomics-01.png)
:::

::: {.text-nano .text-justify}
DBnomics is one of the largest economic databases in the world, 
aggregating hundreds of millions of time series from dozens of sources 
and making them available through a single API.

[For more info](https://db.nomics.world/)

:::

:::

::::::::::::::



