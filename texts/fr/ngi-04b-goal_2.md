


# **Goal #2**<br>Keep a global coherence of a<br> Free/Libre and Open Source Software project

**Enhance the appropriability of the solution by developpers and data producers**
<br>
**and explore a market segment**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}

Datami is a FLOSS project deep to its bones.
We are fully commited to produce good functional and auditable code, 
but we also value the other dimensions of FLOSS approach : 

- Provide UI/UX improving inclusiveness of the solution ;
- Empower future users with an extensive documentation ;
- Being able to communicate clearly about Datami's proposition value ;

:::

::: {.column width=50%}
UX and UI design are key dimensions to improve so 
<b>
any user - with or without technical knowledge -
can be welcome and incentived to contribute to open data 
</b>
, hence building digital commons of information doing so. 

The technical documentation is a worksite in itself, 
as of Datami's landing website.
Both are utterly important to 
<b>
help users broadly engage in using Datami.
</b>
:::

::::::

By converging on the several aspects of this goal we believe 
we may then 
<b>
explore further a forgotten market segment.
</b>
<br>
This segment is represented by all small-sized or local organisations 
wishing to produce open data respectfully of their communities' rights to privacy,
and despite limited resources and skills.

:::


---

## Existing open source audits

**We were audited in late 2022
by the cabinet Smile**
<br>
on the open source dimensions of the Datami project. 


:::::: {.columns}

::: {.column width=30%}

::: {.text-micro}
Smile helped us formalising different recommandations 
about 
<b>
Datami's open source strategies, its functional roadmap, or even about the project's governance.
</b>

::: {.text-center}
![](images/audits/smile-logo.png)
:::

All the documents related to this audit can be requested 
by contacting us at : 
[contact@multi.coop](mailto:contact@multi.coop)
:::

:::

::: {.column width=70%}
![&nbsp;](images/audits/smile-01.png)
:::

::::::

---

## Explore a forgotten market segment

**Given the solutions on the market Datami has a role to play**<br>**for small-sized data producers**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
The [benchmark we conducted](https://datami.multi.coop/benchmark/?locale=en) 
tends to show us that 
only proprietary solutions had targeted or/and convinced 
a broad range of organisations to use their solution.

Open source solutions for edition or data-visualisation
are usually targeting audiences with 
higher technical background than average.

Furthermore, when not used in Saas open source solutions 
all need to install and maintain a backend service
to keep a sovereign use of said solutions.

Those caracteristics usually _de facto_
exclude small-sized or local organisations 
with limited skills or resources from adopting
open source solutions for producing open data.

:::

::: {.column width=50%}

We believe Datami may have an unique place 
in this market landscape due to the 
specifications we prioritized in its development :

- <b>it's designed for a broad audience ;</b>

- <b>it's designed to make open contributions ;</b>

- <b>it includes a moderation process ;</b>

- <b>it has no specific backend ;</b>

- <b>it's open source ;</b>
:::

::::::

:::

---

## Build a coherent commercial offer

**We aim to propose different services aroud Datami**

<br>

::: {.text-micro}

:::::::::::::: {.columns}

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-setup.png)

::: {.text-center}
**Datami setup**
:::

Multi.coop can provide an economic package
of a few days only for support in setting up Datami
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-pack.png)

::: {.text-center}
**Datami as free software** 
:::

Use Datami as it is based on the documentation, 
you are free!
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-custom_dev.png)

::: {.text-center}
**Custom developments** 
:::

Multi.coop can help you responding to you needs
using Datami while adding new features 
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-setup-interface.png)

::: {.text-center}
**Online configuration** \*
:::

Use a simple interface to setup yourself the 
widget you need for your datasets.

_\* This offer is currently under construction_

:::

::::::::::::::

::: 

::: {.text-micro}
In order to make Datami as accessible and affordable as possible, 
our principle is to 
<b>
mutualize design and development costs.
</b>
<br>
All developments - even minimal ones - contributing to improve Datami 
eventually benefit all users.
:::

---

## Maintain the documentation up to date

**Technical documentation remains a keystone of any FLOSS project**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
We started making a documentation as clear as possible 
since the very beginning of the project.

Keeping up with the documentation as the rest of the project
continues in its other dimensions
(business model, iterations, debugging, ...)
is a real challenge.
:::

::: {.column width=50%}

<b>
We adapted our roadmap 
so every new iteration and new feature development 
now includes its own documentation time 
into its provisional timing.
</b>
:::

::::::

:::

---

## Enhance the bug reporting & <br>new features demands processes

:::::: {.columns}

::: {.column width=50%}

**As for any FLOSS project**
<br>
**issues and improvements**
<br>
**can be publicly reported**

::: {.text-micro}
Our source code is fully open sourced 
on Gitlab with a miror repository on Github.

We are continuously improving our documentation 
but we are well aware we could do more 
to help users present and future 
in participating more actively in the project.
:::

:::

::: {.column width=50%}

::: {.text-micro}
There are some leads 
we don't mention in our global roadmap
but we noted as simple means 
to improve the process demands may be brought back to us :

- <b>Issues templates</b>
- <b>Merge requests templates</b>
- <b>Contributing & rules of conduct files</b>
- <b>Welcome pack</b>
- <b>Governance diagram</b>
:::
:::

::::::

---

## Translate documentation & application <br>into other languages

**Datami was from the start designed to be internationalized**


::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}

For now the Datami project - including 
the application, the official website, and the technical documentation -
are translated into French and English.

<b>
We value language as a mean to make our solution 
more accessible and more inclusive, either for target audiences or data producers.
</b>
:::

::: {.column width=50%}

We included in our roadmap
the translation to spanish 
as one of our next important improvements.
That being, we also consider translating to other languages (such as German or Italian)
as posiible improvements too, 
depending on the needs of our next use cases
and the resources at hand. 

:::

::::::

:::

---

## Make the repositories more visible <br>to FLOSS communities

**We are starting to implement a communication strategy**<br>**towards open source communities**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}

Even if they are essential to the project 
we know that "just" having a new open source solution, 
its technical documentation, and a value proposition, 
may not be enough to alone encourage developpers to contribute.

:::

::: {.column width=50%}

We draw and we are currently testing different 
<b>
strategies to promote Datami
to open data and open source communities,
</b>
such in the vue.js, datavisualization, javascript, or other potential 
low-level users and contributors communities on Reddit, Twitter, Linkedin, or DevTo...

:::

::::::

:::




