# Revealjs slideshow for Datami project

![stars](https://img.shields.io/badge/dynamic/json?color=turquoise&label=gitlab%20stars%20%E2%98%85&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39268471)

A template for writing slideshows in markdown and transform them into html or pdf.

## Deployed Gitlab pages

- https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-fr.html
- https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-en.html

## Usage

- Install dependencies :
  - [pandoc](https://pandoc.org/) for html output
  - [decktape](https://github.com/astefanutti/decktape) for pdf output. 

- Check that you have the proper fonts : [`Poppins 
  semibold`](https://fonts.google.com/specimen/Poppins)
  & [`Carlito`](https://www.1001fonts.com/carlito-font.html) (or [`Lato`](https://fonts.google.com/specimen/Lato))

- Write your presentation in pandoc using [pandoc markdown syntax](https://pandoc.org/MANUAL.html#slide-shows) (default `presentation-fr.md` file shows some examples)

- Convert to a [revealjs](https://revealjs.com/) html presentation with `make render_html` (or just `make`) or to a pdf with `make render_pdf`.
  
## Gitlab pages

Build and deployed to Gitlab pages with Gitlab CI.

To render a collection of presentations on the same instance of pages, just repeat `make gitlab_pages` with the specific `IN` and optionnaly `OUT` variables in the `.gitlab-ci.yml` file. The first call will determine to which presentation the root will be redirected.

## Customization

- The name of the presentation and of the output can be changed with environment variables `IN` and `OUT`. `IN` is the markdown source file, and  `OUT` is the basename (no extension) of the output files. They can be updated in the header of the "Makefile" accordingly, or passed to the  command line as in the following example : `make render_html OUT=$(date -I)_presentation-fr`.
- Adding and positionning the logo of the customer on the title page can be done using the yaml header of the markdown file.
- The default reciepe (`make` or `make all`) can be customized to your liking,see commented block in `Makefile` for an example that renders several presentations at once.
