---
title: Datami![](images/logos/logo-DATAMI-rect-colors.png)
# ref: https://www.ow2con.org/view/2023/Abstract_Community_Day#14061510
subtitle: |
  <br>
  <strong>
    SHARE, SHOW AND CONTRIBUTE
  </strong>
  <br>
  <strong>
    TO DIGITAL COMMONS
  </strong>
  <br>
  <br>
  <span class="text-micro">
    A customizable open source widget 
    to visualize and edit open datasets
  </span><br>
  <span class="text-micro">
    without any other backend than Github or Gitlab
  </span><br>
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _fullstack developer / lead dev of Datami_
  </span><br>
  <span class="text-micro text-author-details">
    _co-founder of [tech cooperative multi](https://multi.coop)_
  </span><br>
  <span class="text-micro text-author-details">
    _julien.paris@multi.coop_
  </span><br>
  <span class="img-cover">
    ![](images/logos/logo-MULTI-colored-063442-02-w120.png)
  </span>
'
date: '
  <a href="https://datami.multi.coop?locale=en" target="_blank">
    https://datami.multi.coop
  </a><br>
  <span class="emph-line">June 14th 2023</span> / 
  Plan by pressing <span class="text-nano">`esc`</span><br>'
title-slide-attributes:
  data-background-image: "images/sponsors/ow2con23_borderL.png"
  # data-background-image: "images/logos/logo-DATAMI-rect-colors.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 5%"
  data-background-position: "right 10% top 40%, right 4% top 40%"
---

#

::: {.text-center}

::: {.img-medium}
![&nbsp;](images/QR-code-ow2.png)
:::

**https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-en-ow2.html**
:::

# Help your publics with your data

::: {.text-center style="font-size:2em"}
**What are the challenges ?**
:::

## Challenge #1 

### Make your data **intelligible**

---

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/messy-excel-02.png)
:::
:::

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ODF-map-all.png)
:::
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
Your raw data <br>**without** Datami
:::
:::

::: {.column width="50%"}
::: {.text-center}
Your data enhanced <br>**with** Datami
:::
:::
::::::::::::::

---

## Challenge #2 

### Make **updating** and **contributing** easier

---

:::::::::::::: {.columns}

::: {.column width="40%"}
Handling data is a matter of habits, facilitating their manipulation for the greatest number means that you have to **adapt to the habits** of the greatest number

:::fragment
To allow everyone to easily contribute to the data, the view in the form of a table is still the most commonly adopted today.
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/table-view-01.png)
:::
:::

::::::::::::::

---

## Challenge #3

### Make all of the above **with a tight budget**

---

When you are a small or medium-sized structure (association, community, etc.) and you produce data of general interest, it can be complicated to highlight them on your site or to call on your community to put them up to date

::: {.text-center}
**Lack of resources, lack of skills, lack of time...**
:::

:::::::::::::: {.columns}
::: {.column width="30%"}
:::fragment
::: {.text-justify}
The cost of the usual technical solutions for sharing / viewing / contributing to data is often explained by the technical **complexity** of these functionalities
:::
:::
:::
::: {.column width="30%"}
:::fragment
::: {.text-justify style="padding: 0 1.5em 0 1.5em"}
Another cost is related to the need to set up dedicated **servers** in _backend_ or very specific configurations
:::
:::
:::
::: {.column width="30%"}
:::fragment
::: {.text-justify}
The **maintenance** of applications or servers often generates significant costs
:::
:::
:::
::::::::::::::

:::fragment
Datami's original architecture makes it possible to **get rid of a large part of these _backend_ server costs** while allowing customization dataset by dataset
:::



# The team

**Who are the people behind Datami ?**

---

## A project by the tech cooperative multi

<br>

:::::::::::::: {.columns}

::: {.column width="30%"}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)

::: {.text-center}
[https://multi.coop](https://multi.coop)
:::

:::

::: {.column width="70%"}
Our cooperative contributes to the development of **digital commons** and associated services, by bringing together a community of professionals working for a **digital of general interest**
::: 

::::::::::::::

---

## Datami's team members

**Dedicated, experienced and multi-disciplinary profesionals**

---

## Julien Paris

### Developer  · product owner

:::::::::::::: {.columns}

::: {.column .img-team .img-pdf width=25%}
![](images/team/julien-paris-01.png)

::: {.text-nano}
&nbsp; ✉️ [julien.paris@multi.coop](mailto:julien.paris@multi.coop)
:::

:::

::: {.column .text-micro width=75%}
After initial training as a DPLG architect, then having carried out cultural projects in international cultural cooperation (Spanish embassy in Mexico, French embassy in Egypt), doing research (CNRS doctoral student in Turkey), I finally switched to digital as a _fullstack_ developer.

I only develop open source softwares, thus hoping to support the **open data and free software movement.** 
I am particularly interested in data visualization and open data contribution processes.

Since 2015 I have **directed and developed digital projects** for french ministerial institutions (ANCT, Bercy, CGET, Agence Bio), inter-ministerial (DINUM), associations and think tanks involved in general interest (Mednum , PiNG, Ternum, Rhinocc, Décider Ensemble), and for other public structures such as a public mediatheque nearby Nantes.

I participated in Etalab's "General Interest Entrepreneurs" program in 2018. I joined Johan Richer in 2021 to initiate the transformation of Jailbreak into a tech cooperative, which then became multi in 2022.
:::

::::::::::::::

---

## Johan Richer

### Open data consultant · product manager

:::::::::::::: {.columns}

::: {.column .img-pdf .img-team width=25%}

![](images/team/johan-richer.jpg)

::: {.text-nano}
&nbsp; ✉️ [johan.richer@multi.coop](mailto:johan.richer@multi.coop)
:::

:::

::: {.column .text-micro width=75%}
Coming from a training in political sociology and international relations and after a stint at the Franch Ministry of Foreign Affairs, I discovered digital by participating in various collaborative mapping initiatives for humanitarian purposes in France and abroad: CartONG, MapFugees, Missing Maps …

I then joined Etalab, the French agency responsible for opening public data, to work on open government topics and create a **“digital toolbox”**

It was with other Etalab alumni that I founded Jailbreak in 2017 with the ambition to put 
free software, open data, open government and agile methods 
**to serve the general interest.** 
Jailbreak will become multi with the transition to SCOP in 2022.

I am also co-founder and treasurer of Code for France.
:::

::::::::::::::


---

## Erica Delagnier

### Developer · full stack

:::::::::::::: {.columns}

::: {.column .img-team .img-pdf width=25%}
![](images/team/erica-delagnier-square.jpeg)

::: {.text-nano}
&nbsp; ✉️ [erica.delagnier@multi.coop](mailto:amelie.delagnier@multi.coop)
:::

:::

::: {.column .text-micro width=75%}
Graduated from ENSEEIHT and self-taught since then, **my goal is to create intuitive and sustainable IT tools serving the general interest**. From project to project, I have been able to contribute to home support for vulnerable people, access to research journals, open source collaborative writing (EIG program) and recently to the fight against the financial fragility of families.

multi allows me to **join a community of peers to defend these values together** and allow us to put our skills at the service of structures that share them.
:::

::::::::::::::

---

## Amélie Rondot

### Developer · data analyst

:::::::::::::: {.columns}

::: {.column .img-team .img-pdf width=25%}
![](images/team/amelie-rondot-square.jpg)

::: {.text-nano}
&nbsp; ✉️ [amelie.rondot@multi.coop](mailto:amelie.rondot@multi.coop)
:::

:::

::: {.column .text-micro width=75%}
I was trained as a hydraulics engineer and worked for several years in the fields of drinking water and sanitation in project management and water services management companies. Then, after having worked for two years as a bicycle mechanic, I discovered a strong interest for digital development. 
**So I started a new reconversion to become a developer**

A first experience of one year and a half in DevOps within the start-up meteo*swift 
allowed me to develop my skills in backend development and in 
deployment and maintenance of microservices on the cloud.

Driven by the desire to contribute to the **development of open source software and to work in an ethical environment,** I was seduced by the way multi works and the projects carried by the team.
:::

::::::::::::::

---

# Enhance your open datasets with **Datami**

::: {.img-mini}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::

::: {.text-center}
**https://datami.multi.coop**
:::

---

<video
  id="datami-video-presentation"
  width="100%"
  height="90%"
  style="box-shadow: 0 0 20px #D7D7D7;"
  poster="https://raw.githubusercontent.com/multi-coop/datami-website-content/images/logos/logo-DATAMI-rect-colors-03.png"
  controls>
  <source
    src="https://raw.githubusercontent.com/multi-coop/datami-website-content/main/videos/DATAMI_PRESENTATION-EN.mp4"
    type="video/mp4">
</video>

---

## Your spreadsheets transformed into maps

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami allows you to create **customized interactive maps**

:::fragment
Your territorial data can be visualized in geographical form, whatever their themes
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ping-map-detail-01.png)
:::
:::

::::::::::::::

---

## View your data from all angles

:::::::::::::: {.columns}

::: {.column width="40%"}
Your data can be viewed as **spreadsheets**, **maps**, miniature or detailed **lists of records**, or **graphs**

:::fragment
All views are interactive and customizable to highlight all the specifics of your data
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-views-01.png)
:::

::::::::::::::


--- 

## Adapt Datami <br>to your needs

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-sketches-01.png)
:::
:::

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-sketches-02.png)
:::
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
Your **sketches** <br>and your **datasets**...
:::
:::

::: {.column width="50%"}
::: {.text-center}
... implemented and enhanced <br>with **Datami**
:::
:::
::::::::::::::

---

## You control your data <br>Datami makes it understandable

Datami's architecture is designed as an **interface between the citizen and your database** in order to facilitate the link **between citizens and open data producers**

::: {.no-mt .h-30}
![](images/roadmaps/archi-devs-en.png)
:::

---


**Datami doesn't store your data** : your data stays within the tool of your choice (Github, Gitlab, database or API). <br>
Using Datami is more **affordable** because you don't have to install nor maintain a dedicated _backend_. 

---

## Integrate Datami into your <br>site and your partners

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami uses **_widgets_** technology: turnkey and customizable modules that you can add to an existing site

:::fragment
Datami _widgets_ are **_open source_**, simple to copy and paste, without subscription, **without additional cost**
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/widget-copy-01.png)
:::

::::::::::::::



# A simplified process <br>for contributory data updates

## Empower your teams and audiences <br>to improve data

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami includes a **contribution and moderation system**

:::fragment
Based on the Git language, Datami _widgets_ allow you to keep control of your data and manage contributions, **without creating an account** to suggest improvements
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-contribution-01.png)
:::
:::

::::::::::::::

---

## Structure your data <br>to make it interoperable

:::::::::::::: {.columns}

::: {.column width="40%"}
Your data may be associated with files to structure it, such as **data schema files**

:::fragment
By associating your data set with a data schema respecting international **standards** you ensure that they can be correctly reused and improved
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-schemas-01.png)
:::

::::::::::::::

# A management tool

## Drive your field actions through data

:::::::::::::: {.columns}

::: {.column width="40%"}
**Exploring your data** allows you to better understand and manage your actions in the field

:::fragment
Datami allows you to set up personalized interactive data-visualizations, in order to make your data easily explorable
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-pilotage-01.png)
:::

::::::::::::::

---

### Discover the interfaces

<video
  id="datami-video-presentation"
  width="100%"
  height="85%"
  style="box-shadow: 0 0 20px #D7D7D7;"
  poster="https://raw.githubusercontent.com/multi-coop/datami-website-content/images/logos/logo-DATAMI-rect-colors-03.png"
  controls>
  <source
    src="https://raw.githubusercontent.com/multi-coop/datami-website-content/main/videos/DATAMI_TUTORIEL-FR.mp4#t=0,135"
    type="video/mp4">
</video>


# An _open source_ tool

## A free and multi-purpose software

![&nbsp;](images/screenshots/datami-feats-matrix-en.png)

---

Datami is fully auditable and reusable **free licensed software**

To discover and learn how to use Datami you can:

:::incremental
- Go to the [official Datami website](https://datami.multi.coop);
- Directly access the [Datami source code](https://gitlab.com/multi-coop/datami-project/datami);
- Consult the [technical documentation](https://datami-docs.multi.coop) and online tutorials;
- Call on the [Multi cooperative](https://multi.coop) for advice in data science and for training.
:::



---


## The official website

:::::::::::::: {.columns}

::: {.column width="40%"}
On our **official website** you will find **videos** of presentation, **examples**, as well as our **blog** space

:::fragment
The site is translated into French and English
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-website-en.png)
:::
::: {.text-center}
🚀 [datami.multi.coop](https://datami.multi.coop)
:::
:::

::::::::::::::

---

## The source code

:::::::::::::: {.columns}

::: {.column width="40%"}
Our **source code is on Gitlab** under _open source_ license

:::fragment
Do not hesitate to report _bugs_ to us by suggesting [_issues_](https://gitlab.com/multi-coop/datami-project/datami/-/issues), or to give ideas for new features on our [ _roadmap_](https://gitlab.com/multi-coop/datami-project/datami/-/boards/4736577)

A [mirror repo](https://github.com/multi-coop/datami) is also automatically synced to Github

Don't forget to leave a little ⭐️ if you like the project!
:::
:::


::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-gitlab.png)
:::
::: {.text-center}
💻 [Repo Gitlab](https://gitlab.com/multi-coop/datami-project/datami)

![](https://img.shields.io/badge/dynamic/json?color=turquoise&label=gitlab%20stars%20%E2%98%85&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39268471)

:::
:::

::::::::::::::

---

## The technical documentation site

:::::::::::::: {.columns}

::: {.column width="40%"}
Also visit our dedicated **documentation site**


:::fragment
The site is translated into French and English

You will find different sections there: technical principles, tutorials, examples, description of the different widgets and their configuration elements...
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-doc-website.png)
:::
::: {.text-center}
🔧 [datami-docs.multi.coop](https://datami-docs.multi.coop)
:::
:::

::::::::::::::

---

## The _stack_

:::::::::::::: {.columns}

::: {.column width="40%"}
The technical _stack_ is **entirely composed of _open source_ libraries**
::: 

::: {.column width="60%"}
- [`Vue.js (v2.x)`](https://v2.vuejs.org/v2/guide)
- [`VueX`](https://vuex.vuejs.org/)
- [`vue-custom-element`](https://github.com/karol-f/vue-custom-element)
- [`gray-matter`](https://www.npmjs.com/package/gray-matter)
- [`Showdown`](https://www.npmjs.com/package/showdown) + [`showdown-table extension`](https://github.com/showdownjs/table-extension#readme)
- [`Bulma`](https://bulma.io/) + [`Buefy`](https://buefy.org/)
- [`Material Design`](https://materialdesignicons.com/)
- [`Fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
- [`JSDiff`](https://github.com/kpdecker/jsdiff)
- [`Diff2html`](https://www.npmjs.com/package/diff2html)
- [`MapLibre`](https://maplibre.org)
- [`ApexCharts`](https://apexcharts.com)
:::

::::::::::::::

<!-- ---

## References

:::::::::::::: {.columns}

::: {.column width="50%"}
On [Le Comptoir du Libre](https://comptoir-du-libre.org/fr/softwares/583)

:::fragment
Visit this page to leave a comment on Datami!
:::
:::

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ref-comptoir-du-libre.png)
:::
:::

::::::::::::::

:::::::::::::: {.columns}

::: {.column width="50%"}
On [AlternativeTo](https://alternativeto.net/software/datami/about/p)

:::fragment
Visit this page to leave a comment on Datami!
:::
::: 

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ref-alternative-to.png)
:::
:::

:::::::::::::: -->

# Service offer

## The Datami offer by the multi cooperative

:::::::::::::: {.columns}

::: {.column width="40%"}
In order to make Datami as accessible as possible, our principle is to **share design and development costs**

:::fragment
All developments - even minimal ones - contributing to improving Datami thus ultimately benefit all users.
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/offer/multi-datami-offer-en.png)
:::

::::::::::::::

---

## Setting up Datami

:::::::::::::: {.columns}

::: {.column width="40%"}
![&nbsp;](images/offer/datami-logo-setup.png)
:::

::: {.column width="60%"}

We offer an **economic package of a few days** only for support in setting up Datami

:::fragment
We can help you configure Datami and advise you in your data recovery project
:::

:::

::::::::::::::

---

## Free software

:::::::::::::: {.columns}

::: {.column width="40%"}
![&nbsp;](images/offer/datami-logo-pack.png)
:::

::: {.column width="60%"}
Datami is a **100% _open source_** tool

:::fragment
You can use Datami as it is based on the documentation, you are free!
:::

:::
::::::::::::::

---

## Custom Developments

:::::::::::::: {.columns}

::: {.column width="40%"}
![&nbsp;](images/offer/datami-logo-custom_dev.png)
:::

::: {.column width="60%"}
Do not hesitate to contact us to tell us about **your needs** to establish a personalized quote

:::fragment
We are committed to continuing to develop and improve Datami, and we also offer services in _data science_ and _data engineering_
:::

:::
::::::::::::::

# Installation guide

## The source code

Clone the source code from Datami's [Gitlab repo](https://gitlab.com/multi-coop/datami-project/datami)

```bash
git clone git@gitlab.com:multi-coop/datami-project/datami.git
````

Then navigate to the folder

```bash
cd datami
```

---

## Installation

:::::::::::::: {.columns}
::: {.column width="30%"}
Install `npm 8.3.2` ...
::: 
::: {.column width="70%"}
```bash
npm install -g npm@8.3.2
```
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="30%"}
... or use `nvm`
::: 
::: {.column width="70%"}
```bash
brew install nvm # sur mac
nvm use
```
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="30%"}
Install the dependancies
::: 
::: {.column width="70%"}
```bash
npm install
```
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="30%"}
Create a local `.env` file based on `example.env`
::: 
::: {.column width="70%"}
```bash
cp example.env .env
```
:::
::::::::::::::

---

## Run Datami

:::::::::::::: {.columns}
::: {.column width="30%"}
Run the script
::: 
::: {.column width="70%"}
```bash
npm run serve
```
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="30%"}
In your browser
::: 
::: {.column width="70%"}
```bash
http://localhost:8080
```
:::
::::::::::::::

---

## Datami _widgets_ examples list

:::::::::::::: {.columns}
::: {.column width="30%"}
Run the local server
::: 
::: {.column width="70%"}
```bash
nvm use
npm run http
```
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="30%"}
In your browser
::: 
::: {.column width="70%"}
```bash
http://localhost:8180/html-tests/
```
:::
::::::::::::::


# Benchmark

---

::: {.text-center}
There are several data visualization and editing solutions <br>sharing similarities with [Datami](https://datami.multi.coop)

Here are some of the most popular solutions

:::fragment
**Those benchmarks are given for information only**

Do not hesitate to let us know by writing to<br>`contact@multi.coop` <br>if you wish to make any corrections or additions
:::

:::

---

### Datavisualisation tools


| Solution               | Solution type    | Langages                            | Difficulty            | Saas    | Official website                                                 |
| ------------------     | ---------------- | ----------------------------------- | --------------------  | ------- | ---------------------------------------------------------------- |
| **Gogocarto**          | Open source      | Custom                              | Easy                  | Yes    | [https://gogocarto.fr/projects](https://gogocarto.fr/projects)   |
| **Umap**               | Open source      | Custom                              | Facile                | Yes    | [https://umap.openstreetmap.fr/](https://umap.openstreetmap.fr/) |
| **Lizmap**             | Open source      | Propre langage de requête           | Medium                | Yes    | [https://www.lizmap.com](https://www.lizmap.com)                 |
| **Apache Superset**    | Open source      | SQL                                 | Medium                | Yes    | [https://superset.apache.org/](https://superset.apache.org/)     |
| **Apache Zeppelin**    | Open source      | Several programmation languages     | Hard                  | No     | [https://zeppelin.apache.org/](https://zeppelin.apache.org/)     |
| **BIRT**               | Open source      | Java, JavaScript                    | Hard                  | No     | [https://www.eclipse.org/birt/](https://www.eclipse.org/birt/)   |
| **FineReport**         | Open source      | Java, JavaScript                    | Medium                | No     | [https://www.finereport.com/en](https://www.finereport.com/en)   |
| **Grafana**            | Open source      | Custom request language             | Hard                  | No     | [https://grafana.com/](https://grafana.com/)                     |
| **Metabase**           | Open source      | SQL                                 | Easy                  | Yes    | [https://www.metabase.com/](https://www.metabase.com/)           |
| **Redash**             | Open source      | SQL                                 | Medium                | No     | [https://redash.io/](https://redash.io/)                         |
| **Datasette**          | Open source      | SQL                                 | Medium                | No     | [https://datasette.io/](https://datasette.io/)                   |
| **LightDash**          | Open source      | Dbt                                 | Medium                | Yes    | [https://www.lightdash.com/](https://www.lightdash.com/)         |
| **Google Data Studio** | Free (but not open) | SQL                              | Easy                  | Yes    | [https://datastudio.google.com/](https://datastudio.google.com/) |
| **Datawrapper**        | Commercial       | API, CSV, GSheet                    | Easy                  | Yes    | [https://www.datawrapper.de/](https://www.datawrapper.de/)       |
| **Google Looker**      | Commercial       | LookML                              | Hard                  | Yes    | [https://looker.com/](https://looker.com/)                       |
| **Microsoft Power BI** | Commercial       | DAX et M                            | Medium                | Yes    | [https://powerbi.microsoft.com/](https://powerbi.microsoft.com/) |
| **QlikView**           | Commercial       | Custom script language              | Hard                  | Yes    | [https://www.qlik.com/](https://www.qlik.com/)                   |
| **Tableau**            | Commercial       | Custom                              | Medium                | Yes    | [https://www.tableau.com/](https://www.tableau.com/)             |

---

### Online editing tools

| Solution            | Solution type | Langages                 | Difficulty             | Saas | Targeted public                                                              | Official webiste                                                             |
|---------------------|-------------|----------------------------|----------------------  |------|--------------------------------------------------------------------------    |------------------------------------------------------------------------------|
| **Apache Zeppelin** | Open source | Scala, Python, R, SQL      | Hard                   | No   | Developers and professional users                                            | [https://zeppelin.apache.org/](https://zeppelin.apache.org/)                 |
| **Baserow**         | Open source | Python, Javascript, Vue.js | Moyen                  | Yes  | Developers and professional users                                            | [https://baserow.io/](https://baserow.io/)                                   |
| **Grist**           | Open source | Python                     | Easy                   | Yes  | Businesses, non-profit organizations, governments, universities, researchers | [https://getgrist.com/](https://getgrist.com/)                               |
| **Metabase**        | Open source | Java, Clojure              | Medium                 | Yes  | Startups, businessees, non-profit organizations                              | [https://www.metabase.com/](https://www.metabase.com/)                       |
| **LockoKit**        | Open source | ...                        | Medium                 | No   | Developers and professional user                                             | [https://locokit.io/](https://locokit.io/)                                   |
| **NoCodB**          | Open source | Javascript, Node.js        | Easy                   | Yes  | Developers and professional users                                            | [https://nocodb.com/](https://nocodb.com/)                                   |
| **Gsheet**          | -           | None                       | Easy                   | Yes  | Companies, teams, freelancers, SMEs                                          | [https://www.google.com/sheets/about/](https://www.google.com/sheets/about/) |
| **Airtable**        | Commercial  | None                       | Easy                   | Yes  | Companies, teams, freelancers, SMEs                                          | [https://airtable.com/](https://airtable.com/)                               |
| **Qlikview**        | Commercial  | None                       | Medium                 | Yes  | Large corporations, financial institutions                                   | [https://www.qlik.com/us/](https://www.qlik.com/us/)                         |

---

## Benchmark features

::: {.text-micro}
Among all the solutions we have just listed, some can be compared with **Datami**'s main features.
:::

| Solution            | Open source  | Readyness | Table view | Cards view  | Map view    | Dataviz view | Edition | Moderation   | Configuration interface    | Data sources         | Backend              | Widget  | Official website                                |
|-----------------    |------------- |---------- |----------- |------------ |----------- |------------- |--------  |------------ |---------------------------   |-------------------- |--------------------- | ------  |-------------------------------------------------|
| **Datami**          | ✅           | ⭐⭐      | ✅         | ✅          | ✅         | ✅           | ✅       | ✅          | ❌ (for now)                | API ext. (Git)      | Git platforms / APIs | ✅      | [Website](https://datami.multi.coop/)          |
| **Metabase**        | ✅           | ⭐⭐      | ✅         | ❓          | ✅         | ✅           | ⚠️        | ❌          | ✅                          | SQL, connectors     | server / APIs        | ✅      | [Website](https://www.metabase.com/)           |
| **Gogocarto**       | ✅           | ⭐⭐⭐    | ✅         | ✅          | ✅         | ❌           | ✅       | ❌          | ✅                          | proper              | server / APIs        | ✅      | [Website](https://gogocarto.fr/projects)       |
| **Lizmap**          | ✅           | ⭐        | ⚠️          | ❌          | ✅         | ✅           | ✅       | ❌          | ✅                          | proper              | server / ❓          | ✅      | [Website](https://www.lizmap.com)              |
| **Umap**            | ✅           | ⭐⭐⭐    | ❌         | ❌          | ✅         | ❌           | ✅       | ❌          | ✅                          | ...                 | server / ❓          | ✅      | [Website](https://umap.openstreetmap.fr/)      |
| **Grist**           | ✅           | ⭐        | ✅         | ✅          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / APIs        | ❌      | [Website](https://getgrist.com/)               |
| **Baserow**         | ✅           | ⭐⭐      | ✅         | ✅          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / ❓          | ❌      | [Website](https://baserow.io/)                 |
| **LockoKit**        | ✅           | ⭐⭐      | ✅         | ❌          | ❌         | ❌           | ✅       | ❓          | ✅                          | proper              | server / ❓          | ❌      | [Website](https://locokit.io/)                 |
| **NoCodB**          | ✅           | ⭐        | ✅         | ❌          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / ❓          | ❌      | [Website](https://nocodb.com/)                  |
| **Apache Superset** | ✅           | ⭐⭐      | ✅         | ❌          | ❌         | ✅           | ✅       | ❓          | ✅                          | SQL                 | server / Saas        | ❓      | [Website](https://superset.apache.org/)         |
| **Datawrapper**     | 🔒           | ⭐⭐⭐    | ✅         | ❓          | ✅         | ✅           | ❌       | ❌          | ✅                          | SQL, connectors     | Saas                 | ✅      | [Website](https://www.datawrapper.de/)          |
| **Airtable**        | 🔒           | ⭐⭐⭐    | ✅         | ✅          | ❌         | ⚠️            | ✅       | ⚠️           | ✅                          | proper, connectors  | Saas                 | ✅      | [Website](https://airtable.com/)                |
| **Gsheet**          | 🔒           | ⭐⭐⭐    | ✅         | ❌          | ❌         | ✅           | ✅       | ✅          | ✅                          | proper, connectors  | Saas                 | ✅      | [Website](https://www.google.com/sheets/about/) |


::: {.text-micro .text-center}
The ❓ indicates the information is to be completed<br>
The ⚠️ that the functionality can be implemented either in a roundabout way or as a hack
:::

---

Although these solutions share some features with [Datami](https://datami.multi.coop), they can differ significantly in terms of cost, complexity and specific features

It is important to take into account the **specific needs of each project** before choosing the most suitable online data visualization and editing solution


# Technical roadmaps

---

Here is a list of different features <br>that reflect requests and needs <br>from our users to date.

::: {.text-micro}
This roadmap is to be taken as a _whish list_ of features that we consider interesting and compatible with Datami, and that we would like to be able to develop **if we manage to find the corresponding funding**.
:::

---

The features' developments are described as follow :

:::{.text-micro}
- **Milestone** : family of features
- **Features** : feature to develop
- **Priority** ( 🔴 ) : importance given to the feature by the users
- **Difficulty** ( ▪️ ) : anticipated comlexity of the feature
- **Dev. + man.** : sum of development and management (in days per person)
:::



---

## Technical roadmap 2023 - global

<br>

::: {.text-center}
<b>Classified by milestones</b>
:::

| **Milestones**                      | **Priority** | **Difficulty** | **Dev + man.** |
|-------------------------------------|--------------|----------------|----------------|
| <b>More data sources</b>            | 🔴 high      | ▪️ easy        | <b>38 days</b> |
| <b>Online widget configuration</b>  | 🔴 high      | ▪️▪️▪️ hard    | <b>26 days</b> |
| <b>Manage contribution widget</b>   | 🔴 high      | ▪️▪️▪️ hard    | <b>25 days</b> |
| <b>Protect widget with password</b> | 🟠 medium    | ▪️ easy        | <b>12 days</b> |
| <b>Better UX - data management</b>  | 🟡 low       | ▪️▪️ medium    | <b>42 days</b> |
| <b>Better UX - data interaction</b> | 🟡 low       | ▪️▪️ medium    | <b>44 days</b> |
| <b>Better UX - maps</b>             | 🟡 low       | ▪️▪️ medium    | <b>37 days</b> |
| <b>Better UI - customization</b>    | 🟠 medium    | ▪️ easy        | <b>13 days</b> |
| <b>Better UI - new views</b>        | 🟡 low       | ▪️▪️ medium    | <b>27 days</b> |
| <b>Refactoring</b>                  | 🟠 medium    | ▪️▪️▪️ hard    | <b>48 days</b> |
| <b>Tests</b>                        | 🟡 low       | ▪️▪️▪️ hard    | <b>18 days</b> |
|                                     |              | **TOTAL**      | **330 days**   |

---

## Diagram of future developments

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
Elements in <span class="text-secondary">orange</span> symbolize the main milestones to be developped as described in the roadmap above.

Elements colored in **turquoise** symbolize features already existing in Datami.
:::

::: {.column width=50% .mt-default}
- <soan class="text-secondary">A</soan> : <b>Online widget configuration</b>
- <soan class="text-secondary">B</soan> : <b>Manage contribution widget</b>
- <soan class="text-secondary">C</soan> : <b>More data sources</b>
:::

::::::

:::

::: {.no-mt .h-50}
![&nbsp;](images/roadmaps/archi-devs-roadmap-en.png)
:::

---

## Diagram of future developments (mermaid)

::: {.text-micro}
A more complete version of the previous diagram
:::

::: {.img-table}
![&nbsp;](images/diagrams/diagram-premium-01.png)
:::

---

## Technical roadmap - detailed 1/2

| **Milestones**                      | **Features**                                     | **Priority** | **Difficulty** | **Dev + man.** |
|-------------------------------------|--------------------------------------------------|--------------|----------------|----------------|
| <b>More data sources</b>            | Connect to external APIs - generic               | 🔴 high      | ▪️ easy        | <b>5 days</b>  |
| <b>More data sources</b>            | Connect to external APIs - OSM                   | 🟡 low       | ▪️ easy        | <b>5 days</b>  |
| <b>More data sources</b>            | Activity pub integration                         | 🟡 low       | ▪️▪️▪️ hard    | <b>18 days</b> |
| <b>More data sources</b>            | Connector to SourceForge                         | 🟡 low       | ▪️▪️▪️ hard    | <b>5 days</b>  |
| <b>More data sources</b>            | Connector to BitBucket                           | 🟡 low       | ▪️▪️▪️ hard    | <b>5 days</b>  |
| <b>Online widget configuration</b>  | Interactive interface + preview                  | 🔴 high      | ▪️▪️▪️ hard    | <b>20 days</b> |
| <b>Online widget configuration</b>  | Save new config to git repo                      | 🔴 high      | ▪️▪️ medium    | <b>6 days</b>  |
| <b>Manage contribution widget</b>   | Interface loading last PRs from repo             | 🔴 high      | ▪️▪️▪️ hard    | <b>15 days</b> |
| <b>Manage contribution widget</b>   | Accept / moderate PR + messages                  | 🔴 high      | ▪️ easy        | <b>10 days</b> |
| <b>Protect widget with password</b> | Protect access before showing widget             | 🟠 medium    | ▪️ easy        | <b>6 days</b>  |
| <b>Protect widget with password</b> | Special token for protected widget               | 🟠 medium    | ▪️▪️ medium    | <b>6 days</b>  |
| <b>Better UX - data management</b>  | Add a new column + update schema                 | 🟠 medium    | ▪️▪️ medium    | <b>10 days</b> |
| <b>Better UX - data management</b>  | Drag & drop CSV to widget                        | 🟡 low       | ▪️▪️ medium    | <b>10 days</b> |
| <b>Better UX - data management</b>  | Save CSV file to Git repo                        | 🟡 low       | ▪️▪️ medium    | <b>4 days</b>  |
| <b>Better UX - data management</b>  | Add / drag-drop a picture in a cell              | 🟡 low       | ▪️▪️ medium    | <b>6 days</b>  |
| <b>Better UX - data management</b>  | Cache user changes / branch until pushing       | 🟠 medium    | ▪️▪️▪️ hard    | <b>12 days</b> |
| <b>Better UX - data interaction</b> | Full screen - debug                              | 🟠 medium    | ▪️▪️ medium    | <b>6 days</b>  |
| <b>Better UX - data interaction</b> | Range filter                                     | 🟡 low       | ▪️ easy        | <b>4 days</b>  |
| <b>Better UX - data interaction</b> | Change width column                              | 🟠 medium    | ▪️▪️ medium    | <b>8 days</b>  |
| <b>Better UX - data interaction</b> | Helper at loader                                 | 🟡 low       | ▪️ easy        | <b>2 days</b>  |
| <b>Better UX - data interaction</b> | Better integration of Frictionless data packages | 🟡 low       | ▪️▪️▪️ hard    | <b>12 days</b> |
| <b>Better UX - data interaction</b> | Export as pdf                                    | 🟡 low       | ▪️▪️▪️ hard    | <b>12 days</b> |
| <b>Better UX - maps</b>             | Inject data to vector tiles on map view          | 🟡 low       | ▪️▪️ medium    | <b>8 days</b>  |

## Technical roadmap - detailed 2/2

| **Milestones**                      | **Features**                                     | **Priority** | **Difficulty** | **Dev + man.** |
|-------------------------------------|--------------------------------------------------|--------------|----------------|----------------|
| <b>Better UX - maps</b>             | Add or edit geojson objects                      | 🟡 low       | ▪️▪️▪️ hard    | <b>29 days</b> |
| <b>Better UI - customization</b>    | Custom styles / CSS / logos                      | 🟠 medium    | ▪️ easy        | <b>4 days</b>  |
| <b>Better UI - customization</b>    | Accessibility                                    | 🟠 medium    | ▪️▪️ medium    | <b>9 days</b>  |
| <b>Better UI - new views</b>        | Agenda view                                      | 🟡 low       | ▪️ easy        | <b>8 days</b>  |
| <b>Better UI - new views</b>        | Graphs view with D3js                            | 🟡 low       | ▪️▪️ medium    | <b>10 days</b> |
| <b>Better UI - new views</b>        | Simultaneous dataviz + map on same view          | 🟡 low       | ▪️▪️ medium    | <b>9 days</b>  |
| <b>Refactoring</b>                  | Put all git* requests into a package             | 🟡 low       | ▪️▪️▪️ hard    | <b>15 days</b> |
| <b>Refactoring</b>                  | Migration to Typescript                          | 🟠 medium    | ▪️▪️▪️ hard    | <b>15 days</b> |
| <b>Refactoring</b>                  | Migration to Vue3                                | 🟡 low       | ▪️▪️▪️ hard    | <b>18 days</b> |
| <b>Tests</b>                        | Add functional & unit tests                      | 🟡 low       | ▪️▪️▪️ hard    | <b>18 days</b> |

---

## Technical roadmap - global

<br>

::: {.text-center}
<b>Classified by priority / difficulty</b>
:::

::: {.table-matrix}

| **Priority / Difficulty** | ▪️▪️▪️ hard     | ▪️▪️ medium    | ▪️ easy        | **TOTAL**       |
|---------------------------|-----------------|----------------|----------------|-----------------|
| 🔴 high                   |  35 days        |  6 days        |  15 days       | <b>56 days</b>  |
| 🟠 medium                 |  27 days        |  39 days       |  10 days       | <b>76 days</b>  |
| 🟡 low                    |  132 days       |  47 days       |  19 days       | <b>198 days</b> |
| **TOTAL**                 | <b>194 days</b> | <b>92 days</b> | <b>44 days</b> | **330 days**    |

:::

# Credits

## A project by the multi cooperative

:::::::::::::: {.columns}

::: {.column width="60%"}
Our cooperative contributes to the development of **digital commons** and associated services, by bringing together a community of professionals working for a **digital of general interest**
::: 

::: {.column width="40%"}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)

::: {.text-center}
🚀 [https://multi.coop](https://multi.coop)
:::

:::

::::::::::::::

---

## Our first users

![&nbsp;](images/clients/clients.png)

---

## Our sponsors

Datami was **laureate of the Plan France Relance 2022**<br>
and has benefited from the support of the following organizations

![&nbsp;](images/sponsors/sponsors.png)

#

::: {.text-center}
**Thanks for your attention !**
:::

<br>

::::::::::::: {.columns}

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::
:::

::: {.column width="20%"}
::: {.text-micro .text-center}
[Datami](https://datami.multi.coop)

is a project led by the cooperative

[multi](https://multi.coop)
:::
:::

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)
:::
:::

:::::::::::::


::::::::::::: {.columns}

::: {.column width="30%"}
::: {.img-mini}
![&nbsp;](images/sponsors/sponsors.png)
:::
:::

:::::::::::::

::: {.text-center}
**contact@multi.coop**
:::

---

::: {.img-mini}
![&nbsp;](images/sponsors/ow2con23_borderL.png)
:::

::: {.text-center}
[Slides source](https://gitlab.com/multi-coop/datami-project/datami-slides-fr)
:::