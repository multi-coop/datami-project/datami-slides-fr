---
title: Researcher's datasets and Open Science
# ![](images/logos/logo-DATAMI-rect-colors.png)
# ref: https://www.ow2con.org/view/2023/Abstract_Community_Day#14061510
subtitle: |
  <br>
  <strong>
    A LONG, BUMPY BUT OPEN ROAD
  </strong>
  <br>
  <strong>
    TOWARD DIGITAL COMMONS
  </strong>
  <br>
  <br>
  <br>
# <span class="text-micro">
#   A customizable open source widget 
#   to visualize and edit open datasets
# </span><br>
# <span class="text-micro">
#   without any other backend than Github or Gitlab
# </span><br>
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _fullstack developer / lead dev of Datami_
  </span><br>
  <span class="text-micro text-author-details">
    _co-founder of [tech cooperative multi](https://multi.coop)_
  </span><br>
  <span class="text-micro text-author-details">
    _julien.paris@multi.coop_
  </span><br><br>
  <span class="img-cover">
    ![](images/logos/logo-MULTI-colored-063442-02-w120.png)
  </span>
  <span class="img-cover">
    ![](images/sponsors/ngi_enrichers-logo.png)
  </span>
'
date: '
  <a href="https://multi.coop?locale=en" target="_blank">
    https://multi.coop
  </a><br>
  <span class="emph-line">2024 February 19th </span> / 
  Plan by pressing <span class="text-nano">`esc`</span><br>'
title-slide-attributes:
  data-background-image: "images/open-science/concordia.png"
  # data-background-image: "images/logos/logo-DATAMI-rect-colors.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 8%, auto 5%"
  data-background-position: "right 10% top 40%, right 4% top 40%"
---

# Open the science

::: {.text-center style="font-size:2em"}
**A long, and bumpy road ...**
:::

## Introduction

::: {.text-micro}
**The Practices and Attitudes of Researchers at Concordia University with Regard to Research Data Management:**<br>
**Results of an Investigation**
:::

:::::: {.columns}

::: {.column width=30%}

::: {.text-nano}

"A survey and a series of interviews of Concordia University (Montréal) professors were undertaken in 2015-2016 to better understand the needs and attitudes of Canadian researchers in the area of data management.

The <b>majority of researchers want to better manage, preserve and share their research data</b>. They also recognise the merits of doing so.

However, certain barriers prevent them from better managing their data, namely the <b>lack of incentives, the shortage of human and technological resources, confidentiality issues and the ability to exercise a certain level of control</b> on the use of the data by others."
:::

:::

::: {.column width=70%}

:::r-stack

::: {.fragment .no-pt .no-mt .img-border .h-40 .no-caption .current-visible}
![&nbsp;](images/open-science/Guindon-01.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-40 .no-caption .fade-in-then-out}
![&nbsp;](images/open-science/Guindon-02.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-40 .no-caption .fade-in-then-out}
![&nbsp;](images/open-science/Guindon-03.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-40 .no-caption .fade-in-then-out}
![&nbsp;](images/open-science/Guindon-04.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-40 .no-caption .fade-in-then-out}
![&nbsp;](images/open-science/Guindon-05.png)
:::

:::

:::

::::::


::: {.text-nano}
*<b>Dennie, D. & Guindon, A. (2017)</b>. Résultats d’une enquête sur les pratiques et attitudes des chercheurs de l’Université Concordia en matière de gestion des données de recherche. Documentation et bibliothèques, 63(4), 59–72. doi:10.7202/1042311ar*
:::

---

## Challenge #1

### **Structure** and **enrich** your datasets

---

:::::::::::::: {.columns}

::: {.column width="45%"}
A major step to **make datasets more discoverable and reusable** is to specify and document its schema, metadata, sources, methodology...
This step could be considered quite time-consuming and technical in nature.

Another step before publishing datasets shall be to enrich them by **cross-refencing them with third party open datasets** to insure their interoperability.
:::

::: {.column width="55%"}
<br>
<br>
![&nbsp;](images/misc/data-package/frictionless-tiles.png)
:::

::::::::::::::

---

## Challenge #2

### Make datasets **intelligible**

---

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/messy-excel-02.png)
:::
:::

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ODF-map-all.png)
:::
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
A **raw** data
:::
:::

::: {.column width="50%"}
::: {.text-center}
The **same** dataset
<br>
for open publication
:::
:::
::::::::::::::

---

## Challenge #3

### Make **updating** and **contributing** easier

---

:::::::::::::: {.columns}

::: {.column width="40%"}
Handling data is a matter of habits, facilitating their manipulation for the greatest number means that you have to **adapt to the habits** of the greatest number

::: {.text-micro}
To allow everyone to easily contribute to the data, the view in the form of a table is still the most commonly adopted today.
:::

:::

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/table-view-01.png)
:::
:::

::::::::::::::

---

## Challenge #4

### Make all of the above **with limited resources**

---

Publishing data of general interest (research data in particular) while publishing research papers, in order to highlight them on a website, or calling on your community to put them up to date, could turn out to be complicated :

::: {.text-center}
**Lack of resources, lack of skills, lack of time...**
:::

:::::::::::::: {.columns}
::: {.column width="30%"}
:::fragment
::: {.text-center}
<i class="ri-tools-fill"></i>
:::
::: {.text-justify}

The cost of the usual technical solutions for sharing / viewing / contributing to data is often explained by the technical **complexity** of these functionalities
:::
:::
:::
::: {.column width="30%"}
:::fragment
::: {.text-center}
<i class="ri-server-fill"></i>
:::
::: {.text-justify style="padding: 0 1.5em 0 1.5em"}

Another cost is related to the need to set up dedicated **servers** in _backend_ or very specific configurations
:::
:::
:::
::: {.column width="30%"}
:::fragment
::: {.text-center}
<i class="ri-hourglass-2-fill"></i>
:::
::: {.text-justify}

The **maintenance** of applications or servers often generates significant costs
:::
:::
:::
::::::::::::::


# The multi cooperative

:::::::::::::: {.columns}

::: {.column width="60%"}
Our cooperative contributes to the development of **digital commons** and associated services, by bringing together a community of professionals working for a **digital of general interest**
:::

::: {.column width="40%"}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)

::: {.text-center}
🚀 [https://multi.coop](https://multi.coop)
:::

:::

::::::::::::::

## Our clients

![&nbsp;](images/clients/multi-clients.png)

---

## Our methods

#### **Project management**

::: {.text-micro}
At multi we chose to engage in <b>agile projects management</b>, while mobilizing our core technical skills transversaly.
:::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column .text-center width=20%}
<b>⭐️ Product owner</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data science</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data engineering</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Web development</b>

:::

::::::
:::

<hr>

<br>

#### **Data auditing & developments**

::: {.text-micro}
<b>Every project we engage in has a data dimension</b>. As our clients are mainly public organisations the datasets we help to build or valorize need to be prepared for open data publishing.
:::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column .text-center width=20%}
<b>⭐️ Data management</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data preprocessing</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data enrichment</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data publishing</b>

:::
::: {.column .text-center width=20%}
<b>⭐️ Data valorisation</b>

:::
::::::
:::

<hr>

---

## Data lifecycle guidelines

::::::{.columns}

:::{.column width="35%"}
Since their creation [Datactivist](https://datactivist.coop/en/) shared a lot of guidelines we are glad to adopt in the projects and developments we engage in.

<br>

::: {.text-micro}
Datactivist is a fellow French cooperative specialized in data consulting.
:::

:::{.img-nano}
![&nbsp;](images/misc/datactivist/Logo_Datactivist.png)
:::

:::

:::{.column width=75%}

:::{.no-mt .no-caption .h-60}
![&nbsp;](images/misc/datactivist/data-lifecycle.png)
:::


:::

:::::::

---

## Our team (aka the Multeam)

::: {.no-mt .no-pt .h-50}
![&nbsp;](images/team/multeam.png)
:::

# Some references & examples

<br>

:::{.img-mini}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)
:::

:::{.text-center .text-micro}
All our projects are **open-sourced**
:::

---

## DBnomics

:::::: {.columns}

::: {.column width=30%}

::: {.text-nano}
[DBnomics](https://db.nomics.world/) is one of the largest economic databases in the world, aggregating hundreds of millions of time series from dozens of sources (INSEE, Eurostat, IMF, WTO, WB... i.e. 50+ providers) and making them available via a single API.

DBnomics is a project led by the Macro Observatory of the Center for Economic Research and its Applications (CEPREMAP), the Banque de France and France Stratégie are partners in the project and the software development is ensured by Jailbreak. DBnomics is also a winner of the Future Investment Program (PIA).
:::

:::

::: {.column width=70%}

:::r-stack

::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .current-visible}
![&nbsp;](images/references/dbnomics/01.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/dbnomics/02.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/dbnomics/03.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/dbnomics/04.png)
:::

:::

:::

::::::

<hr>

:::::: {.columns}

::: {.column width=33%}

**Problem**

::: {.text-nano}
- Agregate multiple data sources
- Regular data updates
:::

:::

::: {.column width=33%}

**Solution**

::: {.text-nano}
- Dedicated platform
- Public and protected API
- Connectors & transformers
:::

:::

::: {.column width=33%}

**Constraints**

::: {.text-nano}
- Connectors maintenance
- Data complexity
:::

:::

::::::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column width=20%}
<b>Data management</b>

⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data preprocessing</b>

⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data enrichment</b>

⭐️⭐️
:::
::: {.column width=20%}
<b>Data publishing</b>

⭐️⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data valorisation</b>

⭐️⭐️⭐️
:::
::::::
:::

---

## Validata

:::::: {.columns}

::: {.column width=30%}

::: {.text-nano}
[Validata](https://validata.fr/) aims to offer a platform for validating open data sources.

It is aimed at French communities wishing to validate the quality and interoperability of the data they publish using an external tool. It also allows data warehouse managers to qualify the integrity of the data they wish to use before importing it into a multi-source database.

This platform, developed in open source, is accessible to any public or private actor wishing to publish or operate open public data.
:::

:::

::: {.column width=70%}

:::r-stack

::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .current-visible}
![&nbsp;](images/references/validata/validata-01.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/validata/validata-02.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/validata/01.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/validata/02.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/validata/03.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/validata/04.png)
:::

:::

:::

::::::

<hr>

:::::: {.columns}

::: {.column width=33%}

**Problem**

::: {.text-nano}
- Compare source datasets to schemas
- Schema sources could be external
- Sovereign deployment
:::

:::

::: {.column width=33%}

**Solution**

::: {.text-nano}
- Dedicated platform
- Public and protected API
- Connectors & transformers
:::

:::

::: {.column width=33%}

**Constraints**

::: {.text-nano}
- Connectors maintenance
- Data complexity
:::

:::

::::::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column width=20%}
<b>Data management</b>

⭐️
:::
::: {.column width=20%}
<b>Data preprocessing</b>

⭐️⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data enrichment</b>

⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data publishing</b>

⭐️⭐️
:::
::: {.column width=20%}
<b>Data valorisation</b>

⭐️
:::
::::::
:::

---

## Ma cantine

:::::: {.columns}

::: {.column width=30%}

::: {.text-nano}
[Ma Cantine](https://ma-cantine.agriculture.gouv.fr/) aims to increase the share of sustainable products in collective catering. A platform is offered to canteen managers and citizens. It serves as display windows for canteens as well as a management tool. For the administration, it is a tool for managing certain measures of the EGALim law. The project is a state start-up in which around ten people work.
:::

:::

::: {.column width=70%}

:::r-stack

::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .current-visible}
![&nbsp;](images/references/ma-cantine/landing.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/ma-cantine/app.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/ma-cantine/home.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/ma-cantine/tools.png)
:::
::: {.fragment .no-pt .no-mt .img-border .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/references/ma-cantine/registre.png)
:::

:::

:::

::::::

<hr>

:::::: {.columns}

::: {.column width=33%}

**Problem**

::: {.text-nano}
- National service for ultra-local entities
- Datasets quality
- Opening data as a project within the project
:::

:::

::: {.column width=33%}

**Solution**

::: {.text-nano}
- Dedicated platform
- Public and protected API
- Connectors & transformers
:::

:::

::: {.column width=33%}

**Constraints**

::: {.text-nano}
- Project lead by public administration
- Number of contributors
:::

:::

::::::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column width=20%}
<b>Data management</b>

⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data preprocessing</b>

⭐️⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data enrichment</b>

⭐️
:::
::: {.column width=20%}
<b>Data publishing</b>

⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data valorisation</b>

⭐️⭐️⭐️
:::
::::::
:::

---

## Datami

:::::: {.columns}

::: {.column width=30%}

::: {.text-nano}
[Datami](https://datami.multi.coop) is a generic widget-type tool, allowing you to view and edit data hosted on Github or Gitlab, or even on a mediawiki.

Datami has different features aimed at simplifying data editing and enrichment:

Visualization in the form of a table, files, cartography or data visualizations / 
Visualization of the differences between the original version and the version edited by the user / 
Search by filters or in full text / 
Import/export data / 
Apply a data schema to spreadsheet data / 
Easy copy/paste the html block of the widget / 
Simplified process for creating a merge request...
:::

:::

::: {.column width=70%}

:::r-stack

::: {.fragment .no-pt .no-mt .h-35 .no-caption .current-visible}
![&nbsp;](images/datami-views-panel-05.png)
:::
::: {.fragment .no-pt .no-mt .h-35 .no-caption .fade-in-then-out}
![&nbsp;](images/datami-widgets-clients.png)
:::
::: {.fragment .no-pt .no-mt .h-35 .img-border .no-caption .fade-in-then-out}
![&nbsp;](images/screenshots/datami-website-en.png)
:::
::: {.fragment .no-pt .no-mt .h-35 .img-border .no-caption .fade-in}
![&nbsp;](images/screenshots/datami-doc-website.png)
:::

:::

:::

::::::

<hr>

:::::: {.columns}

::: {.column width=33%}

**Problem**

::: {.text-nano}
- General public as target
- For structures with low level of resources
- Variety of datasets' models
:::

:::

::: {.column width=33%}

**Solution**

::: {.text-nano}
- Customizable widget
- Git-based versionning
- "Database-less"
:::

:::

::: {.column width=33%}

**Constraints**

::: {.text-nano}
- Manual data management & pre-processing
- Complex to configure
:::

:::

::::::

<hr>

::: {.text-micro}
:::::: {.columns}
::: {.column width=20%}
<b>Data management</b>

⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data preprocessing</b>

⭐️⭐️
:::
::: {.column width=20%}
<b>Data enrichment</b>

⭐️⭐️
:::
::: {.column width=20%}
<b>Data publishing</b>

⭐️⭐️⭐️⭐️
:::
::: {.column width=20%}
<b>Data valorisation</b>

⭐️⭐️⭐️⭐️⭐️
:::
::::::
:::
# Open science with **Datami** toolbox

<br>

::: {.img-mini}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::

::: {.text-center}
**https://datami.multi.coop**
:::

---

## Simplifying datasets preparation before open science publishing

::: {.h-45 .no-mt }
![&nbsp;](images/diagrams/diagram-open-science-01.png)
:::

<hr>

:::::: {.columns .text-center .text-micro}

::: {.column width=25%}
<i class="ri-database-2-line"></i>

<b>Control over <br>data storage & open level</b>
:::
::: {.column width=25%}
<i class="ri-file-search-line"></i>

<b>Interoperability <br>with open standards</b>
:::
::: {.column width=25%}
<i class="ri-window-fill"></i>

<b>Interfaces <br>for all audiences</b>
:::
::: {.column width=25%}
<i class="ri-message-2-line"></i>

<b>Versionning <br> and open contributions</b>
:::

::::::


---

## You control your data <br>Datami makes it understandable

Datami's architecture is designed as an **interface between the citizen and your database** in order to facilitate the link **between citizens and open data producers**

::: {.no-mt .h-30}
![](images/roadmaps/archi-devs-en.png)
:::

---

## Structure your data <br>to make it interoperable

:::::::::::::: {.columns}

::: {.column width="40%"}
Your data may be associated with files to structure it, such as **data schema files**

By associating your data set with a data schema respecting international **standards** you ensure that they can be correctly reused and improved

:::

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-schemas-01.png)
:::

::::::::::::::

---

**Datami doesn't store your data** : your data stays within the tool of your choice (Github, Gitlab, database or API).

Using Datami is more **affordable** because you don't have to install nor maintain a dedicated _backend_. 

---

## View your data from all angles

:::::::::::::: {.columns}

::: {.column width="40%"}
Your data can be viewed as **spreadsheets**, **maps**, miniature or detailed **lists of records**, or **graphs**

All views are interactive and customizable to highlight all the specifics of your data

:::

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-views-01.png)
:::

::::::::::::::

---

## An affordable and ethical solution<br> for structures with limited resources

Datami provides a way 
for data producers from small-sized or local organisations
to **keep control** over their data, 
**engage** with their communities,
<br>
**without subscription nor infrastucture or maintaining costs.**

:::::: {.columns}

::: {.column width=30%}

::: {.text-micro .text-justify}
For a small structure
producing data of general interest 
it can be complicated to allocate resources 
to highlight datasets on your website 
or to call on your community to update them.

The cost of sharing, viewing, or contributing to data 
is often explained by the 
<b>
technical complexity
</b>
of such features,
the necessity to set up 
<b>
dedicated servers
</b>
as _backend_,
or costs of their 
<b>
maintenance
</b>.

<!-- Valorizing your open data and let your communities
contribute should not be limited by the amount of 
technical or financiary resources you are disposing of. -->

:::

:::

::: {.column width=70%}

::: {.img-medium .img-no-margin .text-center}
![](images/diagrams/diagram-benchmark-01.png)
:::
:::

::::::

::: {.text-micro .text-no-mt}
Datami's original architecture makes possible to 
<b>
get rid of a large part of the _backend_ server costs
</b>
<br>
while allowing customization dataset by dataset, 
as open contributions and fine control over the data.
:::

---

## A free and multi-purpose software

![&nbsp;](images/screenshots/datami-feats-matrix-en.png)

---

## The Datami offer by the multi cooperative

**We aim to propose different services aroud Datami**

<br>

::: {.text-micro}

:::::::::::::: {.columns}

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-setup.png)

::: {.text-center}
**Datami setup**
:::

Multi.coop can provide an economic package
of a few days only for support in setting up Datami
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-pack.png)

::: {.text-center}
**Datami as free software** 
:::

Use Datami as it is based on the documentation, 
you are free!
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-custom_dev.png)

::: {.text-center}
**Custom developments** 
:::

Multi.coop can help you responding to you needs
using Datami while adding new features 
:::

::: {.column width="25%"}

![&nbsp;](images/offer/datami-logo-setup-interface.png)

::: {.text-center}
**Online configuration** \*
:::

Use a simple interface to setup yourself the 
widget you need for your datasets.

_\* This offer is currently under construction_

:::

::::::::::::::

::: 

::: {.text-micro}
In order to make Datami as accessible and affordable as possible, 
our principle is to 
<b>
mutualize design and development costs.
</b>
<br>
All developments - even minimal ones - contributing to improve Datami 
eventually benefit all users.
:::


---

## Integrate Datami into your <br>site and your partners

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami uses **_widgets_** technology: turnkey and customizable modules that you can add to an existing site

Datami _widgets_ are **_open source_**, simple to copy and paste, without subscription, **without additional cost**

:::

::: {.column width="60%"}
![&nbsp;](images/screenshots/widget-copy-01.png)
:::

::::::::::::::

---

## Empower your teams and audiences <br>to improve data

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami includes a **contribution and moderation system**

Based on the Git language, Datami _widgets_ allow you to keep control of your data and manage contributions, **without creating an account** to suggest improvements

:::

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-contribution-01.png)
:::
:::

::::::::::::::

---

## Drive your field actions through data

:::::::::::::: {.columns}

::: {.column width="40%"}
**Exploring your data** allows you to better understand and manage your actions in the field

Datami allows you to set up personalized interactive data-visualizations, in order to make your data easily explorable

:::

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-pilotage-01.png)
:::

::::::::::::::

---

Datami is fully auditable and reusable **free licensed software**

To discover and learn how to use Datami you can:

- Go to the [official Datami website](https://datami.multi.coop);
- Directly access the [Datami source code](https://gitlab.com/multi-coop/datami-project/datami);
- Consult the [technical documentation](https://datami-docs.multi.coop) and online tutorials;
- Call on the [Multi cooperative](https://multi.coop) for advice in data science and for training.

---

## Our first users

**Since late 2022 we implemented Datami for a dozen organisations,**<br>**some public and other private**

::: {.text-micro}

Each organisation may at the time have had one or several datasets to expose and share.
<br>
The needs we covered were related to data-visualisation, cartography, embeddibility, reproductibility... 
<br>
<b>
The expression of needs along the way participated to build our roadmap,
</b>
feature demand by feature demand.

:::::: {.columns}

::: {.column width=35%}

Current use cases include datasets focusing on a variety of topics of public interest :

- Indicators about places providing public digital services (France services) ;
- Indicator on digital parental fragility ;
- Open data producers ;
- Third places characteristics ;
- Alternative mobility projects ...

More examples on the [Datami website](https://datami.multi.coop/examples?locale=en)

:::

::: {.column .img-no-margin  width=65%}
![](images/clients/clients.png)
:::

::::::

:::


---

## Analytics

**We use the open souce solution Matomo as our main analytics platform.**

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
The metrics are visible for everyone 
on this [link](https://multi.matomo.cloud/index.php?module=CoreHome&action=index&idSite=1&period=range&date=previous30#?period=range&date=2022-08-28,2023-03-25&category=Dashboard_Dashboard&subcategory=1&idSite=1)
and are set to preserve the privacy of Datami's users.

Our metrics shows 
<b>
a slow but continuous trend of growth in the using of Datami
</b>
amongst our first users.
:::

::: {.column width=50%}

We also observed the peaks in our metrics may be related
with events - either organized by multi or by stakeholders -
boosting up the visibility of datasets exposed with Datami
towards their target audiences.
:::

::::::

:::

::: {.text-center .img-pdf}
![](images/matomo/matomo-datami_widget-04.png)
:::

---


## The official website

:::::::::::::: {.columns}

::: {.column width="40%"}
On our **official website** you will find **videos** of presentation, **examples**, as well as our **blog** space

The site is translated into French and English

:::

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-website-en.png)
:::
::: {.text-center}
🚀 [datami.multi.coop](https://datami.multi.coop)
:::
:::

::::::::::::::

---

## Datami's sponsors in 2022-2023

<br>

:::{.text-micro}
Datami project was **laureate of the "Plan France Relance 2022"**
and has benefited from the support of the following organizations
![&nbsp;](images/sponsors/sponsors.png)

Julien Paris (Datami's product owner and lead developer) is **laureate of the 1st NGI Enrichers** european fellowship program.
He will stay for 6 months in Montreal (Canada) in 2024, in collaboration with the Concordia University and Mitacs.

:::{.text-center}
:::{.columns}
:::{.column width="33%"}
![&nbsp;](images/sponsors/ngi_enrichers-logo.png)
:::
:::{.column width="33%"}
![&nbsp;](images/sponsors/logo-concordia-university-montreal.png)
:::
:::{.column width="33%"}
![&nbsp;](images/sponsors/mitacs-logo.png)
:::
:::
:::
:::

---

## The source code

:::::::::::::: {.columns}

::: {.column width="40%"}
Our **source code is on Gitlab** under _open source_ license

Do not hesitate to report _bugs_ to us by suggesting [_issues_](https://gitlab.com/multi-coop/datami-project/datami/-/issues), or to give ideas for new features on our [ _roadmap_](https://gitlab.com/multi-coop/datami-project/datami/-/boards/4736577)

A [mirror repo](https://github.com/multi-coop/datami) is also automatically synced to Github

Don't forget to leave a little ⭐️ if you like the project!
:::

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-gitlab.png)
:::
::: {.text-center}
💻 [Repo Gitlab](https://gitlab.com/multi-coop/datami-project/datami)

![](https://img.shields.io/badge/dynamic/json?color=turquoise&label=gitlab%20stars%20%E2%98%85&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39268471)

:::
:::

::::::::::::::

---

## Existing open source audits

**We were audited in late 2022
by the cabinet Smile**
<br>
on the open source dimensions of the Datami project. 


:::::: {.columns}

::: {.column width=30%}

::: {.text-micro}
Smile helped us formalising different recommandations 
about 
<b>
Datami's open source strategies, its functional roadmap, or even about the project's governance.
</b>

::: {.text-center}
![](images/audits/smile-logo.png)
:::

All the documents related to this audit can be requested 
by contacting us at : 
[contact@multi.coop](mailto:contact@multi.coop)
:::

:::

::: {.column width=70%}
![&nbsp;](images/audits/smile-01.png)
:::

::::::


---

## The technical documentation site

:::::::::::::: {.columns}

::: {.column width="40%"}
Also visit our dedicated **documentation site**

The site is translated into French and English

You will find different sections there: technical principles, tutorials, examples, description of the different widgets and their configuration elements...

:::

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-doc-website.png)
:::
::: {.text-center}
🔧 [datami-docs.multi.coop](https://datami-docs.multi.coop)
:::
:::

::::::::::::::

---

## The _stack_

:::::::::::::: {.columns}

::: {.column width="40%"}
The technical _stack_ is **entirely composed of _open source_ libraries**
:::

::: {.column width="60%"}
- [`Vue.js (v2.x)`](https://v2.vuejs.org/v2/guide)
- [`VueX`](https://vuex.vuejs.org/)
- [`vue-custom-element`](https://github.com/karol-f/vue-custom-element)
- [`gray-matter`](https://www.npmjs.com/package/gray-matter)
- [`Showdown`](https://www.npmjs.com/package/showdown) + [`showdown-table extension`](https://github.com/showdownjs/table-extension#readme)
- [`Bulma`](https://bulma.io/) + [`Buefy`](https://buefy.org/)
- [`Material Design`](https://materialdesignicons.com/)
- [`Fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
- [`JSDiff`](https://github.com/kpdecker/jsdiff)
- [`Diff2html`](https://www.npmjs.com/package/diff2html)
- [`MapLibre`](https://maplibre.org)
- [`ApexCharts`](https://apexcharts.com)
:::

::::::::::::::

# Datami PRO (teasing)

![&nbsp;](images/datami-pro/intro.png)

---

## Why Datami PRO ?

Datami PRO will be the platform to simplify the opening datasets for individual or organisations, without the need for any tech skills.

- **Autonomous data management**

- **Step-by-step opening process**

- **Upload structured & enriched datasets to forges** (Github, Gitlab...)

- **Autonomous configuration of Datami widgets**

- **Plans for every need and type of organisation**

---

## First interfaces

:::r-stack

::: {.fragment .no-mt .h-75 .img-border .current-visible}
![Landing page](images/datami-pro/datamipro-home.png)

:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Dataset edition](images/datami-pro/datamipro-dataset.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Dataset's columns settings (basic schema)](images/datami-pro/datamipro-dataset-settings.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Dataset's columns settings (schema details)](images/datami-pro/datamipro-dataset-settings-dialog.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Dataset's metadata edition](images/datami-pro/datamipro-dataset-settings-metadata.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Dataset converted into a Datami widget](images/datami-pro/datamipro-widget.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in-then-out}
![Passwordless authentication](images/datami-pro/datamipro-login.png)
:::

::: {.fragment .no-mt .h-75 .img-border .fade-in}
![Datami PRO plans](images/datami-pro/datamipro-plans.png)
:::

:::

---

## Diagram of developments

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
Elements in <span class="text-secondary">orange</span> symbolize the main milestones for Datami PRO roadmap.

Elements colored in **turquoise** symbolize features already existing in Datami.
:::

::: {.column width=50% .mt-default}
- <soan class="text-secondary">A</soan> : <b>Online widget configuration</b>
- <soan class="text-secondary">B</soan> : <b>Manage contribution widget</b>
- <soan class="text-secondary">C</soan> : <b>More data sources</b>
:::

::::::

:::

::: {.no-mt .h-50}
![&nbsp;](images/roadmaps/archi-devs-roadmap-en.png)
:::


# Benchmark

---

::: {.text-center}
There are several data visualization and editing solutions <br>sharing similarities with [Datami](https://datami.multi.coop)

Here are some of the most popular solutions

:::fragment
**Those benchmarks are given for information only**

Do not hesitate to let us know by writing to<br>`contact@multi.coop` <br>if you wish to make any corrections or additions
:::

:::

---

### Datavisualisation tools


| Solution               | Solution type    | Langages                            | Difficulty            | Saas    | Official website                                                 |
| ------------------     | ---------------- | ----------------------------------- | --------------------  | ------- | ---------------------------------------------------------------- |
| **Gogocarto**          | Open source      | Custom                              | Easy                  | Yes    | [https://gogocarto.fr/projects](https://gogocarto.fr/projects)   |
| **Umap**               | Open source      | Custom                              | Facile                | Yes    | [https://umap.openstreetmap.fr/](https://umap.openstreetmap.fr/) |
| **Lizmap**             | Open source      | Propre langage de requête           | Medium                | Yes    | [https://www.lizmap.com](https://www.lizmap.com)                 |
| **Apache Superset**    | Open source      | SQL                                 | Medium                | Yes    | [https://superset.apache.org/](https://superset.apache.org/)     |
| **Apache Zeppelin**    | Open source      | Several programmation languages     | Hard                  | No     | [https://zeppelin.apache.org/](https://zeppelin.apache.org/)     |
| **BIRT**               | Open source      | Java, JavaScript                    | Hard                  | No     | [https://www.eclipse.org/birt/](https://www.eclipse.org/birt/)   |
| **FineReport**         | Open source      | Java, JavaScript                    | Medium                | No     | [https://www.finereport.com/en](https://www.finereport.com/en)   |
| **Grafana**            | Open source      | Custom request language             | Hard                  | No     | [https://grafana.com/](https://grafana.com/)                     |
| **Metabase**           | Open source      | SQL                                 | Easy                  | Yes    | [https://www.metabase.com/](https://www.metabase.com/)           |
| **Redash**             | Open source      | SQL                                 | Medium                | No     | [https://redash.io/](https://redash.io/)                         |
| **Datasette**          | Open source      | SQL                                 | Medium                | No     | [https://datasette.io/](https://datasette.io/)                   |
| **LightDash**          | Open source      | Dbt                                 | Medium                | Yes    | [https://www.lightdash.com/](https://www.lightdash.com/)         |
| **Google Data Studio** | Free (but not open) | SQL                              | Easy                  | Yes    | [https://datastudio.google.com/](https://datastudio.google.com/) |
| **Datawrapper**        | Commercial       | API, CSV, GSheet                    | Easy                  | Yes    | [https://www.datawrapper.de/](https://www.datawrapper.de/)       |
| **Google Looker**      | Commercial       | LookML                              | Hard                  | Yes    | [https://looker.com/](https://looker.com/)                       |
| **Microsoft Power BI** | Commercial       | DAX et M                            | Medium                | Yes    | [https://powerbi.microsoft.com/](https://powerbi.microsoft.com/) |
| **QlikView**           | Commercial       | Custom script language              | Hard                  | Yes    | [https://www.qlik.com/](https://www.qlik.com/)                   |
| **Tableau**            | Commercial       | Custom                              | Medium                | Yes    | [https://www.tableau.com/](https://www.tableau.com/)             |

---

### Online editing tools

| Solution            | Solution type | Langages                 | Difficulty             | Saas | Targeted public                                                              | Official webiste                                                             |
|---------------------|-------------|----------------------------|----------------------  |------|--------------------------------------------------------------------------    |------------------------------------------------------------------------------|
| **Apache Zeppelin** | Open source | Scala, Python, R, SQL      | Hard                   | No   | Developers and professional users                                            | [https://zeppelin.apache.org/](https://zeppelin.apache.org/)                 |
| **Baserow**         | Open source | Python, Javascript, Vue.js | Moyen                  | Yes  | Developers and professional users                                            | [https://baserow.io/](https://baserow.io/)                                   |
| **Grist**           | Open source | Python                     | Easy                   | Yes  | Businesses, non-profit organizations, governments, universities, researchers | [https://getgrist.com/](https://getgrist.com/)                               |
| **Metabase**        | Open source | Java, Clojure              | Medium                 | Yes  | Startups, businessees, non-profit organizations                              | [https://www.metabase.com/](https://www.metabase.com/)                       |
| **LockoKit**        | Open source | ...                        | Medium                 | No   | Developers and professional user                                             | [https://locokit.io/](https://locokit.io/)                                   |
| **NoCodB**          | Open source | Javascript, Node.js        | Easy                   | Yes  | Developers and professional users                                            | [https://nocodb.com/](https://nocodb.com/)                                   |
| **Gsheet**          | -           | None                       | Easy                   | Yes  | Companies, teams, freelancers, SMEs                                          | [https://www.google.com/sheets/about/](https://www.google.com/sheets/about/) |
| **Airtable**        | Commercial  | None                       | Easy                   | Yes  | Companies, teams, freelancers, SMEs                                          | [https://airtable.com/](https://airtable.com/)                               |
| **Qlikview**        | Commercial  | None                       | Medium                 | Yes  | Large corporations, financial institutions                                   | [https://www.qlik.com/us/](https://www.qlik.com/us/)                         |

---

## Benchmark features

::: {.text-micro}
Among all the solutions we have just listed, some can be compared with **Datami**'s main features.
:::

| Solution            | Open source  | Readyness | Table view | Cards view  | Map view    | Dataviz view | Edition | Moderation   | Configuration interface    | Data sources         | Backend              | Widget  | Official website                                |
|-----------------    |------------- |---------- |----------- |------------ |----------- |------------- |--------  |------------ |---------------------------   |-------------------- |--------------------- | ------  |-------------------------------------------------|
| **Datami**          | ✅           | ⭐⭐      | ✅         | ✅          | ✅         | ✅           | ✅       | ✅          | ❌ (for now)                | API ext. (Git)      | Git platforms / APIs | ✅      | [Website](https://datami.multi.coop/)          |
| **Metabase**        | ✅           | ⭐⭐      | ✅         | ❓          | ✅         | ✅           | ⚠️        | ❌          | ✅                          | SQL, connectors     | server / APIs        | ✅      | [Website](https://www.metabase.com/)           |
| **Gogocarto**       | ✅           | ⭐⭐⭐    | ✅         | ✅          | ✅         | ❌           | ✅       | ❌          | ✅                          | proper              | server / APIs        | ✅      | [Website](https://gogocarto.fr/projects)       |
| **Lizmap**          | ✅           | ⭐        | ⚠️          | ❌          | ✅         | ✅           | ✅       | ❌          | ✅                          | proper              | server / ❓          | ✅      | [Website](https://www.lizmap.com)              |
| **Koumoul**         | ✅           | ⭐⭐      | ✅         | ❓          | ✅         | ✅           | ⚠️        | ❌          | ❓                          | ...                 | server / ❓          | ❓      | [Website](https://koumoul.com/)      |
| **Umap**            | ✅           | ⭐⭐⭐    | ❌         | ❌          | ✅         | ❌           | ✅       | ❌          | ✅                          | ...                 | server / ❓          | ✅      | [Website](https://umap.openstreetmap.fr/)      |
| **Grist**           | ✅           | ⭐        | ✅         | ✅          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / APIs        | ❌      | [Website](https://getgrist.com/)               |
| **Baserow**         | ✅           | ⭐⭐      | ✅         | ✅          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / ❓          | ❌      | [Website](https://baserow.io/)                 |
| **LockoKit**        | ✅           | ⭐⭐      | ✅         | ❌          | ❌         | ❌           | ✅       | ❓          | ✅                          | proper              | server / ❓          | ❌      | [Website](https://locokit.io/)                 |
| **NoCodB**          | ✅           | ⭐        | ✅         | ❌          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / ❓          | ❌      | [Website](https://nocodb.com/)                  |
| **Apache Superset** | ✅           | ⭐⭐      | ✅         | ❌          | ❌         | ✅           | ✅       | ❓          | ✅                          | SQL                 | server / Saas        | ❓      | [Website](https://superset.apache.org/)         |
| **Datawrapper**     | 🔒           | ⭐⭐⭐    | ✅         | ❓          | ✅         | ✅           | ❌       | ❌          | ✅                          | SQL, connectors     | Saas                 | ✅      | [Website](https://www.datawrapper.de/)          |
| **Airtable**        | 🔒           | ⭐⭐⭐    | ✅         | ✅          | ❌         | ⚠️            | ✅       | ⚠️           | ✅                          | proper, connectors  | Saas                 | ✅      | [Website](https://airtable.com/)                |
| **Gsheet**          | 🔒           | ⭐⭐⭐    | ✅         | ❌          | ❌         | ✅           | ✅       | ✅          | ✅                          | proper, connectors  | Saas                 | ✅      | [Website](https://www.google.com/sheets/about/) |


::: {.text-micro .text-center}
The ❓ indicates the information is to be completed<br>
The ⚠️ that the functionality can be implemented either in a roundabout way or as a hack
:::

---

Although these solutions share some features with [Datami](https://datami.multi.coop), they can differ significantly in terms of cost, complexity and specific features

It is important to take into account the **specific needs of each project** before choosing the most suitable online data visualization and editing solution

# Openings & propositions

<i class="ri-message-2-line"></i>
**Labs / test**

::: {.text-micro}
We can <b>accompany researchers/labs</b> in publishing their datasets, according to their specific needs.
:::

<i class="ri-survey-line"></i>
**Grants**

::: {.text-micro}
With the university's support can build a <b>proposal for international grants</b> (
[NGI Search](https://www.ngisearch.eu/view/Main/OpenCalls), 
[NGI Sargasso](https://ngisargasso.eu/)
, ...) to develop a platform helping researchers publishing their data for open science. 
:::

<i class="ri-login-circle-line"></i>
**Datami PRO early adopters plan**

::: {.text-micro}
We propose to the university and to researchers <b>several early plans</b> for Datami PRO.
:::

<i class="ri-question-answer-line"></i>
**Open discussion**

::: {.text-micro}
<b>What is your practice and/or your constraints</b> as researcher or bibliotarian when it comes to open science ?
:::

#

::: {.text-center}
**Thanks for your attention !**
:::

<br>

::::::::::::: {.columns}

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::
:::

::: {.column width="20%"}
::: {.text-micro .text-center}
[Datami](https://datami.multi.coop)

is a project led by the cooperative

[multi](https://multi.coop)
:::
:::

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)
:::
:::

:::::::::::::


::::::::::::: {.columns}

::: {.column width="30%"}
::: {.img-mini  .no-mt .no-caption}
![&nbsp;](images/sponsors/sponsors.png)
:::
::: {.img-mini .no-mt }
![&nbsp;](images/sponsors/ngi_enrichers-logo.png)
:::
:::

:::::::::::::

::: {.text-center}
**contact@multi.coop**
:::

---

::: {.text-center}
[Slides url](https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-en-open-science.html#/title-slide)
:::

::: {.text-center}
[Slides source](https://gitlab.com/multi-coop/datami-project/datami-slides-fr)
:::