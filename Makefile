.PHONY: all, concat_md, render_html, render_pdf, gitlab_pages, setup_gitlab_pages

IN ?= presentation-fr.md
OUT ?= $(basename $(IN))

# $(info $$CI_PROJECT_NAME = $(CI_PROJECT_NAME))

# all: render_html

# # OR to update many presentations at once:

all:
	@make concat_md
	@make render_html IN=presentation-fr.md
	@make render_html IN=presentation-en.md
	@make render_html IN=presentation-en-ow2.md
	@make render_html IN=retroplanning-all.md
	@make render_html IN=ngi-search.md
	@make render_html IN=ngi-entrust.md
	@make render_html IN=test-fr.md

concat_md:
# OPEN SCIENCE 
	@cat \
		texts/en/open-science/open-science-01-head.md \
		texts/en/open-science/open-science-01a-problematic.md \
		texts/en/open-science/open-science-02a-multi.md \
		texts/en/open-science/open-science-02b-references.md \
		texts/en/open-science/open-science-03a-datami.md \
		texts/en/open-science/open-science-04a-datami-pro.md \
		texts/en/open-science/open-science-04b-benchmark.md \
		texts/en/open-science/open-science-05-opening.md \
		texts/en/open-science/open-science-99-credits.md \
		> presentation-en-open-science.md
# OW2 
	@cat \
		texts/en/ow2/ow2-01-head.md \
		texts/en/ngi-05a-team.md \
		texts/en/ow2/ow2-02-content.md \
		texts/en/ow2/ow2-02-credits.md \
		> presentation-en-ow2.md
# NGI ENTRUST
	@cat \
		texts/en/ngi_entrust/ngi-01-head-entrust.md \
		texts/en/ngi-02a-project_description.md \
		texts/en/ngi_entrust/ngi-02b-scopes.md \
		texts/en/ngi-02c-challenges.md \
		texts/en/ngi-03-benchmark.md \
		texts/en/ngi-04-goals_intro.md \
		texts/en/ngi-04a-goal_1.md \
		texts/en/ngi_entrust/ngi-04a-goal_1-entrust.md \
		texts/en/ngi-04b-goal_2.md \
		texts/en/ngi-04c-goal_3.md \
		texts/en/ngi-05a-team.md \
		texts/en/ngi-05b-references.md \
		texts/en/ngi-06-tables-legends.md \
		texts/en/ngi_entrust/ngi-06-tables-entrust.md \
		texts/en/ngi-07-credits.md \
		> ngi-entrust.md
# NGI SEARCH
	@cat \
		texts/en/ngi_search/ngi-01-head-search.md \
		texts/en/ngi-02a-project_description.md \
		texts/en/ngi_search/ngi-02b-scopes.md \
		texts/en/ngi-02c-challenges.md \
		texts/en/ngi-03-benchmark.md \
		texts/en/ngi-04-goals_intro.md \
		texts/en/ngi-04a-goal_1.md \
		texts/en/ngi_search/ngi-04a-goal_1-search.md \
		texts/en/ngi-04b-goal_2.md \
		texts/en/ngi-04c-goal_3.md \
		texts/en/ngi-05a-team.md \
		texts/en/ngi-05b-references.md \
		texts/en/ngi-06-tables-legends.md \
		texts/en/ngi_search/ngi-06-tables-search.md \
		texts/en/ngi-07-credits.md \
		> ngi-search.md
# NGI SARGASSO
	@cat \
		texts/en/ngi_sargasso/ngi-01-head-sargasso.md \
		texts/en/ngi-02a-project_description.md \
		texts/en/ngi_sargasso/ngi-02b-areas_of_knowledge.md \
		texts/en/ngi-02c-challenges.md \
		texts/en/ngi-03-benchmark.md \
		texts/en/ngi-04-goals_intro.md \
		texts/en/ngi-04a-goal_1.md \
		texts/en/ngi_sargasso/ngi-04a-goal_1-sargasso.md \
		texts/en/ngi-04b-goal_2.md \
		texts/en/ngi-04c-goal_3.md \
		texts/en/ngi-05a-team.md \
		texts/en/ngi-05b-references.md \
		texts/en/ngi_sargasso/ngi-06-tables-legends-sargasso.md \
		texts/en/ngi_sargasso/ngi-06-budget-sargasso.md \
		texts/en/ngi_sargasso/ngi-06-timelines-sargasso.md \
		texts/en/ngi-07-credits.md \
		> ngi-sargasso.md
# NGI SARGASSO - LIGHT
	@cat \
		texts/en/ngi_sargasso/ngi-01-head-sargasso.md \
		texts/en/ngi_sargasso/ngi-06-timelines-sargasso.md \
		texts/en/ngi_sargasso/ngi-06-tables-legends-sargasso.md \
		texts/en/ngi_sargasso/ngi-06-budget-sargasso.md \
		texts/en/ngi_sargasso/ngi-07-references-sargasso.md \
		> ngi-sargasso-gantt-budget.md
# TEST
	@cat \
		texts/fr/ngi-01-head.md \
		texts/fr/ngi-02a-project_description.md \
		texts/fr/ngi-02c-challenges.md \
		texts/fr/ngi-03-benchmark.md \
		texts/fr/ngi-04-goals_intro.md \
		texts/fr/ngi-04a-goal_1.md \
		texts/fr/ngi-04b-goal_2.md \
		texts/fr/ngi-04c-goal_3.md \
		texts/fr/ngi-05a-team.md \
		texts/fr/ngi-05b-references.md \
		texts/fr/ngi-06-tables-legends.md \
		texts/fr/ngi-07-credits.md \
		> test-fr.md

open_science_html:
	@make concat_md
	@make render_html IN=presentation-en-open-science.md

ow2_html:
	@make concat_md
	@make render_html IN=presentation-en-ow2.md

ow2_html_pdf:
	@make ow2_html
	@make render_pdf IN=presentation-en-ow2.md
	@make move_pdf

ngi_entrust_pdf:
	@make concat_md
	@make render_pdf IN=ngi-entrust.md
	@make move_pdf

ngi_sargasso_html:
	@make concat_md
	@make render_html IN=ngi-sargasso.md
	@make render_html IN=ngi-sargasso-gantt-budget.md

ngi_sargasso_pdf:
	@make concat_md
	@make render_pdf IN=ngi-sargasso-gantt-budget.md
	@make render_pdf IN=ngi-sargasso.md
	@make move_pdf

ngi_sargasso_pdf_light:
	@make concat_md
	@make render_pdf IN=ngi-sargasso-gantt-budget.md
	@make move_pdf

all_ngi_pdf:
	@make concat_md
	@make render_pdf IN=ngi-entrust.md
	@make render_pdf IN=ngi-search.md
	@make render_pdf IN=ngi-sargasso.md
	@make render_pdf IN=ngi-sargasso-gantt-budget.md
	@make move_pdf

all_pdf:
	@make concat_md
	@make render_pdf IN=presentation-fr.md
	@make render_pdf IN=presentation-en.md
	@make render_pdf IN=presentation-en-ow2.md
	@make render_pdf IN=presentation-en-open-science.md
	@make render_pdf IN=ngi-entrust.md
	@make render_pdf IN=ngi-search.md
	@make render_pdf IN=ngi-sargasso.md
	@make move_pdf

move_pdf:
	@mv *.pdf ./pdf_files

render_html: ## Render presentation to revealjs slideshow with pandoc
	$(info $... render_html)
	$(info $$REPO = $(REPO))
	$(info $$IN = $(IN))
	$(info $$OUT = $(OUT))
	$(info $$basename = $(basename $(IN)))
	@pandoc \
		-t revealjs \
		-o $(OUT).html \
		-V colorlinks=true \
		-V theme=white \
		-V slideNumber=true \
		-V progress=true \
		--css static/style.css \
		--standalone \
		--slide-level=2 \
		--include-in-header=static/header_include.html \
		--include-after-body=static/add_custom_footer.html \
		$(IN)

$(OUT).html: render_html

render_pdf: $(OUT).html ## Render presentation to pdf with decktape (requires revealjs slideshow)
	@decktape $(OUT).html $(OUT).pdf

gitlab_pages: $(OUT).html public/_redirects # Used in CI to prepare ./public directory for gitlab pages. Does not repeat pages setup if already run.
	@cp $(OUT).html public

public/_redirects:
	@make setup_gitlab_pages

setup_gitlab_pages: # Setup static assets and redirection from the root. CI_PROJECT_NAME is populated by gitlab CI
	@mkdir public
	@echo '/$(CI_PROJECT_NAME)  /$(CI_PROJECT_NAME)/$(OUT).html' > public/_redirects
	@echo '/:subgroup/$(CI_PROJECT_NAME)/  /:subgroup/$(CI_PROJECT_NAME)/$(OUT).html' >> public/_redirects
	@cp -r static public/static
	@cp -r images public/images

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

