---
title: Datami
subtitle: <strong>Editez, visualisez, partagez&nbsp;<br>&nbsp;et contribuez à vos données&nbsp; <br>&nbsp;en toute simplicité</strong>
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _développeur fullstack_
  </span><br>
  <span class="text-micro text-author-details">
    _co-fondateur de la [coopérative multi](https://multi.coop)_
  </span>
'
date: <span class="emph-line">20 mars 2023</span><br>Plan en pressant <span class="text-nano">`echap`</span><br>Versions &colon; [fr](https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-fr.html) / [en](https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-en.html)
title-slide-attributes:
  data-background-image: "images/logos/logo-DATAMI-rect-colors.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 5%"
  data-background-position: "right 10% top 40%, right 4% top 40%"
---

# Aider vos publics avec vos données

::: {.text-center style="font-size:2em"}
**Quels enjeux ?**
:::

## Enjeu #1 

### Rendre vos données **intelligibles**

--- 

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/messy-excel-02.png)
:::
:::

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ODF-map-all.png)
:::
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
Vos données brutes <br>**sans** Datami
:::
:::

::: {.column width="50%"}
::: {.text-center}
Vos données valorisées <br>**avec** Datami
:::
:::
::::::::::::::

---

## Enjeu #2 

### Faciliter la **mise à jour** et la **contribution**

---

:::::::::::::: {.columns}

::: {.column width="40%"}
Manipuler des données est une question d'habitudes, faciliter leur manipulation pour le plus grand nombre signifie qu'il faut **s'adapter aux habitudes** du plus grand nombre

:::fragment
Pour permettre à chacun de facilement contribuer aux données la vue sous forme de table est encore aujourd'hui la plus communément adoptée
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/table-view-01.png)
:::
:::

::::::::::::::

---

## Enjeu #3

### Le faire de façon **économique**

---

Lorsqu’on est une structure de taille petite ou moyenne (association, collectivité…) et qu’on produit des données d'intérêt général il peut être compliqué de les mettre en valeur sur son site ou de faire appel à sa communauté pour les mettre à jour

::: {.text-center}
**Manque de moyens, manque de compétences, manque de temps...**
:::

:::::::::::::: {.columns}
::: {.column width="30%"}
:::fragment
::: {.text-justify}
Le coût des solutions techniques habituelles de partage / visualisation / contribution aux données s'explique souvent par la **complexité** technique de ces fonctionnalités
:::
:::
:::
::: {.column width="30%"}
:::fragment
::: {.text-justify style="padding: 0 1.5em 0 1.5em"}
Un autre coût est lié à la nécessité de mettre en place des **serveurs** dédiés en _backend_ ou des configurations très spécifiques
:::
:::
:::
::: {.column width="30%"}
:::fragment
::: {.text-justify}
La **maintenance** des applicatifs ou des serveurs sont générateurs de coûts souvent importants
:::
:::
:::
::::::::::::::

:::fragment
L'architecture originale de Datami permet de **s'affranchir d'une grande part de ces coûts de serveurs de _backend_** tout en permettant une personnalisation jeu de données par jeu de données
:::

# Valorisez vos données d'intérêt général avec **Datami**

:::fragment
::: {.img-mini}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::
:::

---

<video
  id="datami-video-presentation"
  width="100%"
  height="90%"
  style="box-shadow: 0 0 20px #D7D7D7;"
  poster="https://raw.githubusercontent.com/multi-coop/datami-website-content/images/logos/logo-DATAMI-rect-colors-03.png"
  controls>
  <source
    src="https://raw.githubusercontent.com/multi-coop/datami-website-content/main/videos/DATAMI_PRESENTATION-FR.mp4"
    type="video/mp4">
</video>

---

## Vos tableurs transformés en cartographies

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami permet de créer des **cartes interactives sur mesure**

:::fragment
Vos données territoriales peuvent être visualisées sous forme géographique, quelles que soient leurs thématiques
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ping-map-detail-01.png)
:::
:::

::::::::::::::

---

## Visualisez vos données <br>sous tous les angles

:::::::::::::: {.columns}

::: {.column width="40%"}
Vos données peuvent être visualisées comme des **tableurs**, des **cartes**, des **listes de fiches** miniatures ou détaillées, ou des **graphiques**

:::fragment
Toutes les vues sont interactives et configurables afin de mettre en lumière toutes les spécificités de vos données
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-views-01.png)
:::

::::::::::::::


--- 

## Adaptez Datami <br>en fonction de vos besoins

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-sketches-01.png)
:::
:::

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-sketches-02.png)
:::
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="50%"}
::: {.text-center}
Vos **maquettes** <br>et vos **données**...
:::
:::

::: {.column width="50%"}
::: {.text-center}
... implémentées et valorisées <br>avec **Datami**
:::
:::
::::::::::::::

---

## Vous contrôlez vos données <br>Datami les rend intelligibles

L'architecture de Datami est pensée comme une **interface entre le citoyen et votre base de données** afin de faciliter le lien **entre les citoyens et les producteurs d'open data**

::: {.no-mt .h-30}
![](images/roadmaps/archi-devs-fr.png)
:::

---

**Datami ne stocke pas vos données** : vos données restent sur l'outil de votre choix (Github, Gitlab, base de données ou API).

Utiliser Datami est ainsi plus **économique** car il vous évite d'avoir à installer et à maintenir un _backend_ dédié. 


---

## Intégrez Datami à votre site <br>et chez vos partenaires

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami utilise la technologie des **_widgets_** : des modules clé en main et personnalisables que vous pouvez ajouter sur un site déjà existant

:::fragment
Les _widgets_ Datami sont **_open source_**, simples à copier-coller, sans abonnement, **sans surcoût**
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/widget-copy-01.png)
:::

::::::::::::::



# Une mise à jour des données simplifiée et contributive

## Permettez à vos équipes et à vos publics d'améliorer les données

:::::::::::::: {.columns}

::: {.column width="40%"}
Datami inclut un **système de contribution et de modération**

:::fragment
En s’appuyant sur le langage Git, les _widgets_ Datami vous permettent de garder la main sur vos données et de gérer les contributions, **sans création de compte** pour proposer des améliorations
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-contribution-01.png)
:::
:::

::::::::::::::

---

## Structurez vos données <br>pour les rendre interopérables

:::::::::::::: {.columns}

::: {.column width="40%"}
Vos données peuvent être associées à des fichiers permettant de les structurer, tels que des fichiers de **schéma de données**

:::fragment
En associant votre jeu de données à un schéma de données respectant des **standards** internationaux vous vous assurez qu'elles pourront être correctement réutilisées et améliorées
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-schemas-01.png)
:::

::::::::::::::



# Un outil de pilotage

## Pilotez vos actions de terrain par la donnée

:::::::::::::: {.columns}

::: {.column width="40%"}
**Explorer vos données** vous permet de mieux comprendre et piloter vos actions sur le terrain

:::fragment
Datami permet de mettre en place des data-visualisations interactives personnalisées, afin de rendre vos données facilement explorables
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/screenshots/datami-pilotage-01.png)
:::

::::::::::::::



# Un outil _open source_

## Un logiciel libre et multi-usages

![&nbsp;](images/screenshots/datami-feats-matrix.png)

---

### Découvrez les interfaces

<video
  id="datami-video-presentation"
  width="100%"
  height="85%"
  style="box-shadow: 0 0 20px #D7D7D7;"
  poster="https://raw.githubusercontent.com/multi-coop/datami-website-content/images/logos/logo-DATAMI-rect-colors-03.png"
  controls>
  <source
    src="https://raw.githubusercontent.com/multi-coop/datami-website-content/main/videos/DATAMI_TUTORIEL-FR.mp4#t=0,135"
    type="video/mp4">
</video>

---

Datami est un **logiciel sous licence libre** entièrement auditable et réutilisable

Pour découvrir et apprendre à utiliser Datami vous pouvez :

:::incremental
- Aller sur le [site officiel de Datami](https://datami.multi.coop) ;
- Accéder directement au [code source de Datami](https://gitlab.com/multi-coop/datami-project/datami) ;
- Consulter la [documentation technique](https://datami-docs.multi.coop) et les tutoriels en ligne ;
- Faire appel à la [coopérative Multi](https://multi.coop) pour du conseil en data science et pour de la formation.
:::



---


## Le site officiel

:::::::::::::: {.columns}

::: {.column width="40%"}
Sur notre **site officiel** vous trouverez des **vidéos** de présentation, des **exemples**, ainsi que notre espace de **blog**

:::fragment
Le site est traduit au français et à l'anglais
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-website-fr.png)
:::
::: {.text-center}
🚀 [datami.multi.coop](https://datami.multi.coop)
:::
:::

::::::::::::::

---

## Le code source

:::::::::::::: {.columns}

::: {.column width="40%"}
Notre **code source est sur Gitlab** sous licence _open source_

:::fragment
N'hésitez pas à nous signaler les _bugs_ en proposant des [_issues_](https://gitlab.com/multi-coop/datami-project/datami/-/issues), ou à donner des idées de nouvelles fonctionnalités sur notre [_roadmap_](https://gitlab.com/multi-coop/datami-project/datami/-/boards/4736577)

Un [repo miroir](https://github.com/multi-coop/datami) est également synchronisé automatiquement sur Github

Pensez à laisser une petite ⭐️ si le projet vous plaît !
:::
::: 


::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-gitlab.png)
:::
::: {.text-center}
💻 [Repo Gitlab](https://gitlab.com/multi-coop/datami-project/datami)

![](https://img.shields.io/badge/dynamic/json?color=turquoise&label=gitlab%20stars%20%E2%98%85&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39268471)

:::
:::

::::::::::::::

---

## Le site de documentation technique

:::::::::::::: {.columns}

::: {.column width="40%"}
Visitez également notre **site de documentation** dédié


:::fragment
Le site est traduit au français et à l'anglais

Vous y trouverez différentes sections: principes techniques, tutoriels, exemples, description des différents widgets et de leurs éléments de configuration...  
:::

::: 

::: {.column width="60%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/datami-doc-website.png)
:::
::: {.text-center}
🔧 [datami-docs.multi.coop](https://datami-docs.multi.coop)
:::
:::

::::::::::::::

---

## La _stack_ technique

:::::::::::::: {.columns}

::: {.column width="40%"}
La _stack_ technique est **entièrement composée de librairies _open source_**
::: 

::: {.column width="60%"}
- [`Vue.js (v2.x)`](https://v2.vuejs.org/v2/guide)
- [`VueX`](https://vuex.vuejs.org/)
- [`vue-custom-element`](https://github.com/karol-f/vue-custom-element)
- [`gray-matter`](https://www.npmjs.com/package/gray-matter)
- [`Showdown`](https://www.npmjs.com/package/showdown) + [`showdown-table extension`](https://github.com/showdownjs/table-extension#readme)
- [`Bulma`](https://bulma.io/) + [`Buefy`](https://buefy.org/)
- [`Material Design`](https://materialdesignicons.com/)
- [`Fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
- [`JSDiff`](https://github.com/kpdecker/jsdiff)
- [`Diff2html`](https://www.npmjs.com/package/diff2html)
- [`MapLibre`](https://maplibre.org)
- [`ApexCharts`](https://apexcharts.com)
:::

::::::::::::::

---

## Référencement

:::::::::::::: {.columns}

::: {.column width="50%"}
Sur [Le Comptoir du Libre](https://comptoir-du-libre.org/fr/softwares/583)

:::fragment
Visitez cette page pour laisser un commentaire sur Datami !
:::
:::

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ref-comptoir-du-libre.png)
:::
:::

::::::::::::::

:::::::::::::: {.columns}

::: {.column width="50%"}
Sur [AlternativeTo](https://alternativeto.net/software/datami/about/p)

:::fragment
Visitez cette page pour laisser un commentaire sur Datami !
:::
::: 

::: {.column width="50%"}
::: {.img-shadow}
![&nbsp;](images/screenshots/ref-alternative-to.png)
:::
::: 

::::::::::::::

# Offre de services

## L'offre Datami par la coopérative multi

:::::::::::::: {.columns}

::: {.column width="40%"}
Afin de rendre Datami le plus accessible possible, nous avons pour principe celui de **mutualiser la conception et les coûts de développements**

:::fragment
Tous les développements - même minimes - contribuant à améliorer Datami profitent ainsi au final à tous les utilisateurs
:::

::: 

::: {.column width="60%"}
![&nbsp;](images/offer/multi-datami-offer-fr.png)
:::

::::::::::::::

---

## Mise en place de Datami

:::::::::::::: {.columns}

::: {.column width="40%"}
![&nbsp;](images/offer/datami-logo-setup.png)
:::

::: {.column width="60%"}

Nous proposons un **forfait économique de quelques jours** seulement pour un accompagnement de mise en place de Datami

:::fragment
Nous pouvons vous aider à configurer Datami et vous conseiller dans votre projet de valorisation de données
:::

:::

::::::::::::::

---

## Logiciel libre

:::::::::::::: {.columns}

::: {.column width="40%"}
![&nbsp;](images/offer/datami-logo-pack.png)
:::

::: {.column width="60%"}
Datami est un outil **100% _open source_**

:::fragment
Vous pouvez utiliser Datami tel quel en vous appuyant sur la documentation, vous êtes libre !
:::

:::
::::::::::::::

---

## Développements personnalisés

:::::::::::::: {.columns}

::: {.column width="40%"}
![&nbsp;](images/offer/datami-logo-custom_dev.png)
:::

::: {.column width="60%"}
N'hésitez pas à nous contacter pour nous faire part de **vos besoins** pour vous établir un devis personnalisé

:::fragment
Nous avons à cœur de continuer à développer et d'améliorer Datami, et nous proposons également des services en _data science_ et _data engineering_
:::

:::
::::::::::::::


# Guide d'installation

## Le code source


Clonez le code source depuis le [repo Gitlab](https://gitlab.com/multi-coop/datami-project/datami) de Datami

```bash
git clone git@gitlab.com:multi-coop/datami-project/datami.git
````

Puis accédez au dossier

```bash
cd datami
```

---

## Installation

:::::::::::::: {.columns}
::: {.column width="30%"}
Installez `npm 8.3.2` ...
::: 
::: {.column width="70%"}
```bash
npm install -g npm@8.3.2
```
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="30%"}
... ou utilisez `nvm`
::: 
::: {.column width="70%"}
```bash
brew install nvm # sur mac
nvm use
```
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="30%"}
Installez les dépendances
::: 
::: {.column width="70%"}
```bash
npm install
```
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="30%"}
Créez un fichier `.env` localement sur la base du ficher `example.env` en exemple
::: 
::: {.column width="70%"}
```bash
cp example.env .env
```
:::
::::::::::::::

---

## Lancez Datami

:::::::::::::: {.columns}
::: {.column width="30%"}
Lancez le script
::: 
::: {.column width="70%"}
```bash
npm run serve
```
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="30%"}
Dans votre navigateur
::: 
::: {.column width="70%"}
```bash
http://localhost:8080
```
:::
::::::::::::::

---

## Liste d'exemples de _widgets_

:::::::::::::: {.columns}
::: {.column width="30%"}
Lancez le serveur local
::: 
::: {.column width="70%"}
```bash
nvm use
npm run http
```
:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column width="30%"}
Dans votre navigateur
::: 
::: {.column width="70%"}
```bash
http://localhost:8180/html-tests/
```
:::
::::::::::::::


# Benchmark

---

::: {.text-center}
Il existe plusieurs solutions de datavisualisation et d'édition <br>partageant des similitudes avec [Datami](https://datami.multi.coop)

Voici quelques-unes des solutions les plus populaires

:::fragment
**Ces benchmarks sont donnés à titre purement indicatif**

N'hésitez pas à nous écrire à<br>`contact@multi.coop` <br>si vous souhaitez y apporter des corrections ou des ajouts
:::

:::

---

### Outils de datavisualisation


| Solution               | Type de solution | Langages supportés                  | Niveau de difficulté | En Saas | Site officiel                                                    |
| ------------------     | ---------------- | ----------------------------------- | -------------------- | ------- | ---------------------------------------------------------------- |
| **Gogocarto**          | Open source      | Propre langage de requête           | Facile               | Oui     | [https://gogocarto.fr/projects](https://gogocarto.fr/projects)   |
| **Umap**               | Open source      | Propre langage de requête           | Facile               | Oui     | [https://umap.openstreetmap.fr/](https://umap.openstreetmap.fr/) |
| **Lizmap**             | Open source      | Propre langage de requête           | Moyen                | Oui     | [https://www.lizmap.com](https://www.lizmap.com)                  |
| **Apache Superset**    | Open source      | SQL                                 | Moyen                | Oui     | [https://superset.apache.org/](https://superset.apache.org/)     |
| **Apache Zeppelin**    | Open source      | Plusieurs langages de programmation | Difficile            | Non     | [https://zeppelin.apache.org/](https://zeppelin.apache.org/)     |
| **BIRT**               | Open source      | Java, JavaScript                    | Difficile            | Non     | [https://www.eclipse.org/birt/](https://www.eclipse.org/birt/)   |
| **FineReport**         | Open source      | Java, JavaScript                    | Moyen                | Non     | [https://www.finereport.com/en](https://www.finereport.com/en)   |
| **Grafana**            | Open source      | Propre langage de requête           | Difficile            | Non     | [https://grafana.com/](https://grafana.com/)                     |
| **Metabase**           | Open source      | SQL                                 | Facile               | Oui     | [https://www.metabase.com/](https://www.metabase.com/)           |
| **Redash**             | Open source      | SQL                                 | Moyen                | Non     | [https://redash.io/](https://redash.io/)                         |
| **Datasette**          | Open source      | SQL                                 | Moyen                | Non     | [https://datasette.io/](https://datasette.io/)                   |
| **LightDash**          | Open source      | Dbt                                 | Moyen                | Oui     | [https://www.lightdash.com/](https://www.lightdash.com/)         |
| **Google Data Studio** | Gratuit          | SQL                                 | Facile               | Oui     | [https://datastudio.google.com/](https://datastudio.google.com/) |
| **Datawrapper**        | Commercial       | API, CSV, GSheet                    | Facile               | Oui     | [https://www.datawrapper.de/](https://www.datawrapper.de/)       |
| **Google Looker**      | Commercial       | LookML                              | Difficile            | Oui     | [https://looker.com/](https://looker.com/)                       |
| **Microsoft Power BI** | Commercial       | DAX et M                            | Moyen                | Oui     | [https://powerbi.microsoft.com/](https://powerbi.microsoft.com/) |
| **QlikView**           | Commercial       | Propre langage de script            | Difficile            | Oui     | [https://www.qlik.com/](https://www.qlik.com/)                   |
| **Tableau**            | Commercial       | Calculs personnalisés               | Moyen                | Oui     | [https://www.tableau.com/](https://www.tableau.com/)             |

---

### Outils d'édition en ligne

| Solution            | Type de solution | Langages supportés    | Niveau de difficulté | Saas | Public visé                                                                          | Site officiel                                                                |
|---------------------|-------------|----------------------------|----------------------|------|--------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| **Apache Zeppelin** | Open source | Scala, Python, R, SQL      | Difficile            | Non  | Développeurs et utilisateurs professionnels                                          | [https://zeppelin.apache.org/](https://zeppelin.apache.org/)                 |
| **Baserow**         | Open source | Python, Javascript, Vue.js | Moyen                | Oui  | Développeurs et utilisateurs professionnels                                          | [https://baserow.io/](https://baserow.io/)                                   |
| **Grist**           | Open source | Python                     | Facile               | Oui  | Entreprises, organisations sans but lucratif, gouvernements, universités, chercheurs | [https://getgrist.com/](https://getgrist.com/)                               |
| **Metabase**        | Open source | Java, Clojure              | Moyen                | Oui  | Startups, PME, organisations sans but lucratif                                       | [https://www.metabase.com/](https://www.metabase.com/)                       |
| **LockoKit**        | Open source | ...                        | Moyen                | Non  | Développeurs et utilisateurs professionnels                                          | [https://locokit.io/](https://locokit.io/)                                   |
| **NoCodB**          | Open source | Javascript, Node.js        | Facile               | Oui  | Développeurs et utilisateurs professionnels                                          | [https://nocodb.com/](https://nocodb.com/)                                   |
| **Gsheet**          | Gratuit     | Aucune                     | Facile               | Oui  | Entreprises, équipes, indépendants, PME                                              | [https://www.google.com/sheets/about/](https://www.google.com/sheets/about/) |
| **Airtable**        | Commercial  | Aucune                     | Facile               | Oui  | Entreprises, équipes, indépendants, PME                                              | [https://airtable.com/](https://airtable.com/)                               |
| **Qlikview**        | Commercial  | Aucune                     | Moyen                | Oui  | Grandes entreprises, institutions financières                                        | [https://www.qlik.com/us/](https://www.qlik.com/us/)                         |


---

## Benchmark fonctionnel

::: {.text-micro}
Parmi toutes les solutions que nous venons de lister certaines peuvent être relativement aisément <br>comparées avec les fonctionnalités principales proposées par **Datami**  
:::

| Solution            | Open source  | Facilité  | Vue table  | Vue fiches  | Vue carto  | Vue dataviz  | Edition  | Modération |   Interface de configuration  | Data sources        | Backend              | Widget  | Site officiel                                   |
|-----------------    |------------- |---------- |----------- |------------ |----------- |------------- |--------  |------------ |---------------------------   |-------------------- |--------------------- | ------  |-------------------------------------------------|
| **Datami**          | ✅           | ⭐⭐      | ✅         | ✅          | ✅         | ✅           | ✅       | ✅          | ❌ (for now)                | API ext. (Git)      | Git platforms / APIs | ✅      | [Website](https://datami.multi.coop/)          |
| **Metabase**        | ✅           | ⭐⭐      | ✅         | ❓          | ✅         | ✅           | ⚠️        | ❌          | ✅                          | SQL, connectors     | server / APIs        | ✅      | [Website](https://www.metabase.com/)           |
| **Gogocarto**       | ✅           | ⭐⭐⭐    | ✅         | ✅          | ✅         | ❌           | ✅       | ❌          | ✅                          | proper              | server / APIs        | ✅      | [Website](https://gogocarto.fr/projects)       |
| **Lizmap**          | ✅           | ⭐        | ⚠️          | ❌          | ✅         | ✅           | ✅       | ❌          | ✅                          | PostGreSQL              | server / ❓          | ✅      | [Website](https://www.lizmap.com)              |
| **Umap**            | ✅           | ⭐⭐⭐    | ❌         | ❌          | ✅         | ❌           | ✅       | ❌          | ✅                          | ...                 | server / ❓          | ✅      | [Website](https://umap.openstreetmap.fr/)      |
| **Grist**           | ✅           | ⭐        | ✅         | ✅          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / APIs        | ❌      | [Website](https://getgrist.com/)               |
| **Baserow**         | ✅           | ⭐⭐      | ✅         | ✅          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / ❓          | ❌      | [Website](https://baserow.io/)                 |
| **LockoKit**        | ✅           | ⭐⭐      | ✅         | ❌          | ❌         | ❌           | ✅       | ❓          | ✅                          | PostgreSQL              | server / ❓          | ❌      | [Website](https://locokit.io/)                 |
| **NoCodB**          | ✅           | ⭐        | ✅         | ❌          | ❌         | ❌           | ✅       | ❓          | ✅                          | SQL                 | server / ❓          | ❌      | [Website](https://nocodb.com/)                  |
| **Apache Superset** | ✅           | ⭐⭐      | ✅         | ❌          | ❌         | ✅           | ✅       | ❓          | ✅                          | SQL                 | server / Saas        | ❓      | [Website](https://superset.apache.org/)         |
| **Datawrapper**     | 🔒           | ⭐⭐⭐    | ✅         | ❓          | ✅         | ✅           | ❌       | ❌          | ✅                          | SQL, connectors     | Saas                 | ✅      | [Website](https://www.datawrapper.de/)          |
| **Airtable**        | 🔒           | ⭐⭐⭐    | ✅         | ✅          | ❌         | ⚠️            | ✅       | ⚠️           | ✅                          | proper, connectors  | Saas                 | ✅      | [Website](https://airtable.com/)                |
| **Gsheet**          | 🔒           | ⭐⭐⭐    | ✅         | ❌          | ❌         | ✅           | ✅       | ✅          | ✅                          | proper, connectors  | Saas                 | ✅      | [Website](https://www.google.com/sheets/about/) |


::: {.text-micro .text-center}
Les ❓ indiquent que l'information est à compléter <br>
Les ⚠️ que la fonctionnalité peut être mise en place mais que son usage peut être ardu
:::

---

Bien que ces solutions partagent certaines fonctionnalités avec [Datami](https://datami.multi.coop), elles peuvent différer considérablement en termes de coût, de complexité et de fonctionnalités spécifiques

Il est important de prendre en compte les **besoins spécifiques de chaque projet** avant de choisir la solution de datavisualisation et d'édition en ligne la plus adaptée



# Feuilles de route techniques

---

Voici une liste de différentes fonctionnalités <br>reflétant les demandes et les besoins émanant <br>de nos différents utilisateurs à ce jour.

::: {.text-micro}
Cette feuille de route est à prendre comme une _whish list_ des fonctionnalités que nous considérons comme intéressantes et compatibles avec Datami, et que nous aimerions pouvoir développer **si nous arrivons à trouver les financements correspondants**.
:::

---

Les développements sont décrits par :

:::{.text-micro}
- **Milestone** : la famille de fonctionnalités
- **Features** : la fonctionnalité à développer
- **Priority** ( 🔴 ) : importance donnée à la fonctionnalité par les utilisateurs
- **Difficulty** ( ▪️ ) : complexité anticipée des développements
- **Dev. + man.** : somme des temps de développement et de gestion de projet à prévoir (en jours / personne)
:::



---

## Feuille de route technique 2023 - globale

<br>

::: {.text-center}
<b>Classement par milestones</b>
:::

| **Milestones**                      | **Priority** | **Difficulty** | **Dev + man.** |
|-------------------------------------|--------------|----------------|----------------|
| <b>More data sources</b>            | 🔴 high      | ▪️ easy        | <b>38 days</b> |
| <b>Online widget configuration</b>  | 🔴 high      | ▪️▪️▪️ hard    | <b>26 days</b> |
| <b>Manage contribution widget</b>   | 🔴 high      | ▪️▪️▪️ hard    | <b>25 days</b> |
| <b>Protect widget with password</b> | 🟠 medium    | ▪️ easy        | <b>12 days</b> |
| <b>Better UX - data management</b>  | 🟡 low       | ▪️▪️ medium    | <b>42 days</b> |
| <b>Better UX - data interaction</b> | 🟡 low       | ▪️▪️ medium    | <b>44 days</b> |
| <b>Better UX - maps</b>             | 🟡 low       | ▪️▪️ medium    | <b>37 days</b> |
| <b>Better UI - customization</b>    | 🟠 medium    | ▪️ easy        | <b>13 days</b> |
| <b>Better UI - new views</b>        | 🟡 low       | ▪️▪️ medium    | <b>27 days</b> |
| <b>Refactoring</b>                  | 🟠 medium    | ▪️▪️▪️ hard    | <b>48 days</b> |
| <b>Tests</b>                        | 🟡 low       | ▪️▪️▪️ hard    | <b>18 days</b> |
|                                     |              | **TOTAL**      | **330 days**   |

---

## Schémas des futurs développements

::: {.text-micro}

:::::: {.columns}

::: {.column width=50%}
Les parties en <span class="text-secondary">orange</span> symbolisent les principales _milestones_ à développer décrites dans les feuilles de route.

Les parties en **turquoise** symbolisent les fonctionnalités déjà existantes dans Datami.
:::

::: {.column width=50% .mt-default}
- <soan class="text-secondary">A</soan> : <b>Online widget configuration</b>
- <soan class="text-secondary">B</soan> : <b>Manage contribution widget</b>
- <soan class="text-secondary">C</soan> : <b>More data sources</b>
:::

::::::

:::

::: {.no-mt .h-50}
![&nbsp;](images/roadmaps/archi-devs-roadmap-fr.png)
:::


---

## Feuille de route technique 2023 - détaillée 1/2

| **Milestones**                      | **Features**                                     | **Priority** | **Difficulty** | **Dev + man.** |
|-------------------------------------|--------------------------------------------------|--------------|----------------|----------------|
| <b>More data sources</b>            | Connect to external APIs - generic               | 🔴 high      | ▪️ easy        | <b>5 days</b>  |
| <b>More data sources</b>            | Connect to external APIs - OSM                   | 🟡 low       | ▪️ easy        | <b>5 days</b>  |
| <b>More data sources</b>            | Activity pub integration                         | 🟡 low       | ▪️▪️▪️ hard    | <b>18 days</b> |
| <b>More data sources</b>            | Connector to SourceForge                         | 🟡 low       | ▪️▪️▪️ hard    | <b>5 days</b>  |
| <b>More data sources</b>            | Connector to BitBucket                           | 🟡 low       | ▪️▪️▪️ hard    | <b>5 days</b>  |
| <b>Online widget configuration</b>  | Interactive interface + preview                  | 🔴 high      | ▪️▪️▪️ hard    | <b>20 days</b> |
| <b>Online widget configuration</b>  | Save new config to git repo                      | 🔴 high      | ▪️▪️ medium    | <b>6 days</b>  |
| <b>Manage contribution widget</b>   | Interface loading last PRs from repo             | 🔴 high      | ▪️▪️▪️ hard    | <b>15 days</b> |
| <b>Manage contribution widget</b>   | Accept / moderate PR + messages                  | 🔴 high      | ▪️ easy        | <b>10 days</b> |
| <b>Protect widget with password</b> | Protect access before showing widget             | 🟠 medium    | ▪️ easy        | <b>6 days</b>  |
| <b>Protect widget with password</b> | Special token for protected widget               | 🟠 medium    | ▪️▪️ medium    | <b>6 days</b>  |
| <b>Better UX - data management</b>  | Add a new column + update schema                 | 🟠 medium    | ▪️▪️ medium    | <b>10 days</b> |
| <b>Better UX - data management</b>  | Drag & drop CSV to widget                        | 🟡 low       | ▪️▪️ medium    | <b>10 days</b> |
| <b>Better UX - data management</b>  | Save CSV file to Git repo                        | 🟡 low       | ▪️▪️ medium    | <b>4 days</b>  |
| <b>Better UX - data management</b>  | Add / drag-drop a picture in a cell              | 🟡 low       | ▪️▪️ medium    | <b>6 days</b>  |
| <b>Better UX - data management</b>  | Cache user changes / branch until pushing       | 🟠 medium    | ▪️▪️▪️ hard    | <b>12 days</b> |
| <b>Better UX - data interaction</b> | Full screen - debug                              | 🟠 medium    | ▪️▪️ medium    | <b>6 days</b>  |
| <b>Better UX - data interaction</b> | Range filter                                     | 🟡 low       | ▪️ easy        | <b>4 days</b>  |
| <b>Better UX - data interaction</b> | Change width column                              | 🟠 medium    | ▪️▪️ medium    | <b>8 days</b>  |
| <b>Better UX - data interaction</b> | Helper at loader                                 | 🟡 low       | ▪️ easy        | <b>2 days</b>  |
| <b>Better UX - data interaction</b> | Better integration of Frictionless data packages | 🟡 low       | ▪️▪️▪️ hard    | <b>12 days</b> |
| <b>Better UX - data interaction</b> | Export as pdf                                    | 🟡 low       | ▪️▪️▪️ hard    | <b>12 days</b> |

## Feuille de route technique 2023 - détaillée 2/2

| **Milestones**                      | **Features**                                     | **Priority** | **Difficulty** | **Dev + man.** |
|-------------------------------------|--------------------------------------------------|--------------|----------------|----------------|
| <b>Better UX - maps</b>             | Inject data to vector tiles on map view          | 🟡 low       | ▪️▪️ medium    | <b>8 days</b>  |
| <b>Better UX - maps</b>             | Add or edit geojson objects                      | 🟡 low       | ▪️▪️▪️ hard    | <b>29 days</b> |
| <b>Better UI - customization</b>    | Custom styles / CSS / logos                      | 🟠 medium    | ▪️ easy        | <b>4 days</b>  |
| <b>Better UI - customization</b>    | Accessibility                                    | 🟠 medium    | ▪️▪️ medium    | <b>9 days</b>  |
| <b>Better UI - new views</b>        | Agenda view                                      | 🟡 low       | ▪️ easy        | <b>8 days</b>  |
| <b>Better UI - new views</b>        | Graphs view with D3js                            | 🟡 low       | ▪️▪️ medium    | <b>10 days</b> |
| <b>Better UI - new views</b>        | Simultaneous dataviz + map on same view          | 🟡 low       | ▪️▪️ medium    | <b>9 days</b>  |
| <b>Refactoring</b>                  | Put all git* requests into a package             | 🟡 low       | ▪️▪️▪️ hard    | <b>15 days</b> |
| <b>Refactoring</b>                  | Migration to Typescript                          | 🟠 medium    | ▪️▪️▪️ hard    | <b>15 days</b> |
| <b>Refactoring</b>                  | Migration to Vue3                                | 🟡 low       | ▪️▪️▪️ hard    | <b>18 days</b> |
| <b>Tests</b>                        | Add functional & unit tests                      | 🟡 low       | ▪️▪️▪️ hard    | <b>18 days</b> |





---

## Feuille de route technique 2023 - globale

<br>

::: {.text-center}
<b>Classement par priorité / difficulté</b>
:::

::: {.table-matrix}

| **Priority / Difficulty** | ▪️▪️▪️ hard     | ▪️▪️ medium    | ▪️ easy        | **TOTAL**       |
|---------------------------|-----------------|----------------|----------------|-----------------|
| 🔴 high                   |  35 days        |  6 days        |  15 days       | <b>56 days</b>  |
| 🟠 medium                 |  27 days        |  39 days       |  10 days       | <b>76 days</b>  |
| 🟡 low                    |  132 days       |  47 days       |  19 days       | <b>198 days</b> |
| **TOTAL**                 | <b>194 days</b> | <b>92 days</b> | <b>44 days</b> | **330 days**    |

:::




---

## Planning prévisionnel

::: {.img-table}
![&nbsp;](images/tables/retroplanning.png)
:::

::: {.text-micro .text-center}
Note : ce planning est appelé à évoluer en fonction des demandes des utilisateurs et des financements.
:::




# Crédits

## Un projet par la coopérative multi

:::::::::::::: {.columns}

::: {.column width="60%"}
Notre coopérative contribue à développer des **communs numériques** et des services associés, en regroupant une communauté de professionnel·le·s oeuvrant pour un **numérique d’intérêt général**
::: 

::: {.column width="40%"}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)

::: {.text-center}
🚀 [https://multi.coop](https://multi.coop)
:::

:::

::::::::::::::

---

## Nos premiers utilisateurs

![&nbsp;](images/clients/clients.png)


---

## Nos sponsors

Datami a été un projet **lauréat du Plan France Relance 2022** et a bénéficié du soutien des organismes suivants

![&nbsp;](images/sponsors/sponsors.png)

#

::: {.text-center}
**Merci pour votre attention !**
:::

<br>

::::::::::::: {.columns}

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-DATAMI-rect-colors.png)
:::
:::

::: {.column width="20%"}
::: {.text-micro .text-center}
[Datami](https://datami.multi.coop)

est un projet porté par la coopérative

[multi](https://multi.coop)
:::
:::

::: {.column width="40%"}
::: {.img-nano}
![&nbsp;](images/logos/logo-MULTI-colored-063442-02.png)
:::
:::

:::::::::::::


::::::::::::: {.columns}

::: {.column width="30%"}
::: {.img-mini}
![&nbsp;](images/sponsors/sponsors.png)
:::
:::

:::::::::::::

::: {.text-center}
**contact@multi.coop**
:::

---

::: {.text-center}
[Source de la présentation](https://gitlab.com/multi-coop/datami-project/datami-slides-fr)
:::