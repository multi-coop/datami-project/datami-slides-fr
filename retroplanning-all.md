---
title: Datami
subtitle: <strong>Rétroplanning 2023</strong>
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _développeur fullstack_
  </span><br>
  <span class="text-micro text-author-details">
    _co-fondateur de la [coopérative multi](https://multi.coop)_
  </span>
'
date: <span class="emph-line">14 mars 2023</span><br>Plan en pressant <span class="text-nano">`echap`</span><br>Versions &colon; [fr](https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-fr.html) / [en](https://multi-coop.gitlab.io/datami-project/datami-slides-fr/presentation-en.html)
title-slide-attributes:
  data-background-image: "images/logos/logo-DATAMI-rect-colors.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 5%"
  data-background-position: "right 10% top 40%, right 4% top 40%"
---

# GLOBAL

---

**Global - tech**

---

::: {.ngi-timeline .ngi-timeline-mini}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|
|TECH|<b>More data sources</b>|Connect to external APIs - generic|🔴|▪️▪️|<b>15 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>More data sources</b>|Connect to external APIs - OSM|🟡|▪️▪️|<b>8 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>More data sources</b>|Connect to external APIs - OpenDataSoft|🟠|▪️▪️|<b>4 days</b>|||||||
|TECH|<b>More data sources</b>|Connect to external APIs - Datagouv|🟡|▪️▪️|<b>4 days</b>|||||||
|TECH|<b>More data sources</b>|Connect to external APIs - Wikidata|🟠|▪️▪️|<b>4 days</b>|||||||
|TECH|<b>More data sources</b>|Connect to external APIs - Mediawiki ++|🟡|▪️▪️|<b>4 days</b>|||||||
|TECH|<b>More data sources</b>|Connect to external APIs - CSV on Spip / Wordpress |🟡|▪️▪️|<b>4 days</b>|||||||
|TECH|<b>More data sources</b>|Activity pub integration|🟡|▪️▪️▪️|<b>18 days</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>More data sources</b>|Connector to SourceForge|🟡|▪️▪️▪️|<b>5 days</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>More data sources</b>|Connector to BitBucket|🟡|▪️▪️▪️|<b>5 days</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>Online widget configuration</b>|Interactive interface + preview|🔴|▪️▪️▪️|<b>20 days</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Online widget configuration</b>|Save new config to git repo|🔴|▪️▪️|<b>10 days</b>||<span class="cell-bg">x</span>|||||
|TECH|<b>Manage contribution widget</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>17 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Manage contribution widget</b>|Accept / moderate PR + messages|🔴|▪️|<b>12 days</b>|||<span class="cell-bg">x</span>||||
|TECH|<b>Protect widget with password</b>|Protect access before showing widget|🟠|▪️|<b>6 days</b>|||<span class="cell-bg">x</span>||||
|TECH|<b>Protect widget with password</b>|Special token for protected widget|🟠|▪️▪️|<b>6 days</b>||||<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>10 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||
|TECH|<b>Better UX - data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>10 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UX - data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>4 days</b>|||||<span class="cell-bg">x</span>||
|TECH|<b>Better UX - data management</b>|Add / drag-drop a picture in a cell|🟡|▪️▪️|<b>6 days</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>Better UX - data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>12 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|TECH|<b>Better UX - data interaction</b>|Full screen - debug|🟠|▪️▪️|<b>6 days</b>|||||<span class="cell-bg">x</span>||
|TECH|<b>Better UX - data interaction</b>|Range filter|🟡|▪️|<b>4 days</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>Better UX - data interaction</b>|Change width column|🟠|▪️▪️|<b>8 days</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Better UX - data interaction</b>|Helper at loader|🟡|▪️|<b>2 days</b>||<span class="cell-bg">x</span>|||||
|TECH|<b>Better UX - data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>12 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UX - data interaction</b>|Export as pdf|🟡|▪️▪️▪️|<b>12 days</b>|||||||
|TECH|<b>Better UX - maps</b>|Inject data to vector tiles on map view|🟡|▪️▪️|<b>8 days</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|TECH|<b>Better UX - maps</b>|Add or edit geojson objects|🟡|▪️▪️▪️|<b>32 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|TECH|<b>Better UI - customization</b>|Custom styles / CSS / logos|🟠|▪️|<b>4 days</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||||
|TECH|<b>Better UI - customization</b>|Accessibility|🟠|▪️▪️|<b>12 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UI - new views</b>|Agenda view|🟡|▪️|<b>8 days</b>||||<span class="cell-bg">x</span>|||
|TECH|<b>Better UI - new views</b>|Graphs view with D3js|🟡|▪️▪️|<b>10 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|TECH|<b>Better UI - new views</b>|Simultaneous dataviz + map on same view|🟡|▪️▪️|<b>9 days</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>Refactoring</b>|Put all git* requests into a package|🟡|▪️▪️▪️|<b>15 days</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|TECH|<b>Refactoring</b>|Migration to Typescript|🟠|▪️▪️▪️|<b>15 days</b>||||||<span class="cell-bg">x</span>|
|TECH|<b>Refactoring</b>|Migration to Vue3|🟡|▪️▪️▪️|<b>18 days</b>|||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|TECH|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>18 days</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|


:::

---

::: {..ngi-timeline .ngi-timeline-mini}

|**Family**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2023|T2|T3|T4|T1 2024|T2|
|:---:|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|:---:|:---:|
|BIZDEV|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>20 days</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|BIZDEV|<b>Translations</b>|Translate to Spanish|🔴|▪️|<b>2 days</b>||<span class="cell-bg">x</span>|||||
|BIZDEV|<b>Translations</b>|Translate to Italian|🟡|▪️|<b>2 days</b>||||<span class="cell-bg">x</span>|||
|BIZDEV|<b>Community management</b>|Community management|🔴|▪️|<b>25 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|BIZDEV|<b>Community management</b>|Roadmap management|🟠|▪️▪️|<b>10 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|BIZDEV|<b>Community management</b>|Set up showcases|🟡|▪️▪️|<b>12 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|BIZDEV|<b>Business model</b>|Update website|🟠|▪️|<b>12 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|BIZDEV|<b>Business model</b>|Business model |🔴|▪️▪️▪️|<b>12 days</b>||||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|BIZDEV|<b>Design</b>|UX workshops|🟠|▪️▪️|<b>15 days</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|||


:::