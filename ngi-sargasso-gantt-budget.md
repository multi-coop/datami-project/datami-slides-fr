---
title: '
  <strong>NGI SARGASSO OPEN CALL \#2</strong>
  <!-- ![](images/logos/logo-DATAMI-rect-colors.png) -->
  '
subtitle: '
  <strong>0PN DPT 4 ALL</strong><br>
  <b>Digital Privacy Transparency Assessments, Listings<br>
  and Ledgers for Legal and Regulatory Adequacy <br>
  of International Data Transfers <br>
  (Quebec, CAN – France) 
  </b>
  <br>
  <br>
  Gantt chart & provisional budget'
author: '
  <span class="text-bold">Julien Paris</span><br>
  <span class="text-micro text-author-details">
    _fullstack developer_
  </span><br>
  <span class="text-micro text-author-details">
    _co-founder of the digital cooperative [multi.coop](https://multi.coop)_
  </span><br>
  <span class="img-cover">
    ![](images/logos/logo-MULTI-colored-063442-02-w120.png)
    ![](images/sponsors/0pn+green.png)
    ![](images/sponsors/OC-brand-logo-TM.png)
    ![](images/sponsors/SurveillanceTrustLogo.jpg)
    ![](images/sponsors/cropped-IDmachineLogo.png)
    ![](images/sponsors/Logo_Human_Colossus.png)
  </span>
'
date: '
  <span class="emph-line">December 18th 2023</span>
  <br>Navigation with <i class="ri-drag-move-2-fill"></i> / For plan press <span class="text-nano">`echap`</span>
  '
title-slide-attributes:
  data-background-image: "images/sponsors/ngi_sargasso-logo.png"
  # data-background-image: "static/logo.svg, static/logo_client.svg"
  data-background-size: "auto 10%, auto 5%"
  data-background-position: "right 10% top 45%, right 4% top 40%"

---

# Provisional timelines

**A lean approach for iterative and incremental developments**

---

## Timelines

<br>

::: {.text-center}

<b>
The following timelines are
to be considered as only 
provisional.
</b>

Some tasks listed in the Gantt chart,
and estimated to a certain amount of work days,
are considered to ventilated on one or several quarters.

:::{.text-micro}
These timelines may be adapted depending on
<br> 
the context, our resources,
and the priority of demands
<br>
emerging from  new use cases.
:::
:::

---

### Gantt chart

::: {.ngi-timeline .ngi-timeline-mini}

|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|T1 2024|T2 2024|T3 2024|T4 2024|
|------------------|---------------------------|:---:|---|-----:|:---:|:---:|:---:|:---:|
|<b>Privacy frameworks</b>|Implementation of international legal frameworks and schemas|🔴|▪️▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Transparency performance scheme and consent protocol|🔴|▪️▪️|<b>3 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Signalling interface (Privacy Broadcsasting) for digital and physical assessments|🔴|▪️|<b>5 d.</b>||||<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Receipt and record generation for framework to the schema|🔴|▪️|<b>4 d.</b>||||<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Open source technology and community identification and roadmap|🔴|▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Physical Access and Surveillance Risk assessment |🟠|▪️|<b>1 d.</b>||||<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Physical Access and Surveillance Code of Practice |🟠|▪️|<b>1 d.</b>||||<span class="cell-bg">x</span>|
|<b>Privacy frameworks</b>|Physical access and surveillance open source controller and peripheral device  contribution|🟠|▪️|<b>1 d.</b>||||<span class="cell-bg">x</span>|
|<b>Backend</b>|Dedicated backend to process consent protocol and git<>user requests|🔴|▪️▪️|<b>1 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Backend</b>|Open data API|🟡|▪️▪️|<b>5 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Backend</b>|Third party services connectors (GAIA, IDS...)|🟡|▪️▪️|<b>3 d.</b>||||<span class="cell-bg">x</span>|
|<b>Backoffice - widget configuration</b>|Interactive interface + preview|🟠|▪️▪️▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Backoffice - widget configuration</b>|Save new config to git repo|🟠|▪️▪️|<b>5 d.</b>|||<span class="cell-bg">x</span>||
|<b>Backoffice - manage contributions</b>|Authentication processes|🔴|▪️▪️▪️|<b>7 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Backoffice - manage contributions</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>||
|<b>Backoffice - manage contributions</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>2 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>3 d.</b>|||<span class="cell-bg">x</span>||
|<b>Data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>||
|<b>Data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>UI customization</b>|Accessibility|🟠|▪️▪️|<b>3 d.</b>||||<span class="cell-bg">x</span>|
|<b>Deployment / CI</b>|Deployment and CI/CD setup|🔴|▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Refactoring</b>|JS package for Git interactions|🟡|▪️▪️▪️|<b>3 d.</b>||||<span class="cell-bg">x</span>|
|<b>Refactoring</b>|Typescript implementation|🟠|▪️▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Refactoring</b>|Migration to Vue3|🟠|▪️▪️▪️|<b>5 d.</b>||<span class="cell-bg">x</span>|||
|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>2 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Project management</b>|Roadmap management|🟠|▪️▪️|<b>3 d.</b>||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Business model</b>|Implementation of payment portal|🔴|▪️|<b>2 d.</b>||<span class="cell-bg">x</span>|||
|<b>Business model</b>|Outreach to regulators / EU|🔴|▪️▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|
|<b>Business model</b>|Regulator review of code of conduct |🔴|▪️▪️▪️|<b>4 d.</b>|||<span class="cell-bg">x</span>|<span class="cell-bg">x</span>|

:::

# Provisional budget

**Milestones for a 9 month project**

---

### Global budget 

::: {.text-micro .text-justify}

:::::: {.columns}

::: {.column width=50%}

The global roadmap of the project encompasses many insights and demands we had throughout our exchanges with our users and clients.
<b>
For each demand we derived the corresponding features in need for developments
and packed them into major categories and milestones.
</b>

:::

::: {.column width=50%}

This work of discretizing the needs into features allows us
to draw a quite precise idea of the resources to mobilize
while keeping a lean approach in terms of project management.

:::

::::::

:::

### Legend

::: { .text-micro}
- <b>Category</b> : Category of the task between Onboarding / Implementing / Adopting / Backlog ;
- <b>Milestone</b> : Sub-family of tasks aiming to develop a specific feature ;
- <b>Dev + man.</b> : Development and project management time estimate ;
- Symbols used in the following tables represent either a certain level of priority or difficulty.
:::

<hr>

:::::: {.columns}

::: {.column width=35%}
::: { .text-micro}
- <b>Priority</b> is an estimation of the criticity / need of a particular feature as expressed by users or clients.
:::
:::

::: {.column .text-micro width=15%}

| Symbol    | Meaning |
| :-------: | ------  |
| 🔴        | High    |
| 🟠        | Medium  |
| 🟡        | Low     |

:::

::: {.column width=35%}
::: { .text-micro}
- <b>Difficulty</b> is an estimation of the complexity to realize the feature by the team, 
a higher level of complexity also being synonymous of a higher risk of unexpected problems to resolve along the way.   
:::
:::

::: {.column .text-micro width=15%}

| Symbol | Meaning |
| :----  | ------  |
| ▪️▪️▪️    | Hard    |
| ▪️▪️     | Medium  |
| ▪️      | Easy    |

:::

:::::::

---

## Budget

::: {.text-center}
**Regrouped by category and milestones**
:::

|**Category**|**Milestones**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost dev + management**|
|:---:|------------|------|------|---:|---:|
|Implementing|<b>Privacy frameworks</b>|🔴 high|▪️▪️▪️ hard|<b> 21 days</b>|<b>17 850 €</b>|
|Implementing|<b>Backend</b>|🔴 high|▪️▪️▪️ hard|<b> 9 days</b>|<b>7 650 €</b>|
|Implementing|<b>Backoffice - widget configuration</b>|🟠 medium|▪️ easy|<b> 8 days</b>|<b>6 800 €</b>|
|Implementing|<b>Backoffice - manage contributions</b>|🟡 low|▪️▪️ medium|<b> 17 days</b>|<b>14 450 €</b>|
|Implementing|<b>Data management</b>|🟡 low|▪️▪️ medium|<b> 15 days</b>|<b>12 750 €</b>|
|Implementing|<b>Data interaction</b>|🟠 medium|▪️ easy|<b> 4 days</b>|<b>3 400 €</b>|
|Implementing|<b>UI customization</b>|🟠 medium|▪️▪️▪️ hard|<b> 3 days</b>|<b>2 550 €</b>|
|Implementing|<b>Deployment / CI</b>|🔴 high|▪️ easy|<b> 5 days</b>|<b>4 250 €</b>|
|Implementing|<b>Refactoring</b>|🟡 low|▪️ easy|<b> 13 days</b>|<b>11 050 €</b>|
|Implementing|<b>Tests</b>|🔴 high|▪️▪️▪️ hard|<b> 7 days</b>|<b>5 950 €</b>|
|OnBoarding|<b>Project management</b>|🔴 high|▪️▪️▪️ hard|<b> 5 days</b>|<b>4 250 €</b>|
|Adopting|<b>Business model</b>|🟠 medium|▪️▪️ medium|<b> 10 days</b>|<b>8 500 €</b>|
||<b>TOTAL</b>|||**117 d.**|**99 450 €**|

---

## Detailed budget

---

### Detailed budget & roadmap - 1/2

|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost**|
|:---:|---------|------------|:---:|---|---:|---:|
|Implementing|<b>Privacy frameworks</b>|Implementation of international legal frameworks and schemas|🔴|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Privacy frameworks</b>|Transparency performance scheme and consent protocol|🔴|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Privacy frameworks</b>|Signalling interface (Privacy Broadcsasting) for digital and physical assessments|🔴|▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Privacy frameworks</b>|Receipt and record generation for framework to the schema|🔴|▪️|<b>4 d.</b>|<b>3 400 €</b>|
|Implementing|<b>Privacy frameworks</b>|Open source technology and community identification and roadmap|🔴|▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Privacy frameworks</b>|Physical Access and Surveillance Risk assessment |🟠|▪️|<b>1 d.</b>|<b>850 €</b>|
|Implementing|<b>Privacy frameworks</b>|Physical Access and Surveillance Code of Practice |🟠|▪️|<b>1 d.</b>|<b>850 €</b>|
|Implementing|<b>Privacy frameworks</b>|Physical access and surveillance open source controller and peripheral device  contribution|🟠|▪️|<b>1 d.</b>|<b>850 €</b>|
|Implementing|<b>Backend</b>|Dedicated backend to process consent protocol and git<>user requests|🔴|▪️▪️|<b>1 d.</b>|<b>850 €</b>|
|Implementing|<b>Backend</b>|Open data API|🟡|▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Backend</b>|Third party services connectors (GAIA, IDS...)|🟡|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Backoffice - widget configuration</b>|Interactive interface + preview|🟠|▪️▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Backoffice - widget configuration</b>|Save new config to git repo|🟠|▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Backoffice - manage contributions</b>|Authentication processes|🔴|▪️▪️▪️|<b>7 d.</b>|<b>5 950 €</b>|
|Implementing|<b>Backoffice - manage contributions</b>|Interface loading last PRs from repo|🔴|▪️▪️▪️|<b>4 d.</b>|<b>3 400 €</b>|
|Implementing|<b>Backoffice - manage contributions</b>|Accept / moderate PR + messages|🔴|▪️|<b>6 d.</b>|<b>5 100 €</b>|

---

### Detailed budget & roadmap - 2/2

|**Category**|**Milestones**|**Features**|**Priority**|**Difficulty**|**Dev + man.**|**Estimated cost**|
|:---:|---------|------------|:---:|---|---:|---:|
|Implementing|<b>Data management</b>|Add a new column + update schema|🟠|▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Data management</b>|Drag & drop CSV to widget|🟡|▪️▪️|<b>2 d.</b>|<b>1 700 €</b>|
|Implementing|<b>Data management</b>|Save CSV file to Git repo|🟡|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Data management</b>|Cache user changes / branch until pushing  |🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Data interaction</b>|Better integration of Frictionless data packages|🟡|▪️▪️▪️|<b>4 d.</b>|<b>3 400 €</b>|
|Implementing|<b>UI customization</b>|Accessibility|🟠|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Deployment / CI</b>|Deployment and CI/CD setup|🔴|▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Refactoring</b>|JS package for Git interactions|🟡|▪️▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Implementing|<b>Refactoring</b>|Typescript implementation|🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Refactoring</b>|Migration to Vue3|🟠|▪️▪️▪️|<b>5 d.</b>|<b>4 250 €</b>|
|Implementing|<b>Tests</b>|Functional & unit tests|🟡|▪️▪️▪️|<b>7 d.</b>|<b>5 950 €</b>|
|OnBoarding|<b>Project management</b>|Events, coordination|🔴|▪️▪️|<b>2 d.</b>|<b>1 700 €</b>|
|Adopting|<b>Project management</b>|Roadmap management|🟠|▪️▪️|<b>3 d.</b>|<b>2 550 €</b>|
|Adopting|<b>Business model</b>|Implementation of payment portal|🔴|▪️|<b>2 d.</b>|<b>1 700 €</b>|
|Adopting|<b>Business model</b>|Outreach to regulators / EU|🔴|▪️▪️▪️|<b>4 d.</b>|<b>3 400 €</b>|
|Adopting|<b>Business model</b>|Regulator review of code of conduct |🔴|▪️▪️▪️|<b>4 d.</b>|<b>3 400 €</b>|
|||||<b>TOTAL</b>|**117 d.**|**99 450 €**|


---

## Budget analysis

::: {.text-center} 
**Priority · _vs_ · Difficulty**
:::

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
    In days
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high| 19 days| 9 days| 25 days|<b> 53 days</b>|
|🟠 medium| 18 days| 16 days| 3 days|<b> 37 days</b>|
|🟡 low| 14 days| 13 days|  days|<b> 27 days</b>|
|**TOTAL**|<b> 51 days</b>|<b> 38 days</b>|<b> 28 days</b>|**117 days**|

:::


::: {.column width=50%}

::: {.text-center}
<b>
In euros
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|16 150 €|7 650 €|21 250 €|<b>45 050 €</b>|
|🟠 medium|15 300 €|13 600 €|2 550 €|<b>31 450 €</b>|
|🟡 low|11 900 €|11 050 €|  €|<b>22 950 €</b>|
|**TOTAL**|<b>43 350 €</b>|<b>32 300 €</b>|<b>23 800 €</b>|**99 450 €**|

:::

::::::

:::::: {.columns}

::: {.column width=50%}

::: {.text-center}
<b>
In proportions
</b>
:::

|**Priority / Difficulty**|▪️▪️▪️ hard|▪️▪️ medium|▪️ easy|**TOTAL**|
|------|---:|---:|---:|---:|
|🔴 high|16.2%|7.7%|21.4%|<b>45.3%</b>|
|🟠 medium|15.4%|13.7%|2.6%|<b>31.6%</b>|
|🟡 low|12.0%|11.1%|0.0%|<b>23.1%</b>|
|**TOTAL**|<b>43.6%</b>|<b>32.5%</b>|<b>23.9%</b>|**100.0%**|

:::

::::::

# References

---

## List of references

- [Kantara ANCR WG](https://kantara.atlassian.net/wiki/spaces/WA/overview)
- [Kantara ANCR GitHub (Transparency Scheme and AuthZ)](https://github.com/KantaraInitiative/WG-ANCR)
- [Kantara Consent Receipt v1,1 Specification](https://kantarainitiative.org/download/7902/)
- [Privacy as Expected Consent Gateway GitHub](https://github.com/PAECG/paecg.github.io)
- [Overlays Capture Architecture (OCA)](http://oca.colossi.network)
- [Datami](https://datami.multi.coop)